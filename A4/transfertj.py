from keras.applications.resnet50 import ResNet50, preprocess_input
from keras.optimizers import SGD, Adam
from keras.callbacks import ModelCheckpoint
import matplotlib.pyplot as plt
from keras.applications import MobileNet


HEIGHT = 90
WIDTH = 160
FC_LAYERS = [1024, 1024]
dropout = 0.5
from keras.layers import Dense, Activation, Flatten, Dropout, GlobalAveragePooling2D
from keras.models import Sequential, Model
NUM_EPOCHS = 1
BATCH_SIZE = 8
#num_train_images = 10000
num_classes = 5





# Plot the training and validation loss + accuracy
def plot_training(history):
    acc = history.history['accuracy']
    val_acc = history.history['val_accuracy']
    loss = history.history['loss']
    val_loss = history.history['val_loss']
    epochs = range(len(acc))

    plt.plot(epochs, acc, 'r.')
    plt.plot(epochs, val_acc, 'r')
    plt.title('Training and validation accuracy')

    # plt.figure()
    # plt.plot(epochs, loss, 'r.')
    # plt.plot(epochs, val_loss, 'r-')
    # plt.title('Training and validation loss')
    plt.show()

    plt.savefig('acc_vs_epochs.png')


def build_finetune_model(base_model, dropout, fc_layers, num_classes):
    for layer in base_model.layers:
        layer.trainable = False

    x = base_model.output
    x = GlobalAveragePooling2D()(x)
    for fc in fc_layers:
        # New FC layer, random init
        x = Dense(fc, activation='relu')(x) 
        x = Dropout(dropout)(x)

    # New softmax layer
    predictions = Dense(num_classes, activation='softmax')(x) 
    
    finetune_model = Model(inputs=base_model.input, outputs=predictions)

    return finetune_model


def run_trtj(X, y, classcount):

    num_train_images = len(X)

    base_model = MobileNet(weights='imagenet', 
                        include_top=False, 
                        input_shape=(HEIGHT, WIDTH, 3))

    finetune_model = build_finetune_model(base_model, 
                                        dropout=dropout, 
                                        fc_layers=FC_LAYERS, 
                                        num_classes=classcount)
                                        

    finetune_model.summary()
    adam = Adam(lr=0.0001)
    finetune_model.compile(adam, loss='categorical_crossentropy', metrics=['accuracy'])

    filepath="./checkpoints/" + "ResNet50" + "_model_weights.h5"
    checkpoint = ModelCheckpoint(filepath, monitor=["acc"], verbose=1, mode='max')
    callbacks_list = [checkpoint]

    '''
    history = finetune_model.fit_generator(train_generator, epochs=NUM_EPOCHS, workers=8, 
                                        steps_per_epoch=num_train_images // BATCH_SIZE, 
                                        shuffle=True, callbacks=callbacks_list)

    history = finetune_model.fit(X, y, epochs=NUM_EPOCHS, workers=8, 
                                        steps_per_epoch=num_train_images // BATCH_SIZE, 
                                        shuffle=True, callbacks=callbacks_list, verbose=1)
    '''
    history = finetune_model.fit(X, y, epochs=NUM_EPOCHS, verbose=1)
    #plot_training(history)
    return finetune_model

