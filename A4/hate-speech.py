"""
https://www.aicrowd.com/challenges/htspc-hate-speech-classification

"""

import numpy as np
import time
from sklearn import svm
import pickle
import sklearn.metrics as metrics
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.stem import PorterStemmer

class HateSpeechClassifier:
    def __init__(self):
        self.dataset = 0
        self.kernel = 'linear'
        self.vocabulary = None
        self.clf = 0
        self.tX = 0
        self.tlabels = 0
        self.vX = 0
        self.vlabels = 0
        self.porter_stemmer = None

    


    def metric_score(self, actual, predicted):
        vscore = metrics.accuracy_score(actual, predicted)
        f1score = metrics.f1_score(actual, predicted)

        return vscore, f1score
        
    def svm_predict(self, testdata):
        time2 = time.time()
        predicted_labels = self.clf.predict(testdata)
        time3 = time.time()
        predictduration = time3 - time2
        return predicted_labels, predictduration

    def svm_fit(self, tX, tlabels, kernel = 'linear', degree = 3):
        # kernel has to be one of ‘linear’, ‘poly’, ‘rbf’, ‘sigmoid’
        time1 = time.time()
        clf = svm.SVC(kernel=kernel, random_state = 0)
        clf.fit(tX, tlabels)
        time2 = time.time()
        fitduration = time2 - time1
        return clf, fitduration

    def fit(self, kernel = 'linear'):
        self.clf, self.fitduration = self.svm_fit(self.tX, self.tlabels, kernel = kernel)


    def read_data(self, filename, datacol = ["text"], labelcol = ["labels"], validate = True):
        dataset = pd.read_csv(filename)
        #print(dataset.columns)
        inputdata = dataset.iloc[:, 0]
        #print("Before: \n", inputdata[0:10])
        # Stem the data
        #self.porter_stemmer = PorterStemmer()
        #inputdata = self.stem_content(inputdata)
        #print("Before: \n", inputdata[0:10])


        if (validate == True):
            vocab = None
            labels = True
        else:
            vocab = self.vocabulary
            labels = False

        vectorizer, X = self.vectorize(inputdata, vocab = vocab, labels = labels)
        # Input data is now vectorized. We can split it now. 

        if (validate == True):
            labeldata = dataset.iloc[:,1]
            VALIDATION_SET_SIZE = 0.2
            mask = np.random.rand(len(dataset)) <= VALIDATION_SET_SIZE
            self.tX = X[~mask]
            self.vX = X[mask]
            self.tlabels = labeldata[~mask]
            self.vlabels = labeldata[mask]
            self.tV = vectorizer
            self.vocabulary = self.tV.vocabulary_ # need to use same vocab for test data. 

        
        self.dataset = dataset
        return

    def predict(self, testdata):
        predicted_labels, predictduration = self.svm_predict(testdata)
        return predicted_labels, predictduration

    def vectorize(self, inputdata, vocab = None, labels = True):
        vectorizer = TfidfVectorizer(vocabulary = vocab)
        X = vectorizer.fit_transform(inputdata)
        return vectorizer, X
    
    def stem_content(self, data):
            # apply stem_all to each value in the series
            data = data.apply(self.stem_all)
            return data

    def stem_all(self, content):
        #print("Content\n", content)
        wordlist = list()
        for word in content.split():
            wordlist.append(self.porter_stemmer.stem(word))

        stemmed_content =  " ".join(wordlist)

        #print("\nStemmed Content:\n", stemmed_content)
        return stemmed_content

def test_run():
    # Read the file
    hsclf = HateSpeechClassifier()
    hsclf.read_data("datasets/hs_train.csv")
    hsclf.fit(kernel = 'rbf')
    predicted_labels, predictduration = hsclf.predict(hsclf.vX)
    accuracy_score, f1score = hsclf.metric_score(hsclf.vlabels, predicted_labels)
    print("Accuracy: ", accuracy_score, " F1 score: ", f1score, "\nTime taken to predict (validation data): ", predictduration)

test_run()


    


                

