"""
https://www.aicrowd.com/challenges/htspc-hate-speech-classification

"""

import numpy as np
import time
from sklearn import svm
import pickle
import sklearn.metrics as metrics
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.stem import PorterStemmer
#import a4svm as a4svm
#import a4mlp as a4mlp
#import a4cnn as a4cnn
from sklearn.linear_model import LogisticRegression as sklogreg

class HateSpeechData:
    def __init__(self):
        self.dataset = 0
        self.vocabulary = None
        self.tX = 0
        self.tlabels = 0
        self.vX = 0
        self.vlabels = 0
        self.porter_stemmer = None

    


    def metric_score(self, actual, predicted):
        vscore = metrics.accuracy_score(actual, predicted)
        f1score = metrics.f1_score(actual, predicted)

        return vscore, f1score
        


    def read_data(self, filename, datacol = ["text"], labelcol = ["labels"], validate = True):
        dataset = pd.read_csv(filename)
        #print(dataset.columns)
        inputdata = dataset.iloc[:, 0]
        #print("Before: \n", inputdata[0:10])
        # Stem the data
        #self.porter_stemmer = PorterStemmer()
        #inputdata = self.stem_content(inputdata)
        #print("Before: \n", inputdata[0:10])


        if (validate == True):
            vocab = None
            labels = True
        else:
            vocab = self.vocabulary
            labels = False

        vectorizer, X = self.vectorize(inputdata, vocab = vocab, labels = labels)
        # Input data is now vectorized. We can split it now. 
        labeldata = dataset.iloc[:,1]
        self.tV = vectorizer
        self.vocabulary = self.tV.vocabulary_ # need to use same vocab for test data. 

        if (validate == True):
            VALIDATION_SET_SIZE = 0.2
            mask = np.random.rand(len(dataset)) <= VALIDATION_SET_SIZE
            self.tX = X[~mask]
            self.tlabels = labeldata[~mask]
            self.vX = X[mask]
            self.vlabels = labeldata[mask]
        else:
            self.tX = X
            self.tlabels = labeldata
            self.vX = None
            self.vlabels = None

        
        self.dataset = dataset
        return


    def vectorize(self, inputdata, vocab = None, labels = True):
        vectorizer = TfidfVectorizer(vocabulary = vocab)
        X = vectorizer.fit_transform(inputdata)
        return vectorizer, X
    
    def stem_content(self, data):
            # apply stem_all to each value in the series
            data = data.apply(self.stem_all)
            return data

    def stem_all(self, content):
        #print("Content\n", content)
        wordlist = list()
        for word in content.split():
            wordlist.append(self.porter_stemmer.stem(word))

        stemmed_content =  " ".join(wordlist)

        #print("\nStemmed Content:\n", stemmed_content)
        return stemmed_content

def test_run():
    # Read the file
    hsclf = HateSpeechData()
    hsclf.read_data("datasets/hs_train.csv")
    clf = a4svm.SVMClassifier(hsclf.tX, hsclf.tlabels)
    fitduration = clf.fit()
    predicted_labels, predictduration = clf.predict(hsclf.vX)
    accuracy_score, f1score = hsclf.metric_score(hsclf.vlabels, predicted_labels)
    print("Accuracy: ", accuracy_score, " F1 score: ", f1score)
    print("\nTime taken: Fit - ", fitduration, " Predict - ", predictduration)

#test_run()

def test_run2():
    # Read the file
    hsclf = HateSpeechData()
    hsclf.read_data("datasets/hs_train.csv")
    # Get a dense matrix representation
    denseX = hsclf.tX.toarray()
    m = a4mlp.MLPModel(denseX, hsclf.tlabels)
    m.set_parameters(dropout_prob = 0.2,
                            loss_function = 'categorical_crossentropy',
                            metric = 'accuracy',
                            final_activation_function = 'softmax',
                            isRegression = False,
                            category_count = 2
                    )
    m.prepare_data()
    m.prepare_model()
    m.fit()
    #m.get_score(hsclf.vX, hsclf.vlabels)
    denseX = hsclf.vX.toarray()
    predicted_labels = m.predict(denseX)
    #print("\nPredicted Labels: ", predicted_labels[0:10])
    #print("\nActual Labels: ", hsclf.vlabels[0:10])
    accuracy_score, f1score = hsclf.metric_score(hsclf.vlabels, predicted_labels)
    print("Accuracy: ", accuracy_score, " F1 score: ", f1score)
    
#test_run2()

def test_run3_sklearn():
    # Read the file
    hsclf = HateSpeechData()
    hsclf.read_data("datasets/hs_train.csv", validate = False)
    denseX = hsclf.tX.toarray()

    clf = sklogreg(random_state=0).fit(denseX, hsclf.tlabels)
    
    #denseX = hsclf.vX.toarray()
    predicted_labels = clf.predict(denseX)
    
    #accuracy_score, f1score = hsclf.metric_score(hsclf.vlabels, predicted_labels)
    accuracy_score, f1score = hsclf.metric_score(hsclf.tlabels, predicted_labels)
    print("\nValidation.\n Accuracy: ", accuracy_score, " F1 score: ", f1score)


    data = pd.read_csv('datasets/hs_test.csv')
    vectorizer, X = hsclf.vectorize(data.iloc[:,0], vocab = hsclf.vocabulary, labels = False)
    
    denseX = X.toarray()
    predicted_labels = clf.predict(denseX)
    wfile = open('hs_testout_sklearnlogreg_full.csv', 'w', encoding="utf-8")
    wfile.write("labels\n")
    for i in range(len(predicted_labels)):
        wstring = str(predicted_labels[i]) + '\n'
        wfile.write(wstring)

    wfile.close()


def test_run3_sklearn_61552():
    '''
    Full data - validate = False. 
    Accuracy:  0.8184580326623623  F1 score:  0.8672222222222222

    '''
    # Read the file
    hsclf = HateSpeechData()
    hsclf.read_data("datasets/hs_train.csv", validate = False)
    denseX = hsclf.tX.toarray()

    clf = sklogreg(random_state=0).fit(denseX, hsclf.tlabels)
    
    #denseX = hsclf.vX.toarray()
    predicted_labels = clf.predict(denseX)
    
    #accuracy_score, f1score = hsclf.metric_score(hsclf.vlabels, predicted_labels)
    accuracy_score, f1score = hsclf.metric_score(hsclf.tlabels, predicted_labels)
    print("\nValidation.\n Accuracy: ", accuracy_score, " F1 score: ", f1score)


    data = pd.read_csv('datasets/hs_test.csv')
    vectorizer, X = hsclf.vectorize(data.iloc[:,0], vocab = hsclf.vocabulary, labels = False)
    
    denseX = X.toarray()
    predicted_labels = clf.predict(denseX)
    wfile = open('hs_testout_sklearnlogreg_full.csv', 'w', encoding="utf-8")
    wfile.write("labels\n")
    for i in range(len(predicted_labels)):
        wstring = str(predicted_labels[i]) + '\n'
        wfile.write(wstring)

    wfile.close()

test_run3_sklearn_61552()

