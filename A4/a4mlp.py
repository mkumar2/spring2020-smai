'''
We need to prepare the data. Since we are given date and time, we should break down the x values into the 
components: year, month, week, day_of_year, day_of_month, day_of_week, hour, minute

'''


# References: 
# https://machinelearningmastery.com/tensorflow-tutorial-deep-learning-with-tf-keras/


import numpy as np 
import pandas as pd 
import time
from sklearn.preprocessing import MinMaxScaler
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras.optimizers import RMSprop
from keras import backend as K




class MLPModel:

    '''
    For Regression: 
            self.metric = 'mean_squared_error'
            self.loss_function = 'mean_absolute_error'
            self.final_activation_function = 'linear' # For regression

    For classification
            self.loss_function = 'categorical_crossentropy'
            self.metric = 'accuracy'
            self.final_activation_function = 'softmax'


    '''

    def __init__(self, x_data, y_data):
        self.model = None # The keras model that is setup
        self.batch_size = 128
        self.epochs = 20
        self.layer_dimension = 120
        self.final_dimension = None
        self.category_count = None
        self.hidden_layers = 5
        self.loss_function = None
        self.optimizer = None
        self.metric = None
        self.layer_activation_function = 'relu'
        self.final_activation_function = None
        self.dropout_prob = 0.2
        self.input_shape = None
        self.verbose = 1
        self.scaler_x = None
        self.x_data = x_data
        self.y_data = y_data
        self.isRegression = None
        print("\nX Shape: ", x_data.shape, " Y Shape: ", y_data.shape)
        
        
    def set_parameters(self, 
                            isRegression = True,
                            hidden_layers = 1, 
                            layer_activation_function = 'relu', 
                            dropout_prob = 0.2,
                            optimizer = 'adam',
                            final_activation_function = 'linear',
                            loss_function = 'mean_absolute_error',
                            metric = 'mean_squared_error',
                            category_count = 1,
                            epochs = 10
                            ):
    
        # Set various parameters to tune the MLP
        self.hidden_layers = hidden_layers
        self.layer_activation_function = layer_activation_function
        self.dropout_prob = dropout_prob
        self.optimizer =  optimizer
        self.final_activation_function = final_activation_function
        self.loss_function = loss_function
        self.metric = metric
        self.epochs = epochs
        self.isRegression = isRegression
        self.category_count = category_count
            


        
    def prepare_data(self):
        
        (samplesize, featuresize) = self.x_data.shape
        print("Prepare_data: X Data shape is ", self.x_data.shape)

        self.input_shape = (featuresize,)

        if (self.isRegression == False):
            # For categorical, we have to convert Y to matrix
            y_data = keras.utils.to_categorical(self.y_data, self.category_count)
            self.final_dimension = self.category_count
            
        else:
            y_data = self.y_data
            self.final_dimension = 1

        scaler_x = MinMaxScaler()
        self.scaler_x = scaler_x

        xscale = scaler_x.fit_transform(self.x_data)
        yscale = y_data
        
        self.x_data = xscale
        self.y_data = yscale
                  
        return (xscale, yscale)
        
        
        
    def add_mlp_layers(self, model, hidden_layers, 
                        layer_dimension, layer_activation_function, 
                        input_shape, dropout_prob):
    
        #print("Adding Layers: ", hidden_layers + 1)
        for i in range(hidden_layers + 1): # Add 1 because we have to also add input layer
                #print("--- Adding dense layer")
                model.add(Dense(
                        layer_dimension, 
                        activation=layer_activation_function,
                        input_shape=input_shape)
                    )
                if (dropout_prob > 0):
                        #print("--- Adding dropout layer")
                        model.add(Dropout(dropout_prob))
        
        #print("Done adding layers")
                

    def prepare_model(self):

        model = Sequential()
        # add input and hidden layers
        self.add_mlp_layers(
                            model,
                            self.hidden_layers, 
                            self.layer_dimension, 
                            self.layer_activation_function,
                            self.input_shape,
                            self.dropout_prob
                        )
                

        # Add the output layer
        model.add(Dense(
                    self.final_dimension, 
                    activation=self.final_activation_function)
                )

        #print("Also added final layer")
                
        model.summary()

        model.compile(loss= self.loss_function,
                        optimizer = self.optimizer,
                        metrics=[self.metric])

        self.model = model
              
        return model
    
    def fit (self):
        
        self.model.fit( self.x_data, self.y_data,
                    batch_size=self.batch_size,
                    epochs=self.epochs,
                    verbose=self.verbose,
                    validation_split=0.2
                )
    
    def predict(self, testdata):
        scaledtestdata = self.scaler_x.transform(testdata)
        if (self.isRegression == True):
            predicted_labels = self.model.predict(scaledtestdata)
        else:
            predicted_labels = self.model.predict_classes(scaledtestdata)
        return predicted_labels

    def get_score(self, x_test, y_test, verbose=0):
        scaledtestdata = self.scaler_x.transform(x_test)
        score = self.model.evaluate(scaledtestdata, y_test, verbose=verbose)
        print('Test loss:', score[0])
        print('Test accuracy:', score[1])
        return score     

    



        



