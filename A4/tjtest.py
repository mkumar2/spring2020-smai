import a4tjnn as q3
import tomjerry as tj
import sklearn.metrics as metrics
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D

'''
What have we tried to move above 0.3 val_accuracy and 0.27 of accuracy
1. Changed learning rate
2. Added more convolutional layers, this reduced accuracy
3. Kernel size - 3
4. Kernel size - 7
5. Layer_dimension - 64

'''

def write_output2(filename, actual_labels, predicted_labels, header = None):
    wfile = open(filename, 'w')
    if (header != None):
        wfile.write(header + '\n')
    for i in range(len(predicted_labels)):
        wstring =  str(actual_labels[i]) + ',' + str(predicted_labels[i]) + '\n'
        wfile.write(wstring)

    wfile.close()

def prepare_model_custom(
                        filters = 32,
                        kernel_size = 3,
                        layer_activation_function = 'relu',
                        input_shape = (90, 160, 1),
                        dropout_prob = 0.2,
                        final_activation_function = 'softmax',
                        layer_dimension = 64,
                        final_dimension = 5,
                        loss_function = 'categorical_crossentropy',
                        optimizer = 'adam',
                        metric = 'accuracy',
                        ):
        

        model = Sequential()
        
        # Convolution layer
        model.add(Conv2D(filters, kernel_size, padding='same',
                        activation=layer_activation_function,
                        input_shape=input_shape))
        

        # Another convolution layer
        model.add(Conv2D(filters, kernel_size, padding='same',
                        activation=layer_activation_function,
                        input_shape=input_shape))

        # Max Pooling layer
        model.add(MaxPooling2D()) 
        

        # Dropout layer
        model.add(Dropout(dropout_prob)) 
        
        # Flattened layer
        model.add(Flatten()) 
        
        # Dense layer
        model.add(Dense(      
                layer_dimension, 
                activation=layer_activation_function,
                input_shape=input_shape)
            )
        
        # Dropout layer
        model.add(Dropout(dropout_prob)) 

        
        # Add the output layer
        model.add(Dense(
                    final_dimension, 
                    activation=final_activation_function)
                )

        #model.summary()

        optimizer = keras.optimizers.Adam(learning_rate=0.001, beta_1=0.9, beta_2=0.999, amsgrad=False)
        #optimizer = keras.optimizers.Adam(learning_rate=0.05, beta_1=0.9, beta_2=0.999, amsgrad=False)
        
        model.compile(loss= loss_function,
                        optimizer = optimizer,
                        metrics=[metric])
              
        return model

training_file = 'datasets/tj_train.csv'
training_path = 'datasets/tj_ntrain'

tjclf = tj.LogisticRegression(training_file=training_file, training_path=training_path)
tjclf.read_training_file()
tjclf.read_training_images()

m = q3.Q3Model()
m.training_images = tjclf.X
m.training_labels = (tjclf.y).astype(int)

hidden_layers = 5
cnn_layers = 2
dense_layers = 0 
activation_function = 'relu'
epochs = 10
dropout_prob = 0
verbose = 1
type = 'cnn'


m.set_parameters(dropout_prob = dropout_prob, optimizer = m.set_optimizer(), type=type,
                    activation_function = activation_function,
                    cnn_layers=cnn_layers,
                    dense_layers=dense_layers,
                    hidden_layers=hidden_layers,
                    outdim = 32,
                    epochs = epochs 
                )
if (type == 'mlp'):
    params = m.mlp_params
else:
    params = m.cnn_params

#params.epochs = 5
m.prepare_data(params, type=type)
#params.model = m.prepare_model(params, type=type)
params.model = prepare_model_custom()
params.model.summary()

history = m.fit(params.model, params, verbose = verbose)
score = params.model.evaluate(params.x_train, params.y_train, verbose=verbose)
print('Training loss:', score[0])
print('Training accuracy:', score[1])     
predicted_labels = params.model.predict_classes(params.x_train)
print(m.training_labels)
print(predicted_labels)
write_output2("testfile.csv", m.training_labels, predicted_labels, "actual,predicted")
vscore = metrics.accuracy_score(m.training_labels, predicted_labels)
#f1score = metrics.f1_score(m.training_labels, predicted_labels)

print(vscore)
