import numpy as np
from skimage import io
import sklearn.metrics as metrics
import sys
from sklearn.decomposition import PCA
import numpy.linalg as linalg

verbose = 1

verboseprint = print if verbose else lambda *a, **k: None


def sigmoid(scores):
    return 1 / (1 + np.exp(-scores))

def loss(samples, features, weights):
    scores = np.dot(samples, weights)
    loss_value = np.sum( features*scores - np.log(1 + np.exp(scores)) )
    return loss_value

def logistic_regression(samples, features, iterations, learning_rate):
    intercept = np.ones((samples.shape[0], 1)) # Add x0 
    samples = np.concatenate((intercept, samples), axis=1)
        
    weights = np.zeros(samples.shape[1])
    
    for iteration in range(iterations):
        scores = np.dot(samples, weights)
        predictions = sigmoid(scores)

        # Update weights with log likelihood gradient
        output_error_signal = features - predictions
        
        gradient = np.dot(samples.T, output_error_signal)
        weights += learning_rate * gradient

        # Print the loss value 10 times in a given run
        if iteration % round(iterations/10) == 0:
            verboseprint ("Iteration: ", iteration, " Loss: ", loss(samples, features, weights))
        
    return weights

def predicted_values(test_X, weights):
    
    # Add the intercept
    intercept = np.ones((test_X.shape[0], 1))
    
    newtest_X = np.concatenate((intercept, test_X), axis = 1)

    final_scores = np.dot(newtest_X, weights) # Use concatenate or append.

    predicted_values = np.round(sigmoid(final_scores))
    
    return predicted_values


def show_metrics(testlabels, predicted_labels):
    print("Accuracy Score: ", metrics.accuracy_score(testlabels, predicted_labels))
    print("F1-score: ", metrics.f1_score(testlabels, predicted_labels, average='weighted'))
    print("Confusion Metrix: \n", metrics.confusion_matrix(testlabels, predicted_labels))




class LogisticRegression:
    def __init__(self, X, y):
        self.comps = None
        self.iterations = None
        self.X = X
        self.y = y
        self.fdict = None
        self.learning_rate = None
        self.wtdict = None






    def set_params(self, iterations = 100000, learning_rate = 0.00001):
        self.iterations = iterations
        self.learning_rate = learning_rate


    def fit(self):
        '''
        How do we fit multi-label?
        1. Find all the unique labels
        2. for each item in the list of labels, create a y such that it has 1 for the selected label, 0 otherwise
        3. For this y, learn the weights and store it against this label
        4. So fit generates a set of weights. 
        '''
        labellist = list(np.unique(self.y))
        verboseprint("Fit: Label List is ", labellist)
        wtdict = dict() # Store the pair of label and its learned weights

        for label in labellist:
            newy = (self.y == label).astype(int) # y corresponding to this label
            verboseprint("\nFitting for label ", label)
            wtdict[label] = logistic_regression(self.X, newy,
                     iterations = self.iterations, learning_rate = self.learning_rate)

        self.wtdict = wtdict
        self.labellist = labellist


    
    def predict(self, test_X):
        # Check with all the labels. Pick the highest score

        wtdict = self.wtdict
        labellist = self.labellist
        valueslist = list()

        for label in labellist:
            weights = wtdict[label]
            # use this weight to predict
            pvalues = predicted_values(test_X, weights)
            valueslist.append(pvalues)

         # Now I have N predictions, N = number of labels. 

        predtable = np.array(valueslist).T
        verboseprint("Shape of predictions table: ", predtable.shape)
        verboseprint("Labels: ", labellist)

        finallist = list()
        for row in predtable:
            i = row.argmax()
            finallist.append(labellist[i]) # The label corresponding to the index of max is inserted

        return np.array(finallist).reshape(len(finallist)) # convert list into 1-D array and return


        

    def evaluate(self, train_X, train_y):
        predicted_labels = self.predict(train_X)
        show_metrics(train_y, predicted_labels)

    

def final_prediction(training_file, testing_file):
    # Use the training file to learn
    clf = LogisticRegression(training_file=training_file)
    clf.set_params()
    clf.prepare_data()
    clf.fit()
    test_X0 = read_testing_data(testing_file)
    test_X = clf.pca.transform(test_X0) # Transform using the same PCA model from training
    predicted_labels = clf.predict(test_X)
    for label in predicted_labels:
        print(label)


