# References: 
# https://machinelearningmastery.com/tensorflow-tutorial-deep-learning-with-tf-keras/


from mnist import MNIST
import numpy as np

import keras


from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras.optimizers import RMSprop
from keras import backend as K



class Optimizer: 
    def __init__(self):
        self.name = 'RMSProp'
        self.clipnorm = 1
        self.clipvalue = 0.5
        self.learning_rate = 0.001
        self.momentum_weight = 0.1
        self.rho = 0.9 # only for RMSProp
        self.beta_1 = 0.9
        self.beta_2 = 0.999 

        

        
class SVMModel:
    def __init__(self):
        self.model = None # The keras model that is setup
        self.featuresize = 784
        self.x_train = None
        self.x_test = None
        self.y_train = None
        self.y_test = None
        self.input_shape = None

class MLPCNNModel:
    def __init__(self):
        self.model = None # The keras model that is setup
        self.batch_size = 128
        self.epochs = 2
        self.layer_dimension = 128
        self.final_dimension = 5
        self.hidden_layers = 5
        self.loss_function = 'categorical_crossentropy'
        self.optimizer = None
        self.metric = 'accuracy'
        self.layer_activation_function = 'relu'
        self.final_activation_function = 'softmax'
        self.featuresize = 784
        self.dropout_prob = 0.2
        self.filters = 32 # should this be 64?
        self.kernel_size = 3
        self.max_pooling = True
        self.x_train = None
        self.x_test = None
        self.y_train = None
        self.y_test = None
        self.input_shape = None
        self.pixelsize = 0
        self.classcount = 0


    def show(self):
        print("----MLP Params-----\n")
        print("Batch size: ", self.batch_size)
        print("epochs: ", self.epochs)
        print("layer dimension: ", self.layer_dimension)
        print("final dimension: ", self.final_dimension)
        print("loss function", self.loss_function)
        print("optimizer: ", self.optimizer)
        print("metric: ", self.metric)
        print("Layer activation function: ", self.layer_activation_function)
        print("Final activation function: ", self.final_activation_function)
        print("Feature size: ", self.featuresize)
        print("Dropout Probability: ", self.dropout_prob)

    

        


class Q3Model:
    def __init__(self, path = './mnist-data'):
        self.validation_split = 0.2
        self.path = path
        self.training_images = None
        self.training_labels = None
        self.testing_images = None
        self.testing_labels = None
        self.mlp_params = None
        self.cnn_params = None
        self.svm_params = None

    def show(self):
        print("----MLP-----\n")
        self.print_image_stats(self.x_train)
        self.print_image_stats(self.x_test)
        self.print_label_stats(self.y_train)
        self.print_label_stats(self.y_test)
        print("Validation split: ", self.validation_split)
        print("Path: ", self.path)
        if (self.mlp_params != None): 
            self.mlp_params.show()
        if (self.cnn_params != None):
            self.mnist.show()


    def create_MLP(self):
        # Create MLP object
        self.mlp_params = MLPCNNModel()


    def print_image_stats(self, img):
        #npimg = np.array(img)
        npimg = img
        print("Shape: ", npimg.shape)
        print("Min: ", np.amin(npimg, axis= 1))
        print("Max: ", np.amax(npimg, axis= 1))

    def print_label_stats(self, label):
        print("Shape: ", label.shape)
        print("Min: ", np.amin(label))
        print("Max: ", np.amax(label))

    
        
        
        

    def set_data(self, tX = None, ty = None, vX = None, vy = None):
        self.training_images = tX
        self.training_labels = ty
        self.testing_images = vX
        self.testing_labels = vy
        

    '''
    Parameters for MLP and CNN: 
    1. # of hidden layers
    2. Activation function in hidden layers
    3. # of output dimension in hidden layers
    4. different frame sizes

    '''
    def set_parameters(self, 
                            hidden_layers = 1, 
                            cnn_layers = 2, 
                            dense_layers = 1, 
                            activation_function = 'relu', 
                            outdim = 512, 
                            epochs = 10,
                            type = 'mlp',
                            pixelsize = 255,
                            classcount = 2,
                            filters = 32,
                            kernel_size = 3,
                            layer_activation_function = 'relu',
                            input_shape = (90, 160, 3),
                            dropout_prob = 0.2,
                            final_activation_function = 'softmax',
                            layer_dimension = 64,
                            loss_function = 'categorical_crossentropy',
                            optimizer = 'adam',
                            metric = 'accuracy'

                            ):
    
        params = MLPCNNModel()
        # Set various parameters to tune the MLP
        params.hidden_layers = hidden_layers
        params.cnn_layers = cnn_layers
        params.dense_layers = dense_layers
        params.layer_activation_function = activation_function
        params.layer_dimension = outdim
        params.dropout_prob = dropout_prob
        params.optimizer = optimizer
        params.epochs = epochs
        params.pixelsize = pixelsize
        params.classcount = classcount
        params.filters = filters
        params.kernel_size = kernel_size
        params.layer_activation_function = layer_activation_function
        params.input_shape = input_shape
        params.final_activation_function = final_activation_function
        params.layer_dimension = layer_dimension
        params.final_dimension = classcount # Classcount needs to be same as final dimension
        params.loss_function = loss_function
        params.metric = metric
        

        if (type == 'mlp'):
            self.mlp_params = params
        else:
            self.cnn_params = params
        


    def set_optimizer(self):
        optimizer=RMSprop()
        return optimizer


    def prepare_data(self, params, type = 'mlp'):
        
        trainingcount = len(self.training_images)
        rowsize = params.input_shape[0]
        colsize = params.input_shape[1]
        channels = params.input_shape[2]
        pixelsize = params.pixelsize
        classcount = params.classcount
        
        featuresize = rowsize * colsize

        x_train = np.array(self.training_images)
        x_train = x_train.astype('float32')/pixelsize
        
        if (type == 'cnn'):
            if K.image_data_format() == 'channels_first':
                x_train = x_train.reshape(x_train.shape[0], channels, rowsize, colsize)
                input_shape = (channels, rowsize, colsize)
            else:
                x_train = x_train.reshape(x_train.shape[0], rowsize, colsize, channels)
                input_shape = (rowsize, colsize, channels)
        else:
            x_train = x_train.reshape(trainingcount, featuresize)
            input_shape = (featuresize,)
            

        print(x_train.shape[0], 'train samples')
        
        y_train = keras.utils.to_categorical(self.training_labels, classcount)
        

        
        params.x_train = x_train
        params.y_train = y_train
        params.input_shape = input_shape
        print("input_shape: ", input_shape)
        print("X shape: ", x_train.shape)
        print("Y shape: ", y_train.shape)
        



    def add_mlp_layers(self, model, hidden_layers, 
                        layer_dimension, layer_activation_function, 
                        input_shape, dropout_prob):
    
        #print("Adding Layers: ", hidden_layers + 1)
        for i in range(hidden_layers + 1): # Add 1 because we have to also add input layer
                #print("--- Adding dense layer")
                model.add(Dense(
                        layer_dimension, 
                        activation=layer_activation_function,
                        input_shape=input_shape)
                    )
                if (dropout_prob > 0):
                        #print("--- Adding dropout layer")
                        model.add(Dropout(dropout_prob))
        
        #print("Done adding layers")
                

    def add_cnn_layers(self, model, cnn_layers, dense_layers, filters, kernel_size,  
                        layer_dimension, layer_activation_function, input_shape, dropout_prob,
                        max_pooling):

        
        for i in range(cnn_layers):
            model.add(Conv2D(filters, kernel_size, padding='same',
                            activation=layer_activation_function,
                            input_shape=input_shape))
            if (max_pooling == True):
                model.add(MaxPooling2D())
            if (dropout_prob > 0):
                model.add(Dropout(dropout_prob))


        model.add(Flatten())

        self.add_mlp_layers(
                        model, 
                        dense_layers, 
                        layer_dimension, 
                        layer_activation_function, 
                        input_shape, 
                        dropout_prob
                    )


    

    def prepare_model(self, params, type = 'mlp'):
        

        model = Sequential()
        # add input and hidden layers
        if (type == 'mlp'):
            self.add_mlp_layers(
                                model,
                                params.hidden_layers, 
                                params.layer_dimension, 
                                params.layer_activation_function,
                                params.input_shape,
                                params.dropout_prob
                            )
        else:
            self.add_cnn_layers(
                                model,
                                params.cnn_layers, 
                                params.dense_layers, 
                                params.filters, 
                                params.kernel_size, 
                                params.layer_dimension, 
                                params.layer_activation_function,
                                params.input_shape,
                                params.dropout_prob,
                                params.max_pooling 
                            )
                

        # Add the output layer
        model.add(Dense(
                    params.final_dimension, 
                    activation=params.final_activation_function)
                )

        #print("Also added final layer")
                
        #model.summary()

        model.compile(loss= params.loss_function,
                        optimizer = params.optimizer,
                        metrics=[params.metric])
              
        return model
    
    def fit (self, model, params, type = "split", verbose = 0):

        data = None
        split = self.validation_split


        history = model.fit(params.x_train, params.y_train,
            batch_size=params.batch_size,
            epochs=params.epochs,
            verbose=verbose,
            validation_data=data,
            validation_split=split)
        
        return history
    
    def get_score(self, params, verbose=0):
        score = params.model.evaluate(params.x_train, params.y_train, verbose=verbose)
        print('Training loss:', score[0])
        print('Training accuracy:', score[1])     


def prepare_model_custom(params):
        
        filters = params.filters
        kernel_size = params.kernel_size
        layer_activation_function = params.layer_activation_function
        input_shape = params.input_shape
        dropout_prob = params.dropout_prob
        final_activation_function = params.final_activation_function
        layer_dimension = params.layer_dimension
        final_dimension = params.final_dimension
        loss_function = params.loss_function
        optimizer = params.optimizer
        metric = params.metric

        model = Sequential()
        
        # Convolution layer
        model.add(Conv2D(filters, kernel_size, padding='same',
                        activation=layer_activation_function,
                        input_shape=input_shape))
        

        # Another convolution layer
        model.add(Conv2D(filters, kernel_size, padding='same',
                        activation=layer_activation_function,
                        input_shape=input_shape))

        # Max Pooling layer
        model.add(MaxPooling2D()) 
        
        # Another convolution layer
        model.add(Conv2D(filters, kernel_size, padding='same',
                        activation=layer_activation_function,
                        input_shape=input_shape))

        # Max Pooling layer
        model.add(MaxPooling2D()) 
        # Another convolution layer
        model.add(Conv2D(filters, kernel_size, padding='same',
                        activation=layer_activation_function,
                        input_shape=input_shape))

        # Max Pooling layer
        model.add(MaxPooling2D()) 

        # Another convolution layer
        model.add(Conv2D(filters, kernel_size, padding='same',
                        activation=layer_activation_function,
                        input_shape=input_shape))

        # Max Pooling layer
        model.add(MaxPooling2D()) 

        # Dropout layer
        model.add(Dropout(dropout_prob)) 
        
        # Flattened layer
        model.add(Flatten()) 
        
        # Dense layer
        model.add(Dense(      
                layer_dimension, 
                activation=layer_activation_function,
                input_shape=input_shape)
            )
        
        # Dropout layer
        model.add(Dropout(dropout_prob)) 

        
        # Add the output layer
        model.add(Dense(
                    final_dimension, 
                    activation=final_activation_function)
                )

        #model.summary()

        optimizer = keras.optimizers.Adam(learning_rate=0.001, beta_1=0.9, beta_2=0.999, amsgrad=False)
        #optimizer = keras.optimizers.Adam(learning_rate=0.05, beta_1=0.9, beta_2=0.999, amsgrad=False)
        
        model.compile(loss= loss_function,
                        optimizer = optimizer,
                        metrics=[metric])
              
        return model

def do_naivecnn(X, y, input_shape, pixelsize, classcount):
    m = Q3Model()
    m.set_data(X, y )
    type = 'cnn'    
    verbose = 1

    m.set_parameters(type = type,
                    epochs = 2,
                    input_shape = input_shape,
                    pixelsize = pixelsize,
                    classcount = classcount,
                    filters = 32,
                    kernel_size = 5,
                    layer_activation_function = 'relu',
                    dropout_prob = 0.2,
                    final_activation_function = 'softmax',
                    layer_dimension = 256,
                    loss_function = 'categorical_crossentropy',
                    optimizer = 'adam',
                    metric = 'accuracy'
                )

    if (type == 'mlp'):
        params = m.mlp_params
    else:
        params = m.cnn_params

    m.prepare_data(params, type=type)
    params.model = prepare_model_custom(params)
    params.model.summary()
    
    verbose = 1

    history = m.fit(params.model, params, verbose = verbose)
    return params.model, m

def run_model(type, m, hidden_layers = 3, cnn_layers = 2, dense_layers = 1, 
                activation_function = 'relu', epochs = 10, dropout_prob = 0.2,
                verbose = 0):
    m.set_parameters(dropout_prob = dropout_prob, optimizer = m.set_optimizer(), type=type,
                        activation_function = activation_function,
                        cnn_layers=cnn_layers,
                        dense_layers=dense_layers,
                        hidden_layers=hidden_layers,
                        epochs = epochs 
                    )
    if (type == 'mlp'):
        params = m.mlp_params
    else:
        params = m.cnn_params

    m.prepare_data(params, type=type)
    #m.show()
    params.model = m.prepare_model(params, type=type)

    history = m.fit(params.model, params, verbose = verbose)
    '''
    score = params.model.evaluate(params.x_test, params.y_test)
    for (m, s) in zip(params.model.metrics_names, score):
        print(m, ": ", s)
    '''
    return history


# test_models('mlp', hidden_layers = 1, activation_function = 'relu', dropout_prob=0)
#m = Q3Model()
#m.load_mnist_data()

#run_model('mlp', m, hidden_layers = 1, activation_function = 'relu', dropout_prob=0.2)
#print("CNN Now. ")
#run_model('cnn', m, cnn_layers = 2, dense_layers = 1, activation_function = 'relu', dropout_prob=0.2)
