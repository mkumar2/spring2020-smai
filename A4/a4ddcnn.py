# References: 
# https://machinelearningmastery.com/tensorflow-tutorial-deep-learning-with-tf-keras/


import numpy as np

import keras


from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, GlobalMaxPooling2D, MaxPooling2D
from keras.optimizers import RMSprop
from keras import backend as K




class MLPCNNModel:
    def __init__(self):
        self.model = None # The keras model that is setup
        self.batch_size = 128
        self.epochs = 2
        self.layer_dimension = 128
        self.final_dimension = 5
        self.hidden_layers = 5
        self.loss_function = 'mean_squared_error'
        self.optimizer = None
        self.metric = 'accuracy'
        self.layer_activation_function = 'relu'
        self.final_activation_function = 'linear'
        self.featuresize = 784
        self.dropout_prob = 0.2
        self.filters = 32 # should this be 64?
        self.kernel_size = 3
        self.max_pooling = True
        self.x_train = None
        self.x_test = None
        self.y_train = None
        self.y_test = None
        self.input_shape = None
        



class Q3Model:
    def __init__(self, path = './mnist-data'):
        self.validation_split = 0.2
        self.path = path
        self.training_images = None
        self.training_labels = None
        self.testing_images = None
        self.testing_labels = None
        self.mlp_params = None
        self.cnn_params = None
        self.svm_params = None


    def set_data(self, tX = None, ty = None, vX = None, vy = None):
        self.training_images = tX
        self.training_labels = ty
        self.testing_images = vX
        self.testing_labels = vy
        

    '''
    Parameters for MLP and CNN: 
    1. # of hidden layers
    2. Activation function in hidden layers
    3. # of output dimension in hidden layers
    4. different frame sizes

    '''
    def set_parameters(self, 
                            hidden_layers = 1, 
                            cnn_layers = 2, 
                            dense_layers = 1, 
                            activation_function = 'relu', 
                            outdim = 512, 
                            epochs = 10,
                            type = 'mlp',
                            filters = 32,
                            kernel_size = 3,
                            layer_activation_function = 'relu',
                            input_shape = (90, 160, 3),
                            dropout_prob = 0.2,
                            final_activation_function = 'linear',
                            layer_dimension = 64,
                            loss_function = 'categorical_crossentropy',
                            optimizer = 'adam',
                            metric = 'accuracy'

                            ):
    
        params = MLPCNNModel()
        # Set various parameters to tune the MLP
        params.hidden_layers = hidden_layers
        params.cnn_layers = cnn_layers
        params.dense_layers = dense_layers
        params.layer_activation_function = activation_function
        params.layer_dimension = outdim
        params.dropout_prob = dropout_prob
        params.optimizer = optimizer
        params.epochs = epochs
        params.filters = filters
        params.kernel_size = kernel_size
        params.layer_activation_function = layer_activation_function
        params.input_shape = input_shape
        params.final_activation_function = final_activation_function
        params.layer_dimension = layer_dimension
        params.final_dimension = 1 
        params.loss_function = loss_function
        params.metric = metric
        

        if (type == 'mlp'):
            self.mlp_params = params
        else:
            self.cnn_params = params
        



    def prepare_data_X(self, X, input_shape, type = 'cnn'):
        trainingcount = len(X)
        rowsize = input_shape[0]
        colsize = input_shape[1]
        channels = input_shape[2]
        
        featuresize = rowsize * colsize
        x_train = np.array(X)
        x_train = x_train.astype('float32')
        
        if (type == 'cnn'):
            if K.image_data_format() == 'channels_first':
                x_train = x_train.reshape(x_train.shape[0], channels, rowsize, colsize)
                input_shape = (channels, rowsize, colsize)
            else:
                x_train = x_train.reshape(x_train.shape[0], rowsize, colsize, channels)
                input_shape = (rowsize, colsize, channels)
        else:
            x_train = x_train.reshape(trainingcount, featuresize)
            input_shape = (featuresize,)

        return x_train, input_shape
            

    def prepare_data(self, params, type = 'cnn'):
        
        '''
        trainingcount = len(self.training_images)
        rowsize = params.input_shape[0]
        colsize = params.input_shape[1]
        channels = params.input_shape[2]
        
        featuresize = rowsize * colsize

        x_train = np.array(self.training_images)
        x_train = x_train.astype('float32')
        
        if (type == 'cnn'):
            if K.image_data_format() == 'channels_first':
                x_train = x_train.reshape(x_train.shape[0], channels, rowsize, colsize)
                input_shape = (channels, rowsize, colsize)
            else:
                x_train = x_train.reshape(x_train.shape[0], rowsize, colsize, channels)
                input_shape = (rowsize, colsize, channels)
        else:
            x_train = x_train.reshape(trainingcount, featuresize)
            input_shape = (featuresize,)
            

        print(x_train.shape[0], 'train samples')
        '''
        x_train, input_shape = self.prepare_data_X(self.training_images, params.input_shape)
        

        y_train = self.training_labels
        params.x_train = x_train
        params.y_train = y_train
        params.input_shape = input_shape
        print("input_shape: ", input_shape)
        print("X shape: ", x_train.shape)
        print("Y shape: ", y_train.shape)
        



    
    def fit (self, model, params, type = "split", verbose = 0):

        data = None
        split = self.validation_split


        history = model.fit(params.x_train, params.y_train,
            batch_size=params.batch_size,
            epochs=params.epochs,
            verbose=verbose,
            validation_data=data,
            validation_split=split)
        
        return history
    
    def get_score(self, params, verbose=0):
        score = params.model.evaluate(params.x_train, params.y_train, verbose=verbose)
        print('Training loss:', score[0])
        print('Training accuracy:', score[1])     


def prepare_model_backup(params):
        
        filters = params.filters
        kernel_size = params.kernel_size
        layer_activation_function = params.layer_activation_function
        input_shape = params.input_shape
        dropout_prob = params.dropout_prob
        final_activation_function = params.final_activation_function
        layer_dimension = params.layer_dimension
        final_dimension = params.final_dimension
        loss_function = params.loss_function
        optimizer = params.optimizer
        metric = params.metric

        model = Sequential()
        
        # Convolution layer
        model.add(Conv2D(filters, kernel_size, padding='same',
                        activation=layer_activation_function,
                        input_shape=input_shape))
        

        # Another convolution layer
        model.add(Conv2D(filters, kernel_size, padding='same',
                        activation=layer_activation_function,
                        input_shape=input_shape))

        
        # Another convolution layer
        model.add(Conv2D(filters, kernel_size, padding='same',
                        activation=layer_activation_function,
                        input_shape=input_shape))

        # Max Pooling layer
        model.add(MaxPooling2D()) 
        # Another convolution layer
        model.add(Conv2D(filters, kernel_size, padding='same',
                        activation=layer_activation_function,
                        input_shape=input_shape))

        # Max Pooling layer
        model.add(MaxPooling2D()) 

        # Another convolution layer
        model.add(Conv2D(filters, kernel_size, padding='same',
                        activation=layer_activation_function,
                        input_shape=input_shape))

        # Max Pooling layer
        model.add(MaxPooling2D()) 

        # Dropout layer
        model.add(Dropout(dropout_prob)) 
        
        # Flattened layer
        model.add(Flatten()) 
        
        # Dense layer
        model.add(Dense(      
                layer_dimension, 
                activation=layer_activation_function,
                input_shape=input_shape)
            )
        
        # Dropout layer
        model.add(Dropout(dropout_prob)) 

        
        # Add the output layer
        model.add(Dense(
                    final_dimension, 
                    activation=final_activation_function)
                )

        #model.summary()

        optimizer = keras.optimizers.Adam(learning_rate=0.05, beta_1=0.9, beta_2=0.999, amsgrad=False)
        #optimizer = keras.optimizers.Adam(learning_rate=0.05, beta_1=0.9, beta_2=0.999, amsgrad=False)
        
        model.compile(loss= loss_function,
                        optimizer = optimizer,
                        metrics=[metric])
              
        return model

def prepare_model_61537(params):
        '''
        # Merged all 3 data sources, 10 epochs. 
                    epochs = 10,
                    input_shape = input_shape,
                    filters = 64,
                    kernel_size = 3,
                    layer_activation_function = 'relu',
                    dropout_prob = 0.2,
                    final_activation_function = 'softmax',
                    layer_dimension = 128,
                    loss_function = 'mean_squared_error',
                    optimizer = 'adam',
                    metric = 'mae'

        '''    
        filters = params.filters
        kernel_size = params.kernel_size
        layer_activation_function = params.layer_activation_function
        input_shape = params.input_shape
        dropout_prob = params.dropout_prob
        final_activation_function = params.final_activation_function
        layer_dimension = params.layer_dimension
        final_dimension = params.final_dimension
        loss_function = params.loss_function
        optimizer = params.optimizer
        metric = params.metric

        model = Sequential()
        
        # Convolution layer
        model.add(Conv2D(filters, kernel_size, padding='same',
                        activation=layer_activation_function,
                        input_shape=input_shape))
        

        # Max Pooling layer
        model.add(MaxPooling2D()) 
        
        # Convolution layer
        model.add(Conv2D(filters, kernel_size, padding='same',
                        activation=layer_activation_function,
                        input_shape=input_shape))
        

        # Max Pooling layer
        model.add(MaxPooling2D()) 
        
        # Convolution layer
        model.add(Conv2D(filters, kernel_size, padding='same',
                        activation=layer_activation_function,
                        input_shape=input_shape))
        

        # Max Pooling layer
        model.add(MaxPooling2D()) 
        
        # Flattened layer
        model.add(Flatten()) 
        
        
        # Dense layer
        model.add(Dense(      
                layer_dimension, 
                activation=layer_activation_function,
                input_shape=input_shape)
            )
        
        # Dense layer
        model.add(Dense(      
                layer_dimension, 
                activation=layer_activation_function,
                input_shape=input_shape)
            )
        
        # Add the output layer
        model.add(Dense(
                    final_dimension)
                )

        #model.summary()

        optimizer = keras.optimizers.Adam(learning_rate=0.001, beta_1=0.9, beta_2=0.999, amsgrad=False)
        #optimizer = keras.optimizers.Adam(learning_rate=0.00001, beta_1=0.9, beta_2=0.999, amsgrad=False)
        
        model.compile(loss= loss_function,
                        optimizer = optimizer,
                        metrics=[metric])
              
        return model

def prepare_model_custom(params):
        
        filters = params.filters
        kernel_size = params.kernel_size
        layer_activation_function = params.layer_activation_function
        input_shape = params.input_shape
        dropout_prob = params.dropout_prob
        final_activation_function = params.final_activation_function
        layer_dimension = params.layer_dimension
        final_dimension = params.final_dimension
        loss_function = params.loss_function
        optimizer = params.optimizer
        metric = params.metric

        model = Sequential()
        
        # Convolution layer
        model.add(Conv2D(filters, kernel_size, padding='same',
                        activation=layer_activation_function,
                        input_shape=input_shape))
        

        # Max Pooling layer
        model.add(MaxPooling2D()) 
        
        
        # Dropout layer
        model.add(Dropout(dropout_prob)) 

        # Convolution layer
        model.add(Conv2D(filters, kernel_size, padding='same',
                        activation=layer_activation_function,
                        input_shape=input_shape))
        

        # Max Pooling layer
        model.add(MaxPooling2D()) 
        
        # Flattened layer
        model.add(Flatten()) 
        
        
        
        # Dense layer
        model.add(Dense(      
                layer_dimension, 
                activation=layer_activation_function,
                input_shape=input_shape)
            )
        
        # Add the output layer
        model.add(Dense(
                    final_dimension)
                )

        #model.summary()

        optimizer = keras.optimizers.Adam(learning_rate=0.001, beta_1=0.9, beta_2=0.999, amsgrad=False)
        #optimizer = keras.optimizers.Adam(learning_rate=0.00001, beta_1=0.9, beta_2=0.999, amsgrad=False)
        
        model.compile(loss= loss_function,
                        optimizer = optimizer,
                        metrics=[metric])
              
        return model

def do_naivecnn(X, y, input_shape):
    m = Q3Model()
    m.set_data(X, y )
    type = 'cnn'    
    verbose = 1

    m.set_parameters(type = type,
                    epochs = 10,
                    input_shape = input_shape,
                    filters = 64,
                    kernel_size = 3,
                    layer_activation_function = 'relu',
                    dropout_prob = 0.2,
                    final_activation_function = 'softmax',
                    layer_dimension = 256,
                    loss_function = 'mean_squared_error',
                    optimizer = 'adam',
                    metric = 'mae'
                )

    if (type == 'mlp'):
        params = m.mlp_params
    else:
        params = m.cnn_params

    m.prepare_data(params, type=type)
    params.model = prepare_model_custom(params)
    params.model.summary()
    
    verbose = 1

    history = m.fit(params.model, params, verbose = verbose)
    return params.model, m, history

