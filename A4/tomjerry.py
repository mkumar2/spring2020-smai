import numpy as np
from skimage import io
import sklearn.metrics as metrics
import sys
from sklearn.decomposition import PCA
import numpy.linalg as linalg
import os
import csv
from sklearn.linear_model import LogisticRegression as sklogreg
from skimage.transform import rescale
import a4tjnn as q3
import transfertj as trtj
from keras import backend as K
import keras

verbose = 1

verboseprint = print if verbose else lambda *a, **k: None


# Generic functions




def show_metrics(testlabels, predicted_labels):
    print("Accuracy Score: ", metrics.accuracy_score(testlabels, predicted_labels))
    print("F1-score: ", metrics.f1_score(testlabels, predicted_labels, average='weighted'))
    print("Confusion Metrix: \n", metrics.confusion_matrix(testlabels, predicted_labels))


def read_testing_data_scaled(testfile, testing_path):
    # Read this file to get the file names 
    imglist = list()
    with open(testfile) as f:
        fdict = csv.DictReader(f)
        for dict in fdict:
            name = dict['image_file'] + '.jpg'
            fname = os.path.join(testing_path, name)
            img = io.imread(fname)
            img = rescale(img, scale)
            imglist.append(img)

    test_X0 = np.array(imglist)
    print("Shape of test_X0: ", test_X0.shape)
    test_X = test_X0.reshape(len(test_X0), -1)
    return test_X


# Read the test file and return X generated out of it. 

def read_testing_data(testfile, testing_path):
    # Read this file to get the file names 
    imglist = list()
    with open(testfile) as f:
        fdict = csv.DictReader(f)
        for dict in fdict:
            name = dict['image_file'] + '.jpg'
            fname = os.path.join(testing_path, name)
            img = io.imread(fname, as_gray = True)
            imglist.append(img)


    test_X0 = np.array(imglist)
    verboseprint("Shape of test_X0: ", test_X0.shape)
    test_X = test_X0.reshape(len(test_X0), -1)
    return test_X



class TJData:
    def __init__(self, training_file = './a3-q2/mj_train.txt', training_path = 'tj_ntrain'):
        self.trainfile = training_file
        self.training_path = training_path
        self.X = None
        self.y = None
        self.vX = None
        self.tX = None
        self.vy = None
        self.ty = None
        


    def read_training_file(self):
        # Read this file to get the file names and labels and create a dictionary 
        with open(self.trainfile) as f:
            dreader = csv.DictReader(f)
            fdict = {}
            for dict in dreader:
                    key = dict['image_file']
                    value = dict['emotion']
                    fdict[key.strip()] = value.strip() # Clean the string read from the file
            
            self.fdict = fdict

        #verboseprint (fdict)



    def read_training_images(self, scale = (0.25, 0.25, 1)):
        imglist = list()
        lablist = list()
        
        for k in self.fdict.keys():
            fname = os.path.join(self.training_path, k + ".jpg")
            #img = io.imread(fname, as_gray = True)
            img = io.imread(fname)
            img = rescale(img, scale)
            #io.imsave(os.path.join(self.training_path, k + "_sml.jpg"), (img*255).astype(np.uint8))

            label = self.fdict[k]
            imglist.append(img)
            lablist.append(label)

        X = np.array(imglist)
        verboseprint("Shape of X: ", X.shape)
        y = np.array(lablist)
        verboseprint("Shape of Y: ", y.shape) 
        self.X = X.reshape(len(X), -1)
        self.y = y
        


    def prepare_data(self, scale = 0.25):
        # Read the data first
        self.read_training_file()
        self.read_training_images()

    def split_data(self, splitsize = 0.2):
        if (splitsize == 0.0):
            self.tX = self.X
            self.ty = self.y
            verboseprint(self.ty.shape, 'training Y')
            verboseprint(self.tX.shape, 'training samples')

        else:
            VALIDATION_SET_SIZE = splitsize
            mask = np.random.rand(len(self.Xp)) <= VALIDATION_SET_SIZE
            self.vX = self.X[mask]
            self.tX = self.X[~mask]
            self.vy = self.y[mask]
            self.ty = self.y[~mask]
            verboseprint(self.tX.shape, 'training samples')
            verboseprint(self.vX.shape, 'validation samples')
            verboseprint(self.ty.shape, 'training Y')
            verboseprint(self.vy.shape, 'validation Y')


def write_output(filename, predicted_labels, header = None):
    wfile = open(filename, 'w')
    if (header != None):
        wfile.write(header + '\n')
    for i in range(len(predicted_labels)):
        wstring = str(predicted_labels[i]) + '\n'
        wfile.write(wstring)

    wfile.close()


# to run: python ../tomjerry.py tj_train.csv tj_ntrain tj_test.csv tj_ntest

def write_output2(filename, actual_labels, predicted_labels, header = None):
    wfile = open(filename, 'w')
    if (header != None):
        wfile.write(header + '\n')
    for i in range(len(predicted_labels)):
        wstring =  str(actual_labels[i]) + ',' + str(predicted_labels[i]) + '\n'
        wfile.write(wstring)

    wfile.close()

def prep_for_cnn(X, y, 
                input_shape,
                pixelsize,
                classcount
                ):
 
    rowsize = input_shape[0]
    colsize = input_shape[1]
    channels = input_shape[2]
    
    featuresize = rowsize * colsize
    
    x_train = np.array(X)
    x_train = x_train.astype('float32')/pixelsize
    
    if K.image_data_format() == 'channels_first':
        x_train = x_train.reshape(x_train.shape[0], channels, rowsize, colsize)
        input_shape = (channels, rowsize, colsize)
    else:
        x_train = x_train.reshape(x_train.shape[0], rowsize, colsize, channels)
        input_shape = (rowsize, colsize, channels)
        

    y_train = keras.utils.to_categorical(y, classcount)
    return x_train, y_train

def do_evaluation(X, y, modellist, labellist):
    valuelist = list()
    for model in modellist:
        # predict using ith model
        predicted_values = model.predict_classes(X)
        valuelist.append(predicted_values)

    predtable = np.array(valueslist).T
    verboseprint("Shape of predictions table: ", predtable.shape)
    verboseprint("Labels: ", labellist)

    finallist = list()
    for row in predtable:
        i = row.argmax()
        finallist.append(labellist[i]) # The label corresponding to the index of max is inserted
    
    vscore = metrics.accuracy_score(m.training_labels, predicted_labels)
    print("Final Accuracy: ", vscore)
    return np.array(finallist).reshape(len(finallist))

def do_transfer_cnn(X, y, label, classcount, isTransferCNN = True):
    if (isTransferCNN == True):
        newX, newy = prep_for_cnn(X, y, (90, 160, 3), 255, classcount)
        model = trtj.run_trtj(newX, newy, classcount)
    else:
        m = do_mycnn(X, y, classcount)
        model = m.cnn_params.model
        newX = m.cnn_params.x_train
        newy = m.cnn_params.y_train 
    score = model.evaluate(newX, newy, verbose=1)
    print('\nLabel: ', label, ' loss:', score[0], 'accuracy:', score[1])
    return model     

def get_data():
    tj = TJData(
            training_file='datasets/tj_train.csv', 
            training_path='datasets/tj_ntrain'
            )
    tj.prepare_data()
    tj.split_data(0.0)
    X = tj.X
    y = (tj.y).astype(int)
    return X, y, tj

def do_mycnn(X, y, classcount):
    
    m = q3.Q3Model()
    m.set_data(X, y )
    type = 'cnn'    
    verbose = 1

    m.set_parameters(type = type,
                    epochs = 2,
                    input_shape = (90, 160, 3),
                    pixelsize = 255,
                    classcount = 2,
                    filters = 32,
                    kernel_size = 3,
                    layer_activation_function = 'relu',
                    dropout_prob = 0.2,
                    final_activation_function = 'softmax',
                    layer_dimension = 64,
                    loss_function = 'categorical_crossentropy',
                    optimizer = 'adam',
                    metric = 'accuracy'
                )

    if (type == 'mlp'):
        params = m.mlp_params
    else:
        params = m.cnn_params

    m.prepare_data(params, type=type)
    params.model = q3.prepare_model_custom(params)
    params.model.summary()
    
    verbose = 1

    history = m.fit(params.model, params, verbose = verbose)
    return m


def do_learning_loop(X, y, isTransferCNN = True):
    # We want to train a model for each of the label. 
    labellist = list(np.unique(y))
    modellist = list()
    lval = 1

    classcount = 2

    for label in labellist:
        newy = np.zeros(y.shape) # Create a new array of same length
        for i in range(len(y)):
            if (y[i] == label):
                newy[i] = lval
            
        model = do_transfer_cnn(X, newy, label, classcount, isTransferCNN)
        modellist.append(model)
    return labellist, modellist, classcount

def run_all_transfer():
    X, y, tj = get_data()

    classcount = 5
    model = do_transfer_cnn(X, y, 'all', classcount)
        
    newX, newy = prep_for_cnn(X, y, (90, 160, 3), 255, classcount)
    
    return newX, newy, y, modellist, labellist, tj

    #do_evaluation(newX, y, modellist, labellist)


def run_transfer():
    X, y, tj = get_data()

    labellist, modellist, classcount = do_learning_loop(X, y)

    newX, newy = prep_for_cnn(X, y, (90, 160, 3), 255, classcount)
    
    return newX, newy, y, modellist, labellist, tj

    #do_evaluation(newX, y, modellist, labellist)


#run_transfer()

def run_mycnn():
    X, y, tj = get_data()

    labellist, modellist, classcount = do_learning_loop(X, y, isTransferCNN=False)

def run_cnn():
    # Read the images
    tj = TJData(
            training_file='datasets/tj_train.csv', 
            training_path='datasets/tj_ntrain'
            )
    tj.prepare_data()
    tj.split_data(0.0)

    m = q3.Q3Model()
    X = tj.X
    y = (tj.y).astype(int)
    # WE will try if binary classification is better
    label = 1
    notlabel = 0
    for i in range(len(y)):
        if (y[i] != label):
            y[i] = notlabel

    # We have relabelled to a binary classification. Let's see if we get something better

    m.set_data(X, y )
    type = 'cnn'    
    m.set_parameters(type = type,
                    epochs = 2,
                    input_shape = (90, 160, 3),
                    pixelsize = 255,
                    classcount = 2,
                    filters = 32,
                    kernel_size = 3,
                    layer_activation_function = 'relu',
                    dropout_prob = 0.2,
                    final_activation_function = 'softmax',
                    layer_dimension = 64,
                    loss_function = 'categorical_crossentropy',
                    optimizer = 'adam',
                    metric = 'accuracy'
                )

    if (type == 'mlp'):
        params = m.mlp_params
    else:
        params = m.cnn_params

    m.prepare_data(params, type=type)
    params.model = q3.prepare_model_custom(params)
    params.model.summary()
    
    verbose = 1

    history = m.fit(params.model, params, verbose = verbose)
    
    score = params.model.evaluate(params.x_train, params.y_train, verbose=verbose)
    print('Training loss:', score[0])
    print('Training accuracy:', score[1])     
    predicted_labels = params.model.predict_classes(params.x_train)
    print(m.training_labels)
    print(predicted_labels)
    #write_output2("testfile.csv", m.training_labels, predicted_labels, "actual,predicted")
    vscore = metrics.accuracy_score(m.training_labels, predicted_labels)
    #f1score = metrics.f1_score(m.training_labels, predicted_labels)

    print(vscore)

#run_cnn()