'''
We need to prepare the data. Since we are given date and time, we should break down the x values into the 
components: year, month, week, day_of_year, day_of_month, day_of_week, hour, minute

'''


# References: 
# https://machinelearningmastery.com/tensorflow-tutorial-deep-learning-with-tf-keras/


import numpy as np 
import pandas as pd 
import time
from sklearn.preprocessing import MinMaxScaler
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras.optimizers import RMSprop
from keras import backend as K




class MLPModel:
    def __init__(self, x_data, y_data):
        self.model = None # The keras model that is setup
        self.batch_size = 128
        self.epochs = 20
        self.layer_dimension = 120
        self.final_dimension = 1 
        self.hidden_layers = 5
        self.loss_function = 'mean_absolute_error'
        self.optimizer = None
        self.metric = 'mean_squared_error'
        self.layer_activation_function = 'relu'
        self.final_activation_function = 'linear' # For regression
        self.dropout_prob = 0.2
        self.input_shape = None
        self.verbose = 1
        self.scaler_x = None
        self.x_data = x_data
        self.y_data = y_data
        
        
    def set_parameters(self, hidden_layers = 1, 
                            activation_function = 'relu', 
                            dropout_prob = 0.2,
                            epochs = 10
                            ):
    
        # Set various parameters to tune the MLP
        self.hidden_layers = hidden_layers
        self.layer_activation_function = activation_function
        self.dropout_prob = dropout_prob
        self.optimizer =  'adam'  # 'RMSprop()'
        
    def prepare_data(self):
        
        (samplesize, featuresize) = self.x_data.shape

        scaler_x = MinMaxScaler()
        
        xscale = scaler_x.fit_transform(self.x_data)
        yscale = self.y_data
        self.input_shape = (featuresize,)
        self.scaler_x = scaler_x

        return (xscale, yscale)
        
        # Split into training and validation
        
        
    def add_mlp_layers(self, model, hidden_layers, 
                        layer_dimension, layer_activation_function, 
                        input_shape, dropout_prob):
    
        #print("Adding Layers: ", hidden_layers + 1)
        for i in range(hidden_layers + 1): # Add 1 because we have to also add input layer
                #print("--- Adding dense layer")
                model.add(Dense(
                        layer_dimension, 
                        activation=layer_activation_function,
                        input_shape=input_shape)
                    )
                if (dropout_prob > 0):
                        #print("--- Adding dropout layer")
                        model.add(Dropout(dropout_prob))
        
        #print("Done adding layers")
                

    def prepare_model(self):

        model = Sequential()
        # add input and hidden layers
        self.add_mlp_layers(
                            model,
                            self.hidden_layers, 
                            self.layer_dimension, 
                            self.layer_activation_function,
                            self.input_shape,
                            self.dropout_prob
                        )
                

        # Add the output layer
        model.add(Dense(
                    self.final_dimension, 
                    activation=self.final_activation_function)
                )

        #print("Also added final layer")
                
        model.summary()

        model.compile(loss= self.loss_function,
                        optimizer = self.optimizer,
                        metrics=[self.metric])

        self.model = model
              
        return model
    
    def fit (self):
        
        self.model.fit( self.x_data, self.y_data,
                    batch_size=self.batch_size,
                    epochs=self.epochs,
                    verbose=self.verbose,
                    validation_split=0.2
                )
    
    def predict(self, testdata):
        scaledtestdata = self.scaler_x.transform(testdata)
        predicted_labels = self.model.predict(scaledtestdata)
        return predicted_labels
    



        



