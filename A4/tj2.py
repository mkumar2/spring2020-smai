import numpy as np
from skimage import io
import sklearn.metrics as metrics
import sys
from sklearn.decomposition import PCA
import numpy.linalg as linalg
import os
import csv
from sklearn.linear_model import LogisticRegression as sklogreg
from skimage.transform import rescale
import a4tjnn as q3
import transfertj as trtj
from keras import backend as K
import keras

verbose = 1

verboseprint = print if verbose else lambda *a, **k: None


# Generic functions




def show_metrics(testlabels, predicted_labels):
    print("Accuracy Score: ", metrics.accuracy_score(testlabels, predicted_labels))
    print("F1-score: ", metrics.f1_score(testlabels, predicted_labels, average='weighted'))
    print("Confusion Metrix: \n", metrics.confusion_matrix(testlabels, predicted_labels))


def read_testing_data_scaled(testfile, testing_path, scale = (0.25, 0.25, 1), as_gray = None):
    # Read this file to get the file names 
    imglist = list()
    with open(testfile) as f:
        fdict = csv.DictReader(f)
        for dict in fdict:
            name = dict['image_file'] + '.jpg'
            fname = os.path.join(testing_path, name)
            img = io.imread(fname)
            img = rescale(img, scale)
            imglist.append(img)

    test_X0 = np.array(imglist)
    print("Shape of test_X0: ", test_X0.shape)
    test_X = test_X0.reshape(len(test_X0), -1)
    return test_X


# Read the test file and return X generated out of it. 

def read_testing_data(testfile, testing_path):
    # Read this file to get the file names 
    imglist = list()
    with open(testfile) as f:
        fdict = csv.DictReader(f)
        for dict in fdict:
            name = dict['image_file'] + '.jpg'
            fname = os.path.join(testing_path, name)
            img = io.imread(fname, as_gray = as_gray)
            imglist.append(img)


    test_X0 = np.array(imglist)
    verboseprint("Shape of test_X0: ", test_X0.shape)
    test_X = test_X0.reshape(len(test_X0), -1)
    return test_X



class TJData:
    def __init__(self, training_file = './a3-q2/mj_train.txt', training_path = 'tj_ntrain'):
        self.trainfile = training_file
        self.training_path = training_path
        self.X = None
        self.y = None
        self.vX = None
        self.tX = None
        self.vy = None
        self.ty = None
        


    def read_training_file(self):
        # Read this file to get the file names and labels and create a dictionary 
        with open(self.trainfile) as f:
            dreader = csv.DictReader(f)
            fdict = {}
            for dict in dreader:
                    key = dict['image_file']
                    value = dict['emotion']
                    fdict[key.strip()] = value.strip() # Clean the string read from the file
            
            self.fdict = fdict

        #verboseprint (fdict)



    def read_training_images(self, scale = (0.25, 0.25, 1), as_gray = None):
        imglist = list()
        lablist = list()
        
        for k in self.fdict.keys():
            fname = os.path.join(self.training_path, k + ".jpg")
            img = io.imread(fname, as_gray = as_gray)
            img = rescale(img, scale)
            #io.imsave(os.path.join(self.training_path, k + "_sml.jpg"), (img*255).astype(np.uint8))

            label = self.fdict[k]
            imglist.append(img)
            lablist.append(label)

        X = np.array(imglist)
        verboseprint("Shape of X: ", X.shape)
        y = np.array(lablist)
        verboseprint("Shape of Y: ", y.shape) 
        self.X = X.reshape(len(X), -1)
        self.y = y
        return X.shape # This is the image shape
        


    def prepare_data(self, scale = (0.25, 0.25, 1)):
        # Read the data first
        self.read_training_file()
        image_shape = self.read_training_images(scale)
        return image_shape

    def split_data(self, splitsize = 0.2):
        if (splitsize == 0.0):
            self.tX = self.X
            self.ty = self.y
            verboseprint(self.ty.shape, 'training Y')
            verboseprint(self.tX.shape, 'training samples')

        else:
            VALIDATION_SET_SIZE = splitsize
            mask = np.random.rand(len(self.Xp)) <= VALIDATION_SET_SIZE
            self.vX = self.X[mask]
            self.tX = self.X[~mask]
            self.vy = self.y[mask]
            self.ty = self.y[~mask]
            verboseprint(self.tX.shape, 'training samples')
            verboseprint(self.vX.shape, 'validation samples')
            verboseprint(self.ty.shape, 'training Y')
            verboseprint(self.vy.shape, 'validation Y')


def write_output(filename, predicted_labels, header = None):
    wfile = open(filename, 'w')
    if (header != None):
        wfile.write(header + '\n')
    for i in range(len(predicted_labels)):
        wstring = str(predicted_labels[i]) + '\n'
        wfile.write(wstring)

    wfile.close()


# to run: python ../tomjerry.py tj_train.csv tj_ntrain tj_test.csv tj_ntest

def write_output2(filename, actual_labels, predicted_labels, header = None):
    wfile = open(filename, 'w')
    if (header != None):
        wfile.write(header + '\n')
    for i in range(len(predicted_labels)):
        wstring =  str(actual_labels[i]) + ',' + str(predicted_labels[i]) + '\n'
        wfile.write(wstring)

    wfile.close()

def prep_y_for_cnn(y, classcount):
    y_train = keras.utils.to_categorical(y, classcount)
    return y_train

def prep_X_for_cnn(X, 
                input_shape,
                pixelsize
                ):
 
    rowsize = input_shape[0]
    colsize = input_shape[1]
    channels = input_shape[2]
    
    featuresize = rowsize * colsize
    
    x_train = np.array(X)
    x_train = x_train.astype('float32')/pixelsize
    
    if K.image_data_format() == 'channels_first':
        x_train = x_train.reshape(x_train.shape[0], channels, rowsize, colsize)
        input_shape = (channels, rowsize, colsize)
    else:
        x_train = x_train.reshape(x_train.shape[0], rowsize, colsize, channels)
        input_shape = (rowsize, colsize, channels)
        

    return x_train



def get_data(training_file, training_path):
    tj = TJData(
            training_file=training_file, 
            training_path=training_path
            )
    image_shape = tj.prepare_data()
    tj.split_data(0.0)
    X = tj.X
    y = (tj.y).astype(int)
    return X, y, tj, image_shape

def predicted_class(predicted_values):
    pred_list = list()
    for row in predicted_values:
        i = row.argmax()
        pred_list.append(i)
    return np.array(pred_list).reshape(len(pred_list)) # Return a 1-D numpy array


def run_all_transfer(
                        mode = 'naive',
                        training_file='datasets/tj_train.csv', 
                        training_path='datasets/tj_ntrain',
                        test_file='datasets/tj_test.csv',
                        test_path='datasets/tj_ntest',
                        outfile = "tj_submission.csv"
                    ):
    X, y, tj, input_shape = get_data(training_file, training_path)

    classcount = 5
    pixelsize = 255
    input_shape = (input_shape[1], input_shape[2], input_shape[3])

    m = None

    if (mode == 'naive'):
        model, m = q3.do_naivecnn(X, y, input_shape, pixelsize, classcount)
    else:
        newX = prep_X_for_cnn(X, input_shape, pixelsize)
        newy = prep_y_for_cnn(y, classcount)
        model = trtj.run_trtj(newX, newy, classcount)

    # Read test data
    test_X = read_testing_data_scaled(test_file, test_path)
    newX = prep_X_for_cnn(test_X, input_shape, pixelsize)
    predicted_values = model.predict(newX)
    final_prediction = predicted_class(predicted_values)
    write_output(outfile, final_prediction, 'emotion')
    return model, m, X, y
    
    #return X, y, newX, newy, test_X, image_shape, predicted_values, final_prediction, tj, model

    #do_evaluation(newX, y, modellist, labellist)

#run_all_transfer(mode = 'naive')