import cv2
import csv
import os
from skimage import io
from skimage.transform import rescale


import numpy as np
#img = cv2.imread('datasets/tj_ntest/IMG_10000013f.jpg')

def remove_bg(img, ofname):

    gray_img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    _, thresh = cv2.threshold(gray_img, 127, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    img_contours = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[-2]

    if ((img_contours is None) or (len(img_contours) == 0)):
        retval = cv2.imwrite(ofname, img)
        print("\n", ofname, "Result = ", retval)
        return img
    
    img_contours = sorted(img_contours, key=cv2.contourArea)

    for i in img_contours:

        if cv2.contourArea(i) > 100:

            break
    mask = np.zeros(img.shape[:2], np.uint8)
    cv2.drawContours(mask, [i],-1, 255, -1)
    new_img = cv2.bitwise_and(img, img, mask=mask)
    #cv2.imshow("Original Image", img)
    #cv2.imshow("Image with background removed", new_img)
    retval = cv2.imwrite(ofname, new_img)
    print("\n", ofname, "Result = ", retval)
    return retval

def preprocess_images(imgfile, imgpath, output_path, scale, as_gray = None):
    # Read this file to get the file names and labels and create a dictionary 
    with open(imgfile) as f:
        dreader = csv.DictReader(f)
        for dict in dreader:
            fname = dict['image_file']
            ifname = os.path.join(imgpath, fname + ".jpg")
            ofname = os.path.join(output_path, fname + ".jpg")
            img = cv2.imread(ifname)
            #img = remove_bg(img)
            #img = rescale(img, scale)
            remove_bg(img, ofname)
            
        
        return 

    #verboseprint (fdict)

'''
preprocess_images('datasets/tj_train.csv', 
                    'datasets/tj_ntrain', 
                    'datasets/tj_ntrain_out', 
                    scale = (0.25, 0.25, 1),
                    as_gray=True)
'''
preprocess_images('datasets/tj_test.csv', 
                    'datasets/tj_ntest', 
                    'datasets/tj_ntest_out', 
                    scale = (0.25, 0.25, 1),
                    as_gray=True)
