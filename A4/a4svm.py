"""
https://www.aicrowd.com/challenges/htspc-hate-speech-classification

"""

import numpy as np
import time
from sklearn import svm
import pandas as pd

class SVMClassifier:
    def __init__(self, tX, tlabels):
        self.kernel = None
        self.clf = None
        self.tX = tX
        self.tlabels = tlabels
        
    
    def predict(self, testdata):
        time2 = time.time()
        predicted_labels = self.clf.predict(testdata)
        time3 = time.time()
        predictduration = time3 - time2
        return predicted_labels, predictduration

    def fit(self, kernel = 'rbf', degree = 3):
        # kernel has to be one of ‘linear’, ‘poly’, ‘rbf’, ‘sigmoid’
        time1 = time.time()
        clf = svm.SVC(kernel=kernel, random_state = 0)
        clf.fit(self.tX, self.tlabels)
        self.clf = clf
        time2 = time.time()
        fitduration = time2 - time1
        return fitduration



