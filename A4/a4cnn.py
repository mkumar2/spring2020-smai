# References: 
# https://machinelearningmastery.com/tensorflow-tutorial-deep-learning-with-tf-keras/


import numpy as np

import keras


from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D, Conv1D, MaxPooling1D
from keras.optimizers import RMSprop
from keras import backend as K



class CNNModel:
    def __init__(self, x_data, y_data):
        self.model = None # The keras model that is setup
        self.batch_size = 64
        self.epochs = 2
        self.layer_dimension = 128
        self.final_dimension = 10 
        self.hidden_layers = 5
        self.loss_function = 'categorical_crossentropy'
        self.optimizer = None
        self.metric = 'accuracy'
        self.layer_activation_function = 'relu'
        self.final_activation_function = 'softmax'
        self.featuresize = 784
        self.dropout_prob = 0.2
        self.filters = 32 # should this be 64?
        self.kernel_size = 3
        self.max_pooling = True
        self.input_shape = None
        self.x_data = x_data
        self.y_data = y_data
        self.is2D = None
        self.shape2D = None


    '''
    Parameters for MLP and CNN: 
    1. # of hidden layers
    2. Activation function in hidden layers
    3. # of output dimension in hidden layers
    4. different frame sizes

    '''
    def set_parameters(self, 
                            cnn_layers = 2, 
                            dense_layers = 1, 
                            outdim = 512, 
                            isRegression = True,
                            hidden_layers = 1, 
                            layer_activation_function = 'relu', 
                            dropout_prob = 0.2,
                            optimizer = 'adam',
                            final_activation_function = 'linear',
                            loss_function = 'mean_absolute_error',
                            metric = 'mean_squared_error',
                            category_count = 1,
                            epochs = 10,
                            is2D = False,
                            shape2D = None
                            ):
    
        # Set various parameters to tune the MLP
        self.hidden_layers = hidden_layers
        self.layer_activation_function = layer_activation_function
        self.dropout_prob = dropout_prob
        self.optimizer =  optimizer
        self.final_activation_function = final_activation_function
        self.loss_function = loss_function
        self.metric = metric
        self.epochs = epochs
        self.isRegression = isRegression
        self.category_count = category_count
        self.cnn_layers = cnn_layers
        self.dense_layers = dense_layers
        self.layer_dimension = outdim
        self.is2D = is2D
        self.shape2D = shape2D



    def prepare_data(self):
        
        (samplesize, featuresize) = self.x_data.shape
        print("Prepare_data: X Data shape is ", self.x_data.shape)

        if (self.is2D == True):
            x_data = self.x_data.reshape(samplesize, self.shape2D[0], self.shape2D[1], 1)
            input_shape = (self.shape2D[0], self.shape2D[1], 1)
        else:
            x_data = self.x_data.reshape(samplesize, featuresize, 1)
            input_shape = (featuresize, 1)

            
        if (self.isRegression == False):
            # For categorical, we have to convert Y to matrix
            y_data = keras.utils.to_categorical(self.y_data, self.category_count)
            self.final_dimension = self.category_count
            
        else:
            y_data = self.y_data
            self.final_dimension = 1
        
        self.x_data = x_data
        self.y_data = y_data
        self.input_shape = input_shape

        



    def add_mlp_layers(self, model, hidden_layers, 
                        layer_dimension, layer_activation_function, 
                        input_shape, dropout_prob):
    
        #print("Adding Layers: ", hidden_layers + 1)
        for i in range(hidden_layers + 1): # Add 1 because we have to also add input layer
                #print("--- Adding dense layer")
                model.add(Dense(
                        layer_dimension, 
                        activation=layer_activation_function,
                        input_shape=input_shape)
                    )
                if (dropout_prob > 0):
                        #print("--- Adding dropout layer")
                        model.add(Dropout(dropout_prob))
        
        #print("Done adding layers")
                

    def add_cnn_layers(self, model, cnn_layers, dense_layers, filters, kernel_size,  
                        layer_dimension, layer_activation_function, input_shape, dropout_prob,
                        max_pooling):

        if (self.is2D == False):
            fconv = Conv1D
            fpool = MaxPooling1D
        else:
            fconv = Conv2D
            fpool = MaxPooling2D


        
        for i in range(cnn_layers):
            model.add(fconv(filters, kernel_size, padding='same',
                            activation=layer_activation_function,
                            input_shape=input_shape))
            if (max_pooling == True):
                model.add(fpool())
            if (dropout_prob > 0):
                model.add(Dropout(dropout_prob))


        model.add(Flatten())

        self.add_mlp_layers(
                        model, 
                        dense_layers, 
                        layer_dimension, 
                        layer_activation_function, 
                        input_shape, 
                        dropout_prob
                    )



    def prepare_model(self):
        

        model = Sequential()
        # add input and hidden layers
        self.add_cnn_layers(
                            model,
                            self.cnn_layers, 
                            self.dense_layers, 
                            self.filters, 
                            self.kernel_size, 
                            self.layer_dimension, 
                            self.layer_activation_function,
                            self.input_shape,
                            self.dropout_prob,
                            self.max_pooling 
                        )
                

        # Add the output layer
        model.add(Dense(
                    self.final_dimension, 
                    activation=self.final_activation_function)
                )

        #print("Also added final layer")
                
        #model.summary()

        model.compile(loss= self.loss_function,
                        optimizer = self.optimizer,
                        metrics=[self.metric])
              
        self.model = model
        return model
    
    def fit (self, verbose = 1):
        history = self.model.fit(self.x_data, self.y_data,
            batch_size=self.batch_size,
            epochs=self.epochs,
            verbose=verbose,
            validation_split=0.2)
        
        return history

    def predict(self, testdata):
        scaledtestdata = testdata.reshape(testdata.shape[0], 
                                    self.x_data.shape[1], 
                                    self.x_data.shape[2])    
        
        if (self.isRegression == True):
            predicted_labels = self.model.predict(scaledtestdata)
        else:
            predicted_labels = self.model.predict_classes(scaledtestdata)
        return predicted_labels

    def get_score(self, x_data, y_data, verbose=0):
        score = self.model.evaluate(x_data, y_data, verbose=0)
        print('Test loss:', score[0])
        print('Test accuracy:', score[1])     


    
