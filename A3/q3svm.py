from sklearn.svm import SVC
import sklearn.metrics as metrics
from mnist import MNIST
import numpy as np
import time as time


class SVMClassifier:
    def __init__(self, path = './mnist-data'):
        self.rowsize = 28
        self.colsize = 28
        self.trainingcount = 60000
        self.testcount = 10000
        self.pixelsize = 255 
        self.classcount = 10
        self.validation_split = 0.2
        self.path = path
        self.training_images = None
        self.training_labels = None
        self.testing_images = None
        self.testing_labels = None
        self.featuresize = 784
        self.x_train = None
        self.x_test = None
        self.y_train = None
        self.y_test = None



    def load_mnist_data(self, topx):
        # Load training data
        mndata = MNIST(self.path)
        self.training_images, self.training_labels = mndata.load_training()
        self.testing_images, self.testing_labels = mndata.load_testing()

        print("Data loaded from file.")   
        featuresize = self.rowsize * self.colsize
        x_train = np.array(self.training_images).reshape(self.trainingcount, featuresize)
        x_test = np.array(self.testing_images).reshape(self.testcount, featuresize)

        x_train = x_train.astype('float32')/self.pixelsize
        x_test = x_test.astype('float32')/self.pixelsize

        y_train = np.array(self.training_labels)
        y_test = np.array(self.testing_labels)

        print("Xtrain: ", x_train.shape, "Ytrain: ", y_train.shape)
        print("Xtest: ", x_test.shape, "Ytest: ", y_test.shape)
        
        trlimit = round(topx * self.trainingcount)
        telimit = round(topx * self.testcount)
        
        self.featuresize = featuresize
        self.x_train = x_train[0:trlimit]
        self.y_train = y_train[0:trlimit]
        self.x_test = x_test[0:telimit]
        self.y_test = y_test[0:telimit]
        



def test_svm():

    m = SVMClassifier()
    t1 = time.time()
    m.load_mnist_data(5000)
    t2 = time.time()
    print("Load Time: ", round(t2-t1, 4))
    clf = SVC(decision_function_shape='ovo', kernel='linear', gamma='auto')
    clf.fit(m.x_train, m.y_train)
    t3 = time.time()
    print("Fit Time: ", round(t3-t2, 4))
    predicted_labels = clf.predict(m.x_test)
    t4 = time.time()
    print("Predict Time: ", round(t4-t3, 4))
    #print("Predicted labels: ", predicted_labels)
    print(metrics.accuracy_score(m.y_test, predicted_labels))

#test_svm()

def run_model(kernel = 'rbf', filter = 0.2):
    m = SVMClassifier()
    t1 = time.time()
    m.load_mnist_data(filter)
    t2 = time.time()
    print("Load Time: ", round(t2-t1, 4))
    clf = SVC(decision_function_shape='ovo', kernel=kernel, gamma='auto')
    clf.fit(m.x_train, m.y_train)
    t3 = time.time()
    print("Fit Time: ", round(t3-t2, 4))
    predicted_labels = clf.predict(m.x_test)
    t4 = time.time()
    print("Predict Time: ", round(t4-t3, 4))
    #print("Predicted labels: ", predicted_labels)
    print(metrics.accuracy_score(m.y_test, predicted_labels))

#run_model(filter=1)

