# References: 
# https://machinelearningmastery.com/tensorflow-tutorial-deep-learning-with-tf-keras/


from mnist import MNIST
import numpy as np

import keras


from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras.optimizers import RMSprop
from keras import backend as K



class Optimizer: 
    def __init__(self):
        self.name = 'RMSProp'
        self.clipnorm = 1
        self.clipvalue = 0.5
        self.learning_rate = 0.001
        self.momentum_weight = 0.1
        self.rho = 0.9 # only for RMSProp
        self.beta_1 = 0.9
        self.beta_2 = 0.999 

        

        
class SVMModel:
    def __init__(self):
        self.model = None # The keras model that is setup
        self.featuresize = 784
        self.x_train = None
        self.x_test = None
        self.y_train = None
        self.y_test = None
        self.input_shape = None

class MLPCNNModel:
    def __init__(self):
        self.model = None # The keras model that is setup
        self.batch_size = 128
        self.epochs = 2
        self.layer_dimension = 512
        self.final_dimension = 10 
        self.hidden_layers = 5
        self.loss_function = 'categorical_crossentropy'
        self.optimizer = None
        self.metric = 'accuracy'
        self.layer_activation_function = 'relu'
        self.final_activation_function = 'softmax'
        self.featuresize = 784
        self.dropout_prob = 0.2
        self.filters = 32 # should this be 64?
        self.kernel_size = (3, 3)
        self.max_pooling = True
        self.x_train = None
        self.x_test = None
        self.y_train = None
        self.y_test = None
        self.input_shape = None
        

    def show(self):
        print("----MLP Params-----\n")
        print("Batch size: ", self.batch_size)
        print("epochs: ", self.epochs)
        print("layer dimension: ", self.layer_dimension)
        print("final dimension: ", self.final_dimension)
        print("loss function", self.loss_function)
        print("optimizer: ", self.optimizer)
        print("metric: ", self.metric)
        print("Layer activation function: ", self.layer_activation_function)
        print("Final activation function: ", self.final_activation_function)
        print("Feature size: ", self.featuresize)
        print("Dropout Probability: ", self.dropout_prob)

    

        

class MNIST_IMAGEPARAMS: 
    def __init__(self):
        self.rowsize = 28
        self.colsize = 28
        self.trainingcount = 60000
        self.testcount = 10000
        self.pixelsize = 255 
        self.classcount = 10

    def show(self):
        print("----MNIST-----\n")
        print("Rowsize: ", self.rowsize)
        print("Colsize: ", self.colsize)
        print("Pixel size: ", self.pixelsize)
        print("Training Count: ", self.trainingcount)
        print("Test count: ", self.testcount)



class Q3Model:
    def __init__(self, path = './mnist-data'):
        self.mnist = MNIST_IMAGEPARAMS()
        self.validation_split = 0.2
        self.path = path
        self.training_images = None
        self.training_labels = None
        self.testing_images = None
        self.testing_labels = None
        self.mlp_params = None
        self.cnn_params = None
        self.svm_params = None

    def show(self):
        print("----MLP-----\n")
        self.print_image_stats(self.x_train)
        self.print_image_stats(self.x_test)
        self.print_label_stats(self.y_train)
        self.print_label_stats(self.y_test)
        print("Validation split: ", self.validation_split)
        print("Path: ", self.path)
        if (self.mlp_params != None): 
            self.mlp_params.show()
        if (self.cnn_params != None):
            self.mnist.show()


    def create_MLP(self):
        # Create MLP object
        self.mlp_params = MLPCNNModel()


    def print_image_stats(self, img):
        #npimg = np.array(img)
        npimg = img
        print("Shape: ", npimg.shape)
        print("Min: ", np.amin(npimg, axis= 1))
        print("Max: ", np.amax(npimg, axis= 1))

    def print_label_stats(self, label):
        print("Shape: ", label.shape)
        print("Min: ", np.amin(label))
        print("Max: ", np.amax(label))

    
        
        
        

    def load_mnist_data(self):
        
        # Load training data
        mndata = MNIST(self.path)
        self.training_images, self.training_labels = mndata.load_training()
        self.testing_images, self.testing_labels = mndata.load_testing()

    '''
    Parameters for MLP and CNN: 
    1. # of hidden layers
    2. Activation function in hidden layers
    3. # of output dimension in hidden layers
    4. different frame sizes

    '''
    def set_parameters(self, hidden_layers = 1, cnn_layers = 2, dense_layers = 1, 
                            activation_function = 'relu', 
                            outdim = 512, dropout_prob = 0.2,
                            epochs = 10,
                            optimizer = None, type = 'mlp'):
    
        params = MLPCNNModel()
        # Set various parameters to tune the MLP
        params.hidden_layers = hidden_layers
        params.cnn_layers = cnn_layers
        params.dense_layers = dense_layers
        params.layer_activation_function = activation_function
        params.layer_dimension = outdim
        params.dropout_prob = dropout_prob
        params.optimizer = optimizer

        if (type == 'mlp'):
            self.mlp_params = params
        else:
            self.cnn_params = params
        


    def set_optimizer(self):
        optimizer=RMSprop()
        return optimizer


    def prepare_data(self, params, type = 'mlp'):
        
        imgparams = self.mnist

        featuresize = imgparams.rowsize * imgparams.colsize
        x_train = np.array(self.training_images).reshape(imgparams.trainingcount, featuresize)
        x_test = np.array(self.testing_images).reshape(imgparams.testcount, featuresize)

        x_train = x_train.astype('float32')/imgparams.pixelsize
        x_test = x_test.astype('float32')/imgparams.pixelsize

        input_shape = (featuresize,)

        if (type == 'cnn'):
            if K.image_data_format() == 'channels_first':
                x_train = x_train.reshape(x_train.shape[0], 1, imgparams.rowsize, imgparams.colsize)
                x_test = x_test.reshape(x_test.shape[0], 1, imgparams.rowsize, imgparams.colsize)
                input_shape = (1, imgparams.rowsize, imgparams.colsize)
            else:
                x_train = x_train.reshape(x_train.shape[0], imgparams.rowsize, imgparams.colsize, 1)
                x_test = x_test.reshape(x_test.shape[0], imgparams.rowsize, imgparams.colsize, 1)
                input_shape = (imgparams.rowsize, imgparams.colsize, 1)
            

        print(x_train.shape[0], 'train samples')
        print(x_test.shape[0], 'test samples')
        
        y_train = keras.utils.to_categorical(self.training_labels, imgparams.classcount)
        y_test = keras.utils.to_categorical(self.testing_labels, imgparams.classcount)
        

        
        params.x_train = x_train
        params.x_test = x_test
        params.y_train = y_train
        params.y_test = y_test
        params.input_shape = input_shape



    def add_mlp_layers(self, model, hidden_layers, 
                        layer_dimension, layer_activation_function, 
                        input_shape, dropout_prob):
    
        #print("Adding Layers: ", hidden_layers + 1)
        for i in range(hidden_layers + 1): # Add 1 because we have to also add input layer
                #print("--- Adding dense layer")
                model.add(Dense(
                        layer_dimension, 
                        activation=layer_activation_function,
                        input_shape=input_shape)
                    )
                if (dropout_prob > 0):
                        #print("--- Adding dropout layer")
                        model.add(Dropout(dropout_prob))
        
        #print("Done adding layers")
                

    def add_cnn_layers(self, model, cnn_layers, dense_layers, filters, kernel_size,  
                        layer_dimension, layer_activation_function, input_shape, dropout_prob,
                        max_pooling):

        
        for i in range(cnn_layers):
            model.add(Conv2D(filters, kernel_size, padding='same',
                            activation=layer_activation_function,
                            input_shape=input_shape))
            if (max_pooling == True):
                model.add(MaxPooling2D())
            if (dropout_prob > 0):
                model.add(Dropout(dropout_prob))


        model.add(Flatten())

        self.add_mlp_layers(
                        model, 
                        dense_layers, 
                        layer_dimension, 
                        layer_activation_function, 
                        input_shape, 
                        dropout_prob
                    )



    def prepare_model(self, params, type = 'mlp'):
        

        model = Sequential()
        # add input and hidden layers
        if (type == 'mlp'):
            self.add_mlp_layers(
                                model,
                                params.hidden_layers, 
                                params.layer_dimension, 
                                params.layer_activation_function,
                                params.input_shape,
                                params.dropout_prob
                            )
        else:
            self.add_cnn_layers(
                                model,
                                params.cnn_layers, 
                                params.dense_layers, 
                                params.filters, 
                                params.kernel_size, 
                                params.layer_dimension, 
                                params.layer_activation_function,
                                params.input_shape,
                                params.dropout_prob,
                                params.max_pooling 
                            )
                

        # Add the output layer
        model.add(Dense(
                    params.final_dimension, 
                    activation=params.final_activation_function)
                )

        #print("Also added final layer")
                
        #model.summary()

        model.compile(loss= params.loss_function,
                        optimizer = params.optimizer,
                        metrics=[params.metric])
              
        return model
    
    def fit (self, model, params, type = "data", verbose = 0):
        if (type == "data"):
            data = (params.x_test, params.y_test)
            split = None
        else:
            data = None
            split = self.validation_split


        history = model.fit(params.x_train, params.y_train,
            batch_size=params.batch_size,
            epochs=params.epochs,
            verbose=verbose,
            validation_data=data,
            validation_split=split)
        
        return history
    
    def get_score(self, verbose=0):
        score = self.mlp_params.model.evaluate(self.x_test, self.y_test, verbose=0)
        print('Test loss:', score[0])
        print('Test accuracy:', score[1])     


    
def test_mlp():
    m = Q3Model()

    m.load_mnist_data()
    m.set_parameters(dropout_prob = 0.2, optimizer = m.set_optimizer())
    m.prepare_data(m.mlp_params)
    #m.show()
    m.mlp_params.model = m.prepare_model(m.mlp_params, type='mlp')

    m.fit(m.mlp_params.model, m.mlp_params)
    
def test_cnn(type = 'cnn'):
    m = Q3Model()
    

    m.load_mnist_data()
    m.set_parameters(dropout_prob = 0.2, optimizer = m.set_optimizer(), type=type)
    m.prepare_data(m.cnn_params, type=type)
    #m.show()
    m.cnn_params.model = m.prepare_model(m.cnn_params, type=type)

    m.fit(m.cnn_params.model, m.cnn_params)
    

def test_models(type = 'mlp', hidden_layers = 3, activation_function = 'relu', dropout_prob = 0.2):
    m = Q3Model()
    m.load_mnist_data()
    m.set_parameters(dropout_prob = dropout_prob, optimizer = m.set_optimizer(), type=type,
                        activation_function = activation_function,
                        hidden_layers=hidden_layers 
                    )
    if (type == 'mlp'):
        params = m.mlp_params
    else:
        params = m.cnn_params

    m.prepare_data(params, type=type)
    #m.show()
    params.model = m.prepare_model(params, type=type)

    m.fit(params.model, params)

def run_model(type, m, hidden_layers = 3, cnn_layers = 2, dense_layers = 1, 
                activation_function = 'relu', epochs = 10, dropout_prob = 0.2,
                verbose = 0):
    m.set_parameters(dropout_prob = dropout_prob, optimizer = m.set_optimizer(), type=type,
                        activation_function = activation_function,
                        cnn_layers=cnn_layers,
                        dense_layers=dense_layers,
                        hidden_layers=hidden_layers,
                        epochs = epochs 
                    )
    if (type == 'mlp'):
        params = m.mlp_params
    else:
        params = m.cnn_params

    m.prepare_data(params, type=type)
    #m.show()
    params.model = m.prepare_model(params, type=type)

    history = m.fit(params.model, params, verbose = verbose)
    score = params.model.evaluate(params.x_test, params.y_test)
    for (m, s) in zip(params.model.metrics_names, score):
        print(m, ": ", s)
    return history


# test_models('mlp', hidden_layers = 1, activation_function = 'relu', dropout_prob=0)
'''
m = Q3Model()
m.load_mnist_data()

run_model('mlp', m, hidden_layers = 1, activation_function = 'relu', dropout_prob=0)
#run_model('mlp', m, hidden_layers = 1, activation_function = 'relu', dropout_prob=0.2)
print("CNN Now. ")
run_model('cnn', m, cnn_layers = 2, dense_layers = 1, activation_function = 'relu', dropout_prob=0.2)
'''

def read_cmd():
    import sys
    optdict = dict()
    params = sys.argv[1:]
    if (len(params) >= 2):
        optdict["type"] = sys.argv[2]
        optdict["dir"] = sys.argv[1]
    elif (len(params) == 1):
        optdict["dir"] = sys.argv[1]
    else:
        print("Usage: ", sys.argv[0], '<directory_location>')
    
    print(optdict)
    
#read_cmd()
