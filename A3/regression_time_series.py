'''
We need to prepare the data. Since we are given date and time, we should break down the x values into the 
components: year, month, week, day_of_year, day_of_month, day_of_week, hour, minute

'''


# References: 
# https://machinelearningmastery.com/tensorflow-tutorial-deep-learning-with-tf-keras/


import numpy as np 
import pandas as pd 
import time
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from sklearn.preprocessing import MinMaxScaler
import os
import sys


# Following few lines are to suppress keras load-time messages and loading errors due to GPU
import logging
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'  # FATAL
logging.getLogger('tensorflow').setLevel(logging.FATAL)

stderr = sys.stderr
sys.stderr = open(os.devnull, 'w')
import keras
sys.stderr = stderr

from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D


printverbose = 0

verboseprint = print if printverbose else lambda *a, **k: None




def convert_power(val):
    if (val == '?'):
        return 'None'
    else:
        return val


class MLPModel:
    def __init__(self):
        self.model = None # The keras model that is setup
        self.batch_size = 128
        self.epochs = 20
        self.layer_dimension = 120
        self.final_dimension = 1 
        self.hidden_layers = 5
        self.loss_function = 'mean_absolute_error'
        self.optimizer = None
        self.metric = 'mean_squared_error'
        self.layer_activation_function = 'relu'
        self.final_activation_function = 'linear' # For regression
        self.dropout_prob = 0.2
        self.x_train = None
        self.x_test = None
        self.y_train = None
        self.y_test = None
        self.input_shape = None
        self.verbose = 1
        self.scaler_x = None
        
    def set_parameters(self, hidden_layers = 1, 
                            activation_function = 'relu', 
                            dropout_prob = 0.2,
                            epochs = 10,
                            learning_rate = 0.001
                            ):
    
        # Set various parameters to tune the MLP
        self.hidden_layers = hidden_layers
        self.layer_activation_function = activation_function
        self.dropout_prob = dropout_prob
        self.epochs = epochs
        optimizer = keras.optimizers.Adam(learning_rate=learning_rate, 
                            beta_1=0.9, beta_2=0.999, amsgrad=False)
        self.optimizer =  optimizer  

        
    def prepare_data(self, x_data, y_data):
        
        (samplesize, featuresize) = x_data.shape

        scaler_x = MinMaxScaler()
        
        xscale = scaler_x.fit_transform(x_data)
        yscale = y_data
        self.input_shape = (featuresize,)
        self.scaler_x = scaler_x

        return (xscale, yscale)
        
        # Split into training and validation
        
        
    def add_mlp_layers(self, model, hidden_layers, 
                        layer_dimension, layer_activation_function, 
                        input_shape, dropout_prob):
    
        #verboseprint("Adding Layers: ", hidden_layers + 1)
        for i in range(hidden_layers + 1): # Add 1 because we have to also add input layer
                #verboseprint("--- Adding dense layer")
                model.add(Dense(
                        layer_dimension, 
                        activation=layer_activation_function,
                        input_shape=input_shape)
                    )
                if (dropout_prob > 0):
                        #verboseprint("--- Adding dropout layer")
                        model.add(Dropout(dropout_prob))
        
        #verboseprint("Done adding layers")
                

    def prepare_model(self):

        model = Sequential()
        # add input and hidden layers
        self.add_mlp_layers(
                            model,
                            self.hidden_layers, 
                            self.layer_dimension, 
                            self.layer_activation_function,
                            self.input_shape,
                            self.dropout_prob
                        )
                

        # Add the output layer
        model.add(Dense(
                    self.final_dimension, 
                    activation=self.final_activation_function)
                )

        #verboseprint("Also added final layer")
                
        model.summary()

        model.compile(loss= self.loss_function,
                        optimizer = self.optimizer,
                        metrics=[self.metric])

        self.model = model
              
        return model
    
    def fit (self, trainingdata_x, trainingdata_y, validationdata_x, validationdata_y):
        
        data = (validationdata_x, validationdata_y)
        self.model.fit( trainingdata_x, trainingdata_y,
                    batch_size=self.batch_size,
                    epochs=self.epochs,
                    verbose=self.verbose,
                    #validation_data=data
                    validation_split=0.2
                )
    
    def predict(self, testdata):
        predicted_labels = self.model.predict(testdata)
        return predicted_labels
    
    def get_score(self, validationdata_x, validationdata_y):
        score = self.model.evaluate(validationdata_x, validationdata_y, verbose=0)
        verboseprint('Test loss:', score[0])
        verboseprint('Test accuracy:', score[1])     


class Q4Model:
    def __init__(self):
        self.validation_split = 0.2
        self.mlp_params = None
        self.le_params = None
        self.validationdata_x = None
        self.trainingdata_x = None
        self.validationdata_y = None
        self.trainingdata_y = None
        self.fulldata = None
        self.x_data = None
        self.y_data = None
        self.x_data_orig = None
        self.y_data_orig = None
        


    def read_powerdata(self, fname):
        df = pd.read_csv(fname, 
                                sep = ';',
                                usecols = [0, 1, 2],
                                infer_datetime_format = True, 
                                parse_dates=['Date'],
                                converters={"Global_active_power": convert_power},
                                dayfirst = True
                                )
        self.df = df
        return df


    def fill_missing(self):
        # Process missing data
        df = self.df
        dx = df['Global_active_power']
        df2 = dx.replace('None', value = None, method = 'pad')
        self.fulldata = np.array(df2, dtype='float64')
        
    
    def build_training_dataset(self, N=60):
        '''
        Each y value is predicted by last N values, # of past samples to consider
        so each of those become sort of feature dimensions. We now construct an X with shape (-1, N+1). 

        '''    
        s_data0 = self.fulldata
        colcount = N + 1 # (N features and 1 y value)
        asize0 = len(s_data0)
        alist = list()
        
        t1 = time.time()
            
        for i in range(colcount):
            beginindex = i
            asize = asize0 - i # The size of array we are operating with
            limit = asize - asize % (colcount)
            endindex = i + limit
            s_data = s_data0[beginindex:endindex]
            #verboseprint("Data:", i, asize, limit, beginindex, endindex, len(s_data))
            alist.append(s_data.reshape(-1, colcount))
        
        newds = np.concatenate(alist, axis = 0)
        t2 = time.time()
        verboseprint("Time taken to reshape data:  ", round(t2-t1))

        verboseprint("New DS shape: ", newds.shape)
        
        x_data = newds[:, 0:N].reshape(-1, N)
        y_data = newds[:, N].reshape(-1)
        
        verboseprint("x_data shape: ", x_data.shape)
        verboseprint("y_data shape: ", y_data.shape)
        
        self.x_data_orig = x_data
        self.y_data_orig = y_data

    def set_training_data(self, fname, N):
        # Read the file, fix missing data, and build the training and validation sets

        self.read_powerdata(fname)
        self.fill_missing()
        self.build_training_dataset(N)

    def split_data(self, x_data, y_data):
        VALIDATION_SET_SIZE = self.validation_split
        mask = np.random.rand(len(x_data)) <= VALIDATION_SET_SIZE
        self.validationdata_x = x_data[mask]
        self.trainingdata_x = x_data[~mask]
        self.validationdata_y = y_data[mask]
        self.trainingdata_y = y_data[~mask]
        
        verboseprint(self.trainingdata_x.shape, 'training samples')
        verboseprint(self.validationdata_x.shape, 'validation samples')
        verboseprint(self.trainingdata_y.shape, 'training Y')
        verboseprint(self.validationdata_y.shape, 'validation Y')

    def metrics(self, predicted_labels):

        verboseprint("Label Shape: ", predicted_labels.shape)
        verboseprint("Validation Y shape: ", self.validationdata_y.shape)
        loss = predicted_labels.reshape(len(self.validationdata_y)) - self.validationdata_y
        MSE = np.square(loss).mean(axis = 0)
        MAE = np.abs(loss).mean(axis = 0)
        if (0 in self.validationdata_y):
            verboseprint("Y has 0! MAPE not possible!")
        else:
            MAPE = np.abs(loss/self.validationdata_y).mean(axis = 0)

        R2Score = r2_score(self.validationdata_y, predicted_labels)
        verboseprint("RMSE: ", round(np.sqrt(MSE), 3),  "MAE: ", round(MAE, 3), "MAPE: ", round(MAPE, 3), "R2 Score: ", round(R2Score, 3) )
    
    def get_replacement(self, df, missing_location, N):
        #verboseprint("missing location: ", missing_location, " Type: ", type(missing_location))
        
        if (missing_location < N):
            #verboseprint("Filling a missing value through extrapolation, missing_location = ", missing_location)
            verboseprint("X for extrapolation: \n", df[0:missing_location])
            val = np.median(np.array(df[0:missing_location], dtype='float64'))
        else:
            # Estimate this using prediction
            predictiondata_x = df[missing_location-N:missing_location].reshape(1, -1) # Make it 2D
            verboseprint("X (for prediction): \n", predictiondata_x)
            predicted_labels = self.le_params.predict(predictiondata_x)
            val = predicted_labels[0]
        verboseprint("Missing Location: ", missing_location, "Replacement value: ", val)
        return val

    def predict_missing_values(self, N):
        '''
        This will do a second pass on the data, after fitment is done, so that all missing value
        are predicted. Here is the logic: 
        1. Read the first N numbers. If there are any missing, fill them like missing values. 
        2. Find all the locations of missing values and add to list, say L1
        3. For i in L1, form X[i-N,i] and use it to estimate, X[i]. 

        '''
        df = np.array(self.df['Global_active_power']).reshape(-1) # This is what we had read from the file, includes some missing values. 
     
        # Let's find the location of all the ? places. 
        pos = np.where(df == 'None')
        pos = pos[0] # pos is an array of array, so we need to get the inner array
        verboseprint(len(pos), "values are missing")
        verboseprint(pos)
        
        predictions = list()
        for i in range(len(pos)):
            # We need to fill the missing value. If pos[i] < N, then fill like a missing value, 
            # if pos[i] >= N, then fill like an estimate. 
            
            missing_location = pos[i]
            replacement = self.get_replacement(df, missing_location, N)
            df[missing_location] = replacement
            # Also keep it around so that we can print it on the console for output
            predictions.append(replacement)
        
        return predictions



        



class LinearClassifier:
    def __init__(self):
        self.reg = None

    def prepare_data(self, x_data, y_data):
        return (x_data, y_data)

        
    def fit(self, trainingdata_x, trainingdata_y):
        self.reg = LinearRegression().fit(trainingdata_x, trainingdata_y)
        
    
    def predict(self, validationdata_x):
        predicted_labels = self.reg.predict(validationdata_x)
        return predicted_labels



def test_lrmodel(fname = './household_power_consumption.txt', N = 60, do_prediction = False):
    verboseprint("Running Linear Regression Model with N = ", N)
    m = Q4Model()
    m.set_training_data(fname, N)
    m.le_params = LinearClassifier()
    # Let the model modify and update the data
    (x_data, y_data) = m.le_params.prepare_data(m.x_data_orig, m.y_data_orig)
    m.split_data(x_data, y_data)
    m.le_params.fit(m.trainingdata_x, m.trainingdata_y)
    predicted_labels = m.le_params.predict(m.validationdata_x)
    m.metrics(predicted_labels)
    # Run the prediction pass to predict the missing values
    if (do_prediction == True):
        predictions = m.predict_missing_values(N)
        verboseprint(len(predictions), "were predicted!")
        for prediction in predictions:
            print(round(prediction, 3))
    
    verboseprint("Linear Regression Model with N = ", N, " completed.")
    
def test_mlpmodel(fname = './household_power_consumption.txt', N = 60,
                    hidden_layers = 3, activation_function = 'relu',
                    dropout_prob = 0.2, epochs = 20, learning_rate = 0.001):
    verboseprint("Running MLP Model with N = ", N)
    m = Q4Model()
    m.set_training_data(fname, N)
    mlp_params = MLPModel()
    mlp_params.set_parameters(hidden_layers=hidden_layers, 
                                dropout_prob=dropout_prob, 
                                activation_function=activation_function,
                                epochs=epochs)
    # Let the model modify and update the data
    (x_data, y_data) = mlp_params.prepare_data(m.x_data_orig, m.y_data_orig)
    m.split_data(x_data, y_data)
    mlp_params.prepare_model()
    mlp_params.fit(m.trainingdata_x, m.trainingdata_y, m.validationdata_x, m.validationdata_y)
    predicted_labels = mlp_params.predict(mlp_params.scaler_x.transform(m.validationdata_x))
    m.metrics(predicted_labels)
    mlp_params.get_score(m.validationdata_x, m.validationdata_y)
    verboseprint("MLP Model with N = ", N, " completed.")

def final_run(fname):
    test_lrmodel(fname = fname, N = 60, do_prediction=True)

#test_lrmodel(fname = 'household_power_consumption - test.txt', 
            #N = 60, do_prediction = True) #RMSE:  0.258 0.258 MAE:  0.096 0.096 MAPE:  0.107 R2 Score:  0.94
#test_mlpmodel(N = 60) #RMSE:  1.396 1.396 MAE:  1.025 1.025 MAPE:  1.012 R2 Score:  -0.753    

def read_cmd_line():
    argcount = len(sys.argv)
    if  (argcount < 2):
        print("Usage: python regression_time_series.py <filename>")
    else:
        fname = sys.argv[1]
        final_run(fname)

read_cmd_line()

