# References: 
# https://machinelearningmastery.com/tensorflow-tutorial-deep-learning-with-tf-keras/


from mnist import MNIST
import numpy as np
from sklearn.preprocessing import MinMaxScaler


import keras


from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras.optimizers import RMSprop
from keras import backend as K



class MLPCNNModel:
    def __init__(self):
        self.model = None # The keras model that is setup
        self.batch_size = 128
        self.epochs = 2
        self.layer_dimension = 32
        self.final_dimension = 1 
        self.hidden_layers = 5
        self.loss_function = 'mean_absolute_error'
        self.optimizer = None
        self.metric = 'accuracy'
        self.layer_activation_function = 'relu'
        self.final_activation_function = 'linear' # For regression
        self.featuresize = 784
        self.dropout_prob = 0.2
        self.filters = 32 # should this be 64?
        self.kernel_size = (3, 3)
        self.max_pooling = True
        self.x_train = None
        self.x_test = None
        self.y_train = None
        self.y_test = None
        self.input_shape = None
        


class MNIST_IMAGEPARAMS: 
    def __init__(self):
        self.rowsize = 28
        self.colsize = 28
        self.trainingcount = 60000
        self.testcount = 10000
        self.pixelsize = 255 
        self.classcount = 10




class Q3Model:
    def __init__(self, path = './mnist-data'):
        self.mnist = MNIST_IMAGEPARAMS()
        self.validation_split = 0.2
        self.path = path
        self.training_images = None
        self.training_labels = None
        self.testing_images = None
        self.testing_labels = None
        self.mlp_params = None
        self.cnn_params = None
        self.svm_params = None


    def create_MLP(self):
        # Create MLP object
        self.mlp_params = MLPCNNModel()


        
        
        


    '''
    Parameters for MLP and CNN: 
    1. # of hidden layers
    2. Activation function in hidden layers
    3. # of output dimension in hidden layers
    4. different frame sizes

    '''
    def set_parameters(self, hidden_layers = 1, 
                            activation_function = 'relu', 
                            dropout_prob = 0.2,
                            epochs = 10,
                            optimizer = None, type = 'mlp'):
    
        params = MLPCNNModel()
        # Set various parameters to tune the MLP
        params.hidden_layers = hidden_layers
        params.layer_activation_function = activation_function
        params.dropout_prob = dropout_prob
        params.optimizer = optimizer

        if (type == 'mlp'):
            self.mlp_params = params
        else:
            self.cnn_params = params
        


    def set_optimizer(self):
        optimizer=RMSprop()
        return optimizer


    def prepare_data(self, params, x_data, y_data, type = 'mlp'):
        
        (samplesize, featuresize) = x_data.shape

        scaler_x = MinMaxScaler()
        scaler_y = MinMaxScaler()

        xscale = scaler_x.fit_transform(x_data)
        yscale = scaler_y.fit_transform(y_data)
        self.params.input_shape = (featuresize,)
        
        # Split into training and validation
        
        VALIDATION_SET_SIZE = 0.2
        mask = np.random.rand(len(xscale)) <= VALIDATION_SET_SIZE
        self.validationdata_x = xscale[mask]
        self.trainingdata_x = xscale[~mask]
        self.validationdata_y = yscale[mask]
        self.trainingdata_y = yscale[~mask]
        
        print(self.trainingdata_x.shape[0], 'training samples')
        print(self.validationdata_x.shape[0], 'validation samples')
        
    def add_mlp_layers(self, model, hidden_layers, 
                        layer_dimension, layer_activation_function, 
                        input_shape, dropout_prob):
    
        #print("Adding Layers: ", hidden_layers + 1)
        for i in range(hidden_layers + 1): # Add 1 because we have to also add input layer
                #print("--- Adding dense layer")
                model.add(Dense(
                        layer_dimension, 
                        activation=layer_activation_function,
                        input_shape=input_shape)
                    )
                if (dropout_prob > 0):
                        #print("--- Adding dropout layer")
                        model.add(Dropout(dropout_prob))
        
        #print("Done adding layers")
                

    def prepare_model(self, params, type = 'mlp'):
        

        model = Sequential()
        # add input and hidden layers
        self.add_mlp_layers(
                            model,
                            params.hidden_layers, 
                            params.layer_dimension, 
                            params.layer_activation_function,
                            params.input_shape,
                            params.dropout_prob
                        )
                

        # Add the output layer
        model.add(Dense(
                    params.final_dimension, 
                    activation=params.final_activation_function)
                )

        #print("Also added final layer")
                
        #model.summary()

        model.compile(loss= params.loss_function,
                        optimizer = params.optimizer,
                        metrics=[params.metric])
              
        return model
    
    def fit (self, model, params, type = "data", verbose = 0):
        if (type == "data"):
            data = (params.x_test, params.y_test)
            split = None
        else:
            data = None
            split = self.validation_split


        model.fit(params.x_train, params.y_train,
            batch_size=params.batch_size,
            epochs=params.epochs,
            verbose=verbose,
            validation_data=data,
            validation_split=split)
    
    
    def get_score(self, verbose=0):
        score = self.mlp_params.model.evaluate(self.x_test, self.y_test, verbose=0)
        print('Test loss:', score[0])
        print('Test accuracy:', score[1])     


def run_model(x_data, y_data, hidden_layers = 3, 
                activation_function = 'relu', 
                epochs = 10, 
                dropout_prob = 0.2
            ):

    type = 'mlp'
    '''
    We take in the data files - training and validation, and run the algorithm. To do that, we need
    to setup the parameter, prepare data and then setup the model to run fit. 

    '''
    m = Q3Model()
    
    m.set_parameters(dropout_prob = dropout_prob, optimizer = m.set_optimizer(), type=type,
                        activation_function = activation_function,
                        hidden_layers=hidden_layers,
                        epochs = epochs 
                    )

    params = m.mlp_params

    m.prepare_data(params, x_data, y_data, type=type)
    params.model = m.prepare_model(params, type=type)

    m.fit(params.model, params)
    m.params.score = params.model.evaluate(params.x_test, params.y_test)
    for (m, s) in zip(params.model.metrics_names, m.params.score):
        print(m, ": ", s)
    return m


# test_models('mlp', hidden_layers = 1, activation_function = 'relu', dropout_prob=0)
