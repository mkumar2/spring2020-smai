import numpy as np
import operator
import numpy as np
import numpy.linalg as linalg
import skimage.io as io
from sklearn.decomposition import PCA
from sklearn.metrics import mean_squared_error
import numpy as np


def eigensort(val, vec):
    idx = val.argsort()[::-1]   
    val = val[idx]
    vec = vec[:,idx]
    return val, vec

def apply_pca_optimized(X, comps = 25):
    pca = PCA(n_components=comps)
    pca.fit(X)
    newX = pca.transform(X)
    return pca, newX


def apply_pca(X0):

  n, m = X.shape
  # make it 0-centered
  X = X0 - X0.mean(axis = 0)
  #print("\n0-centered matrix: \n", X)
    
  # Compute covariance matrix
  C = np.matmul(X.T, X) / (n-1)
  #print("\nCovariance Matrix: \n", C)
  
  # Eigen decomposition
  eigenvalues, eigenvectors = np.linalg.eig(C)
  #print("\nEigenvalues: ", len(eigenvalues), "\nEigenvectors: \n", len(eigenvectors))

  newval, newvec = eigensort(eigenvalues, eigenvectors)
  #print("\nEigenVector Matrix P: \n", newvec)
  
  newX = np.matmul(X, newvec )  

  return newX

'''
To test: 
X = np.random.random((3,3))

Y = pca1(X)
print("\nTransformed Matrix: \n", Y)


'''

def imread_convert(f):
    return  io.imread(f, pilmode = 'L')


# Given the path, read all the images, and represent them as X. 

def read_images(path = './a3-q2/dataset'):
    # Read all the files

    #your path 
    col_dir = path + '/*.jpg'

    #creating a collection with the available images
    col = io.ImageCollection(col_dir, load_func=imread_convert)
    
    print("Images read: ", len(col))
    #io.imshow(col[0])
    #io.show()
    print(col[0], col[0].shape, col[0].dtype)
    X0 = np.array(col)
    print(X0.shape)
    X = X0.reshape(len(col), -1) # Reshape into a 2 dimensional array, with each pixel as a feature
    print(X.shape)
    print(X[0])
    return X

def transform_images(X):
    newX = apply_pca(X)

def rmse(X, newX):
    n1, m1 = X.shape
    n2, m2 = newX.shape
    
    z = np.zeros((n2,m1-m2), dtype='int64')
    newX2 = np.append(newX, z, axis = 1)
    #print("X: ", X.shape, "newX: ", newX.shape, "newX2: ", newX2.shape)
    mse = np.sqrt(mean_squared_error(X, newX2))
    return mse
    
def show_metric(X, newX):
    stat_rmse = rmse(X, newX)
    print("RMSE: ", stat_rmse)
    return stat_rmse

    

    


#read_images()

    '''
    print("\nX dimension before transform: ", X.shape)
    pca = PCA(n_components = 25)
    pca.fit(X)
    print("\nComponents Dimension after Fit: ", pca.components_.shape)
    newX = pca.transform(X)
    print("\nComponents Dimension after Transform: ", pca.components_.shape)
    print("\nX dimension after transform: ", newX.shape)

    #print("RMSE: ", rmse(X, newX))
    #print(pca.components_.shape)
    sum = 0
    for i in range(len(pca.explained_variance_ratio_)):
        ratio = round(pca.explained_variance_ratio_[i] * 100, 2) # in %
        sum = sum + ratio
        #print("\n", i, ratio, sum )

    XX = np.dot(newX, pca.components_)

    show_images(X, 1, 5) 
    show_images(XX, 1, 5) 
    show_images(pca.components_, 1, 5)

    '''