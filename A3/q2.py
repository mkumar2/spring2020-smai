import numpy as np
from skimage import io
import sklearn.metrics as metrics
import sys
from sklearn.decomposition import PCA
import numpy.linalg as linalg

verbose = 1

verboseprint = print if verbose else lambda *a, **k: None


# Generic functions


def eigensort(val, vec):
    idx = val.argsort()[::-1]   
    val = val[idx]
    vec = vec[:,idx]
    return val, vec

def apply_pca_optimized(X, comps = 25):
    pca = PCA(n_components=comps)
    pca.fit(X)
    newX = pca.transform(X)
    return pca, newX


def apply_pca(X0):

  n, m = X.shape
  # make it 0-centered
  X = X0 - X0.mean(axis = 0)
  #print("\n0-centered matrix: \n", X)
    
  # Compute covariance matrix
  C = np.matmul(X.T, X) / (n-1)
  #print("\nCovariance Matrix: \n", C)
  
  # Eigen decomposition
  eigenvalues, eigenvectors = np.linalg.eig(C)
  #print("\nEigenvalues: ", len(eigenvalues), "\nEigenvectors: \n", len(eigenvectors))

  newval, newvec = eigensort(eigenvalues, eigenvectors)
  #print("\nEigenVector Matrix P: \n", newvec)
  
  newX = np.matmul(X, newvec )  

  return newX



def sigmoid(scores):
    return 1 / (1 + np.exp(-scores))

def log_likelihood(features, target, weights):
    scores = np.dot(features, weights)
    ll = np.sum( target*scores - np.log(1 + np.exp(scores)) )
    return ll

def logistic_regression(features, target, iterations, learning_rate, add_intercept = False):
    intercept = np.ones((features.shape[0], 1))
    features = np.hstack((intercept, features))
        
    weights = np.zeros(features.shape[1])
    
    for step in range(iterations):
        scores = np.dot(features, weights)
        predictions = sigmoid(scores)

        # Update weights with log likelihood gradient
        output_error_signal = target - predictions
        
        gradient = np.dot(features.T, output_error_signal)
        weights += learning_rate * gradient

        # Print log-likelihood every so often
        if step % 10000 == 0:
            verboseprint ("Loss: ", log_likelihood(features, target, weights))
        
    return weights

def predicted_values(test_X, weights):
    final_scores = np.dot(np.hstack((np.ones((test_X.shape[0], 1)),
                                test_X)), weights) # Use concatenate or append. 
    predicted_values = np.round(sigmoid(final_scores))
    return predicted_values


def show_metrics(testlabels, predicted_labels):
    print("Accuracy Score: ", metrics.accuracy_score(testlabels, predicted_labels))
    print("F1-score: ", metrics.f1_score(testlabels, predicted_labels, average='weighted'))
    print("Confusion Metrix: \n", metrics.confusion_matrix(testlabels, predicted_labels))

# Read the test file and return X generated out of it. 
def read_testing_data(testfile):
    # Read this file to get the file names 
    imglist = list()
    with open(testfile) as f:
        for imgname in f:
            name = imgname.strip()
            img = io.imread(name, as_gray = True)
            imglist.append(img)

    test_X0 = np.array(imglist)
    verboseprint("Shape of test_X0: ", test_X0.shape)
    test_X = test_X0.reshape(len(test_X0), -1)
    return test_X



class LogisticRegression:
    def __init__(self, training_file = './a3-q2/mj_train.txt'):
        self.trainfile = training_file
        self.comps = None
        self.iterations = None
        self.X = None
        self.y = None
        self.Xp = None
        self.pca = None
        self.fdict = None
        self.learning_rate = None
        self.wtdict = None



    def read_training_file(self):
        # Read this file to get the file names and labels and create a dictionary 
        fdict = {}
        with open(self.trainfile) as f:
            for line in f:
                (key, val) = line.split()
                fdict[key.strip()] = val.strip() # Clean the string read from the file
        self.fdict = fdict

        #verboseprint (fdict)


    def read_training_images(self):
        imglist = list()
        lablist = list()
        
        for k in self.fdict.keys():
            img = io.imread(k, as_gray = True)
            
            label = self.fdict[k]
            imglist.append(img)
            lablist.append(label)

        X = np.array(imglist)
        #verboseprint("Shape of X: ", X.shape)
        y = np.array(lablist)
        verboseprint("Shape of Y: ", y.shape) 
        self.X = X.reshape(len(X), -1)
        self.y = y
        


    def prepare_data(self):
        # Read the data first
        self.read_training_file()
        self.read_training_images()

        self.pca, self.Xp = apply_pca_optimized(self.X, comps = self.comps)
        return self.Xp


    def set_params(self, iterations = 100000, learning_rate = 0.00001, n_comps = 50):
        self.iterations = iterations
        self.learning_rate = learning_rate
        self.comps = n_comps


    def fit(self):
        '''
        How do we fit multi-label?
        1. Find all the unique labels
        2. for each item in the list of labels, create a y such that it has 1 for the selected label, 0 otherwise
        3. For this y, learn the weights and store it against this label
        4. So fit generates a set of weights. 
        '''
        labellist = list(np.unique(self.y))
        verboseprint("Fit: Label List is ", labellist)
        wtdict = dict() # Store the pair of label and its learned weights

        for label in labellist:
            newy = (self.y == label).astype(int) # y corresponding to this label
            verboseprint("Fitting for label ", label)
            wtdict[label] = logistic_regression(self.Xp, newy,
                     iterations = self.iterations, learning_rate = self.learning_rate)

        self.wtdict = wtdict
        self.labellist = labellist


    
    def predict(self, test_X):
        # Check with all the labels. Pick the highest score

        wtdict = self.wtdict
        labellist = self.labellist
        valueslist = list()

        for label in labellist:
            weights = wtdict[label]
            # use this weight to predict
            pvalues = predicted_values(test_X, weights)
            valueslist.append(pvalues)

         # Now I have N predictions, N = number of labels. 

        predtable = np.array(valueslist).T
        verboseprint("Shape of predictions table: ", predtable.shape)
        verboseprint("Labels: ", labellist)

        finallist = list()
        for row in predtable:
            i = row.argmax()
            finallist.append(labellist[i]) # The label corresponding to the index of max is inserted

        return np.array(finallist).reshape(len(finallist)) # convert list into 1-D array and return


        

    def evaluate(self, train_X, train_y):
        predicted_labels = self.predict(train_X)
        show_metrics(train_y, predicted_labels)

    

def final_prediction(training_file, testing_file):
    # Use the training file to learn
    clf = LogisticRegression(training_file=training_file)
    clf.set_params()
    clf.prepare_data()
    clf.fit()
    test_X0 = read_testing_data(testing_file)
    test_X = clf.pca.transform(test_X0) # Transform using the same PCA model from training
    predicted_labels = clf.predict(test_X)
    for label in predicted_labels:
        print(label)


# final_prediction(training_file = './a3-q2/mj_train.txt', testing_file = './a3-q2/mj_test.txt')

def run_q2():
    clf = LogisticRegression()
    clf.prepare_data()
    clf.set_params()
    clf.fit()
    clf.evaluate(clf.Xp, clf.y)

#run_q2()

def read_cmd_line():
    argcount = len(sys.argv)
    if  (argcount < 3):
        print("Usage: python logistic_regression.py <training file name> <test file name>")
    else:
        training_file = sys.argv[1]
        testing_file = sys.argv[2]
        final_prediction(training_file, testing_file)

read_cmd_line()

