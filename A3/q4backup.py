'''
We need to prepare the data. Since we are given date and time, we should break down the x values into the 
components: year, month, week, day_of_year, day_of_month, day_of_week, hour, minute

'''
import numpy as np 
import pandas as pd 
import time
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score

def convert_power(val):
    if (val == '?'):
        return 'None'
    else:
        return val

class LinearClassifier:
    def __init__(self):
        self.df = None
        self.validationdata_x = None
        self.trainingdata_x = None
        self.validationdata_y = None
        self.trainingdata_y = None
        self.reg = None
        
    def read_powerdata(self, fname):
        df = pd.read_csv(fname, 
                                sep = ';',
                                usecols = [0, 1, 2],
                                infer_datetime_format = True, 
                                parse_dates=['Date'],
                                converters={"Global_active_power": convert_power},
                                dayfirst = True
                                )
        self.df = df
        return df

    '''
    def process_datetime(self, ds):
        """
        Date time needs these: absolute_time, day_of_year, day_of_week, month_of_year, hour_of_day, minute_of_hour
        
        """
        ds = pd.DatetimeIndex(ds, tz=None)
        #ds = pd.DataFrame(ds)
        
        year = ds.year
        month = ds.month
        week = ds.week
        day_of_year = ds.dayofyear
        day_of_week = ds.dayofweek
        
        newds = pd.DataFrame({'year': year, 
                                'month': month, 
                                'week': week, 
                                'day_of_year': day_of_year, 
                                'day_of_week': day_of_week 
                                })
        print(newds.shape)
        print(newds.iloc[0])

        return newds

    def process_data(self):
        df = self.df
        # Split the time col into hour and min
        df1 = df.iloc[:, 1].str.split(pat = ':', expand = True)
        
        print(df1.shape)
        newds = self.process_datetime(df['Date'])
        x_data = np.array(pd.concat([newds, df1.iloc[:,0:1]], axis = 1))
        
        # Process missing data
        dx = df['Global_active_power']
        df2 = dx.replace('None', value = None, method = 'pad')
        y_data = np.array(df2)

        
        VALIDATION_SET_SIZE = 0.2
        mask = np.random.rand(len(x_data)) <= VALIDATION_SET_SIZE
        self.validationdata_x = x_data[mask]
        self.trainingdata_x = x_data[~mask]
        self.validationdata_y = y_data[mask]
        self.trainingdata_y = y_data[~mask]

    '''

    def fill_missing(self):
        # Process missing data
        dx = df['Global_active_power']
        df2 = dx.replace('None', value = None, method = 'pad')
        return np.array(df2)
        
    def process_data(self):
        
        VALIDATION_SET_SIZE = 0.2
        mask = np.random.rand(len(x_data)) <= VALIDATION_SET_SIZE
        self.validationdata_x = x_data[mask]
        self.trainingdata_x = x_data[~mask]
        self.validationdata_y = y_data[mask]
        self.trainingdata_y = y_data[~mask]


    def build_regression_dataset(self, N):
        '''
        Each y value is predicted by last N values, # of past samples to consider
        so each of those become sort of feature dimensions. We now construct an X with shape (-1, N). 

        '''    
        y_data = self.fill_missing()
        for i in range(len(df)):
            


    def fit(self):
        self.reg = LinearRegression().fit(self.trainingdata_x, self.trainingdata_y)
        
    def predict(self):
        predicted_labels = self.reg.predict(self.validationdata_x)
        return predicted_labels

    def metrics(self, predicted_labels):
        regr = self.reg
        # The coefficients
        print('Coefficients: \n', regr.coef_)
        # The mean squared error
        print('Mean squared error: %.2f'
            % mean_squared_error(self.validationdata_y, predicted_labels))
        # The coefficient of determination: 1 is perfect prediction
        print('Coefficient of determination: %.2f'
            % r2_score(self.validationdata_y, predicted_labels))






'''

cls = LinearClassifier()
df = cls.read_powerdata('./household_power_consumption.txt')
cls.process_data()
cls.fit()
predicted_labels = cls.predict()
cls.metrics(predicted_labels)



print(df.dtypes)
print(df.iloc[0])

df1 = df.iloc[:, 1].str.split(pat = ':', expand = True)
print (df1.dtypes)
print(df1.iloc[0])
dx = df['Global_active_power']
print("Total Len: ", len(dx), "NA: ", len(dx) - dx.count())
print(dx.value_counts())
#df2 = dx.fillna(method = 'ffill')
df2 = dx.replace('None', value = None, method = 'pad')
print(df2.value_counts())
'''


        
        
