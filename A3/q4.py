'''
We need to prepare the data. Since we are given date and time, we should break down the x values into the 
components: year, month, week, day_of_year, day_of_month, day_of_week, hour, minute

'''


# References: 
# https://machinelearningmastery.com/tensorflow-tutorial-deep-learning-with-tf-keras/


import numpy as np 
import pandas as pd 
import time
from sklearn.linear_model import LinearRegression
from sklearn.metrics import mean_squared_error, r2_score, mean_absolute_error
from sklearn.preprocessing import MinMaxScaler
import keras
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Conv2D, MaxPooling2D
from keras.optimizers import RMSprop
from keras import backend as K





def convert_power(val):
    if (val == '?'):
        return 'None'
    else:
        return val


class MLPModel:
    def __init__(self):
        self.model = None # The keras model that is setup
        self.batch_size = 128
        self.epochs = 20
        self.layer_dimension = 120
        self.final_dimension = 1 
        self.hidden_layers = 5
        self.loss_function = 'mean_absolute_error'
        self.optimizer = None
        self.metric = 'mean_squared_error'
        self.layer_activation_function = 'relu'
        self.final_activation_function = 'linear' # For regression
        self.dropout_prob = 0.2
        self.x_train = None
        self.x_test = None
        self.y_train = None
        self.y_test = None
        self.input_shape = None
        self.verbose = 1
        self.scaler_x = None
        
    def set_parameters(self, hidden_layers = 1, 
                            activation_function = 'relu', 
                            dropout_prob = 0.2,
                            epochs = 10
                            ):
    
        # Set various parameters to tune the MLP
        self.hidden_layers = hidden_layers
        self.layer_activation_function = activation_function
        self.dropout_prob = dropout_prob
        self.optimizer =  'adam'  # 'RMSprop()'
        
    def prepare_data(self, x_data, y_data):
        
        (samplesize, featuresize) = x_data.shape

        scaler_x = MinMaxScaler()
        
        xscale = scaler_x.fit_transform(x_data)
        yscale = y_data
        self.input_shape = (featuresize,)
        self.scaler_x = scaler_x

        return (xscale, yscale)
        
        # Split into training and validation
        
        
    def add_mlp_layers(self, model, hidden_layers, 
                        layer_dimension, layer_activation_function, 
                        input_shape, dropout_prob):
    
        #print("Adding Layers: ", hidden_layers + 1)
        for i in range(hidden_layers + 1): # Add 1 because we have to also add input layer
                #print("--- Adding dense layer")
                model.add(Dense(
                        layer_dimension, 
                        activation=layer_activation_function,
                        input_shape=input_shape)
                    )
                if (dropout_prob > 0):
                        #print("--- Adding dropout layer")
                        model.add(Dropout(dropout_prob))
        
        #print("Done adding layers")
                

    def prepare_model(self):

        model = Sequential()
        # add input and hidden layers
        self.add_mlp_layers(
                            model,
                            self.hidden_layers, 
                            self.layer_dimension, 
                            self.layer_activation_function,
                            self.input_shape,
                            self.dropout_prob
                        )
                

        # Add the output layer
        model.add(Dense(
                    self.final_dimension, 
                    activation=self.final_activation_function)
                )

        #print("Also added final layer")
                
        model.summary()

        model.compile(loss= self.loss_function,
                        optimizer = self.optimizer,
                        metrics=[self.metric])

        self.model = model
              
        return model
    
    def fit (self, trainingdata_x, trainingdata_y, validationdata_x, validationdata_y):
        
        data = (validationdata_x, validationdata_y)
        self.model.fit( trainingdata_x, trainingdata_y,
                    batch_size=self.batch_size,
                    epochs=self.epochs,
                    verbose=self.verbose,
                    #validation_data=data
                    validation_split=0.2
                )
    
    def predict(self, testdata):
        predicted_labels = self.model.predict(testdata)
        return predicted_labels
    
    def get_score(self, validationdata_x, validationdata_y):
        score = self.model.evaluate(validationdata_x, validationdata_y, verbose=0)
        print('Test loss:', score[0])
        print('Test accuracy:', score[1])     


class Q4Model:
    def __init__(self):
        self.validation_split = 0.2
        self.mlp_params = None
        self.le_params = None
        self.validationdata_x = None
        self.trainingdata_x = None
        self.validationdata_y = None
        self.trainingdata_y = None
        self.fulldata = None
        self.x_data = None
        self.y_data = None
        self.x_data_orig = None
        self.y_data_orig = None
        


    def read_powerdata(self, fname):
        df = pd.read_csv(fname, 
                                sep = ';',
                                usecols = [0, 1, 2],
                                infer_datetime_format = True, 
                                parse_dates=['Date'],
                                converters={"Global_active_power": convert_power},
                                dayfirst = True
                                )
        self.df = df
        return df


    def fill_missing(self):
        # Process missing data
        df = self.df
        dx = df['Global_active_power']
        df2 = dx.replace('None', value = None, method = 'pad')
        self.fulldata = np.array(df2, dtype='float64')
        
    
    def build_regression_dataset(self, N=60):
        '''
        Each y value is predicted by last N values, # of past samples to consider
        so each of those become sort of feature dimensions. We now construct an X with shape (-1, N+1). 

        '''    
        s_data0 = self.fulldata
        colcount = N + 1 # (N features and 1 y value)
        asize0 = len(s_data0)
        alist = list()
        
        t1 = time.time()
            
        for i in range(colcount):
            beginindex = i
            asize = asize0 - i # The size of array we are operating with
            limit = asize - asize % (colcount)
            endindex = i + limit
            s_data = s_data0[beginindex:endindex]
            #print("Data:", i, asize, limit, beginindex, endindex, len(s_data))
            alist.append(s_data.reshape(-1, colcount))
        
        newds = np.concatenate(alist, axis = 0)
        t2 = time.time()
        print("Time taken to reshape data:  ", round(t2-t1))

        print("New DS shape: ", newds.shape)
        
        x_data = newds[:, 0:N].reshape(-1, N)
        y_data = newds[:, N].reshape(-1)
        
        print("x_data shape: ", x_data.shape)
        print("y_data shape: ", y_data.shape)
        
        self.x_data_orig = x_data
        self.y_data_orig = y_data

    def set_regressiondata(self, fname, N):
        # Read the file, fix missing data, and build the training and validation sets
        self.read_powerdata(fname)
        self.fill_missing()
        self.build_regression_dataset(N)

    def split_data(self, x_data, y_data):
        VALIDATION_SET_SIZE = self.validation_split
        mask = np.random.rand(len(x_data)) <= VALIDATION_SET_SIZE
        self.validationdata_x = x_data[mask]
        self.trainingdata_x = x_data[~mask]
        self.validationdata_y = y_data[mask]
        self.trainingdata_y = y_data[~mask]
        
        print(self.trainingdata_x.shape, 'training samples')
        print(self.validationdata_x.shape, 'validation samples')
        print(self.trainingdata_y.shape, 'training Y')
        print(self.validationdata_y.shape, 'validation Y')

    def metrics(self, predicted_labels):

        print("Label Shape: ", predicted_labels.shape)
        print("Validation Y shape: ", self.validationdata_y.shape)
        loss = predicted_labels.reshape(len(self.validationdata_y)) - self.validationdata_y
        MSE = np.square(loss).mean(axis = 0)
        MAE = np.abs(loss).mean(axis = 0)
        if (0 in self.validationdata_y):
            print("Y has 0! MAPE not possible!")
        else:
            MAPE = np.abs(loss/self.validationdata_y).mean(axis = 0)

        R2Score = r2_score(self.validationdata_y, predicted_labels)
        RMSE2 = np.sqrt(mean_squared_error(self.validationdata_y, predicted_labels))
        MAE2 = mean_absolute_error(self.validationdata_y, predicted_labels)
        #print (Y)
        print("RMSE: ", round(np.sqrt(MSE), 3), round(RMSE2, 3),  "MAE: ", round(MAE, 3), round(MAE2, 3), "MAPE: ", round(MAPE, 3), "R2 Score: ", round(R2Score, 3) )
        



class LinearClassifier:
    def __init__(self):
        self.reg = None

    def prepare_data(self, x_data, y_data):
        return (x_data, y_data)

        
    def fit(self, trainingdata_x, trainingdata_y):
        self.reg = LinearRegression().fit(trainingdata_x, trainingdata_y)
        
    
    def predict(self, validationdata_x):
        predicted_labels = self.reg.predict(validationdata_x)
        return predicted_labels



def test_lrmodel(fname = './household_power_consumption.txt', N = 60):
    print("Running Linear Regression Model with N = ", N)
    m = Q4Model()
    m.set_regressiondata(fname, N)
    le_params = LinearClassifier()
    # Let the model modify and update the data
    (x_data, y_data) = le_params.prepare_data(m.x_data_orig, m.y_data_orig)
    m.split_data(x_data, y_data)
    le_params.fit(m.trainingdata_x, m.trainingdata_y)
    predicted_labels = le_params.predict(m.validationdata_x)
    m.metrics(predicted_labels)
    print("Linear Regression Model with N = ", N, " completed.")
    
def test_mlpmodel(fname = './household_power_consumption.txt', N = 60):
    print("Running MLP Model with N = ", N)
    m = Q4Model()
    m.set_regressiondata(fname, N)
    mlp_params = MLPModel()
    mlp_params.set_parameters(hidden_layers=3, dropout_prob=0)
    # Let the model modify and update the data
    (x_data, y_data) = mlp_params.prepare_data(m.x_data_orig, m.y_data_orig)
    m.split_data(x_data, y_data)
    mlp_params.prepare_model()
    mlp_params.fit(m.trainingdata_x, m.trainingdata_y, m.validationdata_x, m.validationdata_y)
    predicted_labels = mlp_params.predict(mlp_params.scaler_x.transform(m.validationdata_x))
    m.metrics(predicted_labels)
    mlp_params.get_score(m.validationdata_x, m.validationdata_y)
    print("MLP Model with N = ", N, " completed.")

#test_lrmodel(N = 180) #RMSE:  0.258 0.258 MAE:  0.096 0.096 MAPE:  0.107 R2 Score:  0.94
#test_mlpmodel(N = 60) #RMSE:  1.396 1.396 MAE:  1.025 1.025 MAPE:  1.012 R2 Score:  -0.753    


