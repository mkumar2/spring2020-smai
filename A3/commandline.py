
def read_cmd():
    from optparse import OptionParser
    parser = OptionParser()
    parser.add_option("-f", "--file", dest="filename",
                    help="write report to FILE", metavar="FILE")
    parser.add_option("-q", "--quiet",
                    action="store_false", dest="verbose", default=True,
                    help="don't print status messages to stdout")

    (options, args) = parser.parse_args()
    print ("Parsed: options", options, " Left: ", args)

#read_cmd()

def read_cmd2():
    import sys
    params = sys.argv[1:]
    if (len(params) == 0):
        print("Usage: ", sys.argv[0], '<directory_location>')
    else:
        dir = sys.argv[1]
        print("Directory: ", dir)

read_cmd2()
