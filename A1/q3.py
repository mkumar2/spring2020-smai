import pandas as pd
import math
import numpy as np
import statistics as st
import time
from collections import Counter
import scipy.spatial.distance as sp
import random
import binarytree as bt
from operator import itemgetter, attrgetter
            

"""
Implement a decision tree to predict housing prices for the given dataset using the
available features.
2. The various attributes of the data are explained in the le data description.txt.
Note that some attributes are categorical while others are continuos.
3. Feel Free to use Python libraries such as binarytree or any other library in Python
to implement the binary tree. However, you cannot use libraries like scikit-learn
which automatically create the decision tree for you.
4. Experiment with dierent measures for choosing how to split a node(Gini impurity,
information gain, variance reduction) . You could also try dierent approaches to
decide when to terminate the tree.
5. Report metrics such as Mean Squared Error(MSE) and Mean Absolute Error(MAE)
along with any other metrics that you feel may be useful.
"""

"""
Approach
Creation
1. For each feature, do the following: 
    a. For each partition of the data of the feature, calculate the measure of the 2 groups being formed. Store it. 
    b. Pick the partition that provides the best measure
2. Pick the feature that provides the best measure
3. Keep doing till max depth is reached, or measures are 0

Prediction
1. Match the data with the root. If it is on left side, run the algo for the left, otherwise right. 
2. If no more to go, the leaf node group's average (or some other measure) is the prediction. 

Algorithm steps

1. for a features, do this
    a. select a row from the rows included in this run. 
    b. Create a split into 2 groups. There can be different strategies for categorical. For value, just use < >. 
    c. Evaluate gini index for the split. 
    d. Keep the value and compare with the value so far. If it is less than prev, keep it around as a candidate.
    e. Identify the best split. This becomes the decision tree node at this level.  
2. Repeat 1 for each feature
3. Select the feature with best gini index and use it as the split point for this level. 
4. Repeat this till one of the following happen
    a. Max depth is reached
    b. No split of the nodes at a level gives better gini than current node's. 

while true:
    feature, left, right, continue = split_the_set(root)
    if (continue):
        split_the_set(left)
        split_the_set(right)

def split_the_set:
    for f in features:
        left, right, score = get_better_split()
        blist = append(left, right, score)
    if blist is none:
        continue = false
        return continue
    else:
        min = find_min(score from blist)
        return min.left, min.right, min.score

Data structure: we use a list of rows, so that they can be moved around as needed to form groups based on features. 



Sum of Squared Errors. 

For each item in sample, we find the squared error from mean and sum them. That is the measure we use. 

References: 
https://www.displayr.com/how-is-splitting-decided-for-decision-trees/ - How to split categorical variables
https://machinelearningmastery.com/implement-decision-tree-algorithm-scratch-python/ - decision tree implementation from scratch
https://hlab.stanford.edu/brian/error_sum_of_squares.html - error sum of squares
https://clearpredictions.com/Home/DecisionTree - what is decision tree?
https://binarytree.readthedocs.io/en/latest/overview.html - Binary tree implementation
https://towardsdatascience.com/decision-tree-from-scratch-in-python-46e99dfea775 - decision tree from scratch

For q1 and q2: 
https://www.dataquest.io/blog/jupyter-notebook-tutorial/ - jupyter notebook for beginners
https://medium.com/@george.drakos62/handling-missing-values-in-machine-learning-part-2-222154b4b58e - handling missing values
https://stackoverflow.com/questions/24147278/how-do-i-create-test-and-train-samples-from-one-dataframe-with-pandas
        
"""

"""
Algorithm steps

1. for a features, do this
    a. select a row from the rows included in this run. 
    b. Create a split into 2 groups. There can be different strategies for categorical. For value, just use < >. 
    c. Evaluate gini index for the split. 
    d. Keep the value and compare with the value so far. If it is less than prev, keep it around as a candidate.
    e. Identify the best split. This becomes the decision tree node at this level.  
2. Repeat 1 for each feature
3. Select the feature with best gini index and use it as the split point for this level. 
4. Repeat this till one of the following happen
    a. Max depth is reached
    b. No split of the nodes at a level gives better gini than current node's. 

        while true:
            # feature, left, right, continue = split_the_set(root)
            split_the_set()
            if (continue):
                split_the_set(left)
                split_the_set(right)

def split_the_set:
    for f in features:
        left, right, score = get_better_split()
        blist = append(left, right, score)
    if blist is none:
        continue = false
        return continue
    else:
        min = find_min(score from blist)
        return min.left, min.right, min.score
  
class Impurity:
    def __init__(self, impval = math.inf, type='unknown'):
        self.impval = impval # Value of the impurity
        self.type =  type # type of the impurity measure used


class rowSet:
    def __init__(self, rowlist, indexlist):
        self.rowlist = rowlist
        self.indexlist = indexlist
        

"""

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
Start of the program
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
class DTNode:
    # holds the key info for the node in decision tree. 

    def __init__(self, rowidset, feature = 0, left = 0, right = 0, level = 0):
        self.rowidset = rowidset # List of data elements belonging to this group
        self.feature = feature # feature on which this node does the split. 
        self.impurity_score = math.inf
        self.left = left
        self.right = right
        self.level = level # which level this node is in. Root is 0th level, root's children are level 1 and so on. 
        self.split_value = 0
        self.type = 'numeric' # holds whether it is categorical or numeric
        self.valuemap = 0 # If categorical, we need a map to do comparison


class DecisionTree:
    def __init__(self):
        # include the initializer here. 
        self.trainingset = 0
        self.validationset = 0
        self.fullset = 0 # holds the full set, without partitioning for training and validation. 
        self.regressionTree = 0
        self.features = 0
        self.maxdepth = 5 # we will not go below 15
        self.outputcol = 'SalePrice'
        self.maxclasses = 10 # If a numeric class has less than this, we use the class intervals, otherwise we default to 10. 
        self.threshold = 0
        
        return None
    
    
    def preprocess_data(self, fullset):
        
        # print("Before: ", fullset.isna())
        
        # Data has a few columns with too many missing values. We will drop them. 
        droplist = ['Alley', 'PoolQC', 'Fence', 'MiscFeature']
        dframe = fullset.drop(droplist, axis=1)
        
        for column in dframe.columns:
            dframe[column].fillna(dframe[column].mode()[0], inplace=True)

        # print("After: ", dframe.isna())
        return dframe

    def train_q3(self, fullset):
        # Following steps are required: 
        # 1. Open the file and read the content by line. 
        # 2. Take each row as training data or validation data randomly
        # 3. Keep training and validation set around for future use
        # 4. Run the validation round to see how good the prediction is. If the prediction is not good, tweak k or the distance measure

        mydata = fullset

        
        mydata = self.preprocess_data(mydata)
        
        
        # create 2 sets, one for training, another for validation. 
        VALIDATION_SET_SIZE = 0.2
        #mask0 = np.random.rand(len(mydata)) <= 0.2
        #mydata = mydata[mask0]
        
        mask = np.random.rand(len(mydata)) <= VALIDATION_SET_SIZE
        self.validationset = mydata[mask]
        self.trainingset = mydata[~mask]
        self.fullset = mydata
        
        # Also keep the column names around for future use. 
        self.features = mydata.columns.values.tolist()
        # Remove the sale price column so that decision tree doesn't use it! 
        (self.features).remove(self.outputcol)
        # print("Feature List: ", self.features)
        
        rowidset = self.trainingset.index
        
        node = DTNode(rowidset) # Create the root node with full data set and add it to the binarytree node 
        self.build_decisiontree(self.trainingset, node) # Main function that does the heavy lifting on the training data. 
        self.regressionTree = node 

        # self.print_decisiontree(self.regressionTree)
    
        return self.regressionTree

    def get_values(self, node):
        
        # Get the average of output col for all of the included rows. 

        indexlist = node.rowidset
        dframe = self.trainingset.loc[indexlist, :]
        avgoutput = np.average(dframe.loc[:, self.outputcol])
        return avgoutput

        
    def predict_values_fixed(self, sample, dtroot, type='mean'):
        mean = self.trainingset[self.outputcol].mean()
        median = self.trainingset[self.outputcol].median()
        if (type == 'mean'):
            return mean
        else:
            return median

    

    def predict_values(self, sample, dtroot):
        # Algorithm
        # 1. Visit a node and select its feature and split_value
        # 2. If the sample has value less than this, go to left tree and try. Otherwise right tree. 
        # 3. If there are no more left or right nodes (this is a leaf node), find the average output value of the class and use it. 

        node = dtroot
        while (node):
            # print("Entered predict_values with ", sample)
            # self.print_node(node)
            # if this is a leaf node, this will give us our answer. The average output value of the group
            if ((node.left == 0) or (node.right == 0)): # Both should be 0 or not 0.
                return self.get_values(node)
            else: # We are at intermediate node, keep going
                
                feature = node.feature
                valuemap = node.valuemap
                type = node.type
                split_value = node.split_value
                if (type == 'numeric'):
                    feature_value = sample[feature]
                else:
                    if (sample[feature] not in valuemap.keys()):
                        #print(sample[feature], " is not found! Trying backup..")
                        valuemap = self.get_avgoutput_map(self.trainingset, feature, self.outputcol)

                    feature_value = valuemap[sample[feature]]

                if (feature_value <= split_value):
                    return self.predict_values(sample, node.left)
                else:
                    return self.predict_values(sample, node.left)





    def validate_q3(self):
        # We just run the validation run against the decision tree and see what is % correctness. 

        workingset = self.validationset
        result_list = list()
        MSPE = 0 #  mean squared prediction error
        for index, row in workingset.iterrows():
            match = self.predict_values(row, self.regressionTree)
            actual = row[self.outputcol]
            if (abs(match - actual) <= self.threshold):
                # consider it matched
                result_list.append(1)
            else:
                result_list.append(0)
            MSPE += pow((match - actual), 2)
            
        MSPE = MSPE/len(result_list) # Mean square error requires dividing by 
        # we have the list now. Add up and find accuracy %. 
        matchpercent = (sum(result_list)*100)/len(result_list)
        print("Validation: ", matchpercent, "% matched.")



    def train(self, trainingfile):
        # Read the training data and train the decision tree classifier
        fullset = pd.read_csv(trainingfile, index_col=0) # this has header data, good! also, use first column as index since it has ids. 
        self.train_q3(fullset)
        
    
    def predict_q3(self, fulltestset, type):
        print("predict_q3 called.")
        workingset = fulltestset
        result_list = list()
        for index, row in workingset.iterrows():
            if (type == 'dt'):
                match = self.predict_values(row, self.regressionTree)
            else:
                match = self.predict_values_fixed(row, self.regressionTree, type)
            result_list.append(match)
        
        return result_list
            


 
    def predict(self, testfile, type = 'dt'):
        # Run the prediction on the test data using the trained model
        fulltestset = pd.read_csv(testfile, index_col=0) # this has header data, good! also, use first column as index since it has ids. 
        fulltestset = self.preprocess_data(fulltestset)
        
        return self.predict_q3(fulltestset, type)
    
    def update_node(self, node, candidate):

        node.impurity_score = candidate[0] 
        
        rowidset = candidate[1]
        node.left = DTNode(rowidset) # attach left node
        node.left.level = node.level + 1 # increment the level
        node.left.impurity_score = candidate[5] # set the impurity level
        
        rowidset = candidate[2]
        node.right = DTNode(rowidset) # attach right node 
        node.right.level = node.level + 1 # increment the level
        node.right.impurity_score = candidate[6] # set the impurity level
    
        node.feature = candidate[3] # feature on which this node does the split. 

        node.split_value = candidate[4] # feature on which this node does the split.
        
        # these will be used when predicting
        node.type = candidate[7] 
        node.valuemap = candidate[8] 
        
        
        return node


    def get_impurityscore_numeric(self, dframe, indexlist, feature):
        # Since outcome is numeric, we use Sum of Squared Errors.  
        # Count the number of items of each class
        # Use the definition from here: https://www.displayr.com/how-is-splitting-decided-for-decision-trees/
        # See here: https://www.quora.com/How-do-decision-trees-for-regression-work

        sampleCount = len(indexlist) # number of samples
        workingset = dframe.loc[indexlist, :] # all the relevant rows
        avgoutput = np.average(workingset.loc[:, self.outputcol])
        sse = 0
        for index, row in workingset.iterrows():
            sse += pow((row.loc[self.outputcol] - avgoutput), 2) 
            
        return sse
    
    def create_groups(self, dframe, split_value, type, valuemap, feature):
        left = list()
        right = list()
        for index, row in dframe.iterrows(): # assume cvalue1 is the split point. assign the rows to 2 groups
            if (type == 'numeric'):
                value = dframe.loc[index, feature]
            else:
                # print("index: ", index, " Feature: ", feature, "Value: ", dframe.loc[index, feature])
                # print("valuemap: ", valuemap)
                value = valuemap[dframe.loc[index, feature]]

            if (value > split_value):
                right.append(index)
            else:
                left.append(index)
        return left, right, split_value


    def get_avgoutput_map(self, dframe, feature, outputcol):
        classvalue = dframe.groupby(feature)[outputcol].mean()
        grouplist = list(zip(classvalue.keys(), classvalue.values[:])) # Create a tuple out of this. 
        grouplist = sorted(grouplist, key=itemgetter(1)) # sort the tuple
        split_points = np.array(sorted(list(classvalue.values[:])))
        split_points = (split_points[1:] + split_points[:-1]) / 2 # Very clever indeed!!
        valuemap = dict(grouplist)
        return valuemap

    def split_the_set(self, fullset, node, feature, dtype):
        # dataframe needs to be split into 2 parts, using the value on the feature column. 
        # Depending on the dtype, we will use different algos. 
        # print("split_the_set")
    
        # to start with, we will use a simple logic - we will enforce an ordering based on the value of 
        # the target, which is the last column. 
        # We skip non-categorical for now, later we will refine the logic. 

        # walk through each row, and put the row into right or left group, 
        # depending on whether they are less than or greater than the thresold

        dframe = fullset.loc[node.rowidset] # get the filtered rows that map to this node. 

        if ((dtype != 'int64') and (dtype != 'float64')):
            # categorical variables: We will find split points using the average output value for each class. 
            classvalue = dframe.groupby(feature)[self.outputcol].mean()
            grouplist = list(zip(classvalue.keys(), classvalue.values[:])) # Create a tuple out of this. 
            # print("Group List:", grouplist)
            grouplist = sorted(grouplist, key=itemgetter(1)) # sort the tuple
            # print("Sorted List: ", sorted(grouplist, key=itemgetter(1)))
            split_points = np.array(sorted(list(classvalue.values[:])))
            # clever way to get midpoints of these points to be used for finding groups
            split_points = (split_points[1:] + split_points[:-1]) / 2 # Very clever indeed!!
            type = 'categorical' # to use in create_groups

            valuemap = dict(grouplist)
            
        else:
            # numeric variables: We will find the appropriate class intervals to use as split points. 
            # count number of unique values in the column. If it is > self.maxclasses, we use 
            # self.maxclasses as the number of classes. 
            dcount = dframe.nunique()[feature] # number of distinct items in the column. 
            if (dcount > self.maxclasses):
                dcount = self.maxclasses
            min = dframe.loc[:, feature].min()
            max = dframe.loc[:, feature].max()
            cwidth = (max - min)/dcount
            type = 'numeric' # to use in create_groups
            valuemap = 0 # not required for numeric
            # there is no way to exclude start, so starting from cwidth shifted value 
            split_points = np.linspace(min, max, dcount)
            # clever way to get midpoints of these points to be used for finding groups
            split_points = (split_points[1:] + split_points[:-1]) / 2 # Very clever indeed!!
 
            # now we can order the classes based on these values and pick from n - 1 possible splits where n is the number of classes. 
            
        # 
        # Now we have the list of split points. We iterate on them and figure out the best split. 
        #     
        candidatelist = list()
        for split_point in split_points:
            left, right, split_value = self.create_groups(dframe, split_point, type, valuemap, feature)
        
            impleft = self.get_impurityscore_numeric(dframe, left, feature)
            impright = self.get_impurityscore_numeric(dframe, right, feature)

            impurity_score = impleft + impright

            candidatelist.append((impurity_score, left, right, split_value, impleft ,impright, type, valuemap))

        # Loop is over. Let's sort the list and pick the first item. 
        if (candidatelist):
            candidatelistsorted = sorted(candidatelist, key=itemgetter(0))
            candidate = candidatelistsorted[0] 
        else:
            # No candidates
            left = 0
            right = 0
            impurity = math.inf
            split_value = 0
            impleft = math.inf
            impright = math.inf
            type = 'numeric'
            valuemap = 0
            candidate = (impurity, left, right, split_value, impleft, impright, type, valuemap)

        """
        print("split_the_set:", " Level: ", node.level, " Candidate feature: ", 
                feature,  " Impurity: ", round(candidate[0], 2), " Split value: ", candidate[3] )
        """
        return candidate

    def print_node(self, node):
        level_string = '.'* node.level
        print(level_string, "Feature: ", node.feature, " Impurity: ", node.impurity_score, 
                " Data Size: ", len(node.rowidset), " Split Value: ", node.split_value, " Type: ", node.type, " Valuemap: ", node.valuemap)
        """
        self.rowidset = rowidset # List of data elements belonging to this group
        self.feature = feature # feature on which this node does the split. 
        self.impurity_score = math.inf
        self.left = left
        self.right = right
        self.level = level # which level this node is in. Root is 0th level, root's children are level 1 and so on. 
        self.split_value = 0
        self.type = 'numeric' # holds whether it is categorical or numeric
        self.valuemap = 0 # If categorical, we need a map to do comparison

        """

    def print_feature_split_candidate(self, candidatelist):
        # sequence in the list item: impurity_score, left, right, feature, split_value, impleft, impright
        
        for candidate in candidatelist:
            if (candidate[1] == 0):
                leftlen = 0
            else:
                leftlen = len(candidate[1])
            
            if (candidate[2] == 0):
                rightlen = 0
            else:
                rightlen = len(candidate[2])
            
            print(
                    " Feature: ", candidate[3],
                    " Impurity: ", candidate[0], 
                    " Split Value: ", candidate[4], 
                    " Size(Left): ", leftlen,
                    " Size(Right): ", rightlen,
                    " Imp(Left): ", candidate[5], 
                    " Imp(Right): ", candidate[6]
                )
                
    def print_decisiontree(self, node):
        # iterate and print
        self.print_node(node)
        if (node.left):
            self.print_decisiontree(node.left)
        if (node.right):
            self.print_decisiontree(node.right)
        
    def build_decisiontree(self, fullset, node):

        # print("DT: Index List for this invocation: ", indexlist)
        # we will proceed only if the current level is not already hitting max threshold
        if (node.level >= self.maxdepth): # ideally shouldn't ever be greater than, equality test should be enough. 
            print("DT: Reached max depth of ", self.maxdepth)
            return

        """
        1. for a features, do this
            a. select a row from the rows included in this run. 
            b. Create a split into 2 groups. There can be different strategies for categorical. For value, just use < >. 
            c. Evaluate gini index for the split. 
            d. Keep the value and compare with the value so far. If it is less than prev, keep it around as a candidate.
            e. Identify the best split. This becomes the decision tree node at this level.  
        """

        # We are good to proceed, max depth is not yet reached.     
        print("Node Level: ", node.level, " Node Impurity: ", node.impurity_score, " Data size: ", len(node.rowidset))
        
        dframe = fullset.loc[node.rowidset] # get the filtered rows that map to this node. 

        candidatelist = list()
        for feature in self.features: # For each feature, do this
            dtype = dframe.dtypes[feature] # get the data type of the feature
            # print("Feature: ", feature, "Data Types: ", dtype)
            
            # run the split algo based on feature type and return the 2 sets as well as the new impurity index 
            impurity_score, left, right, split_value, impleft, impright, type, valuemap = self.split_the_set(dframe, node, feature, dtype)

            #print("DT: Return from split_the_set.  : ", impurity_score)

            candidatelist.append((impurity_score, left, right, feature, split_value, impleft, impright, type, valuemap))
            
        # Let's sort this and get the minimum. 
        if (candidatelist):        
            candidatelistsorted = sorted(candidatelist, key=itemgetter(0))
            candidate = candidatelistsorted[0] # This tuple holds the minimum value
            """
            print("Candidates:")
            self.print_feature_split_candidate(candidatelistsorted)
            """

            # We will use this only if it improves the impurity score of the parent. Check for that. 
            if (node.impurity_score <= candidate[0]):
                # Stop splitting since no candidate found which betters the impurity. 
                print("No features found to better the impurity. Current level = ", node.level)
                return
            else:
                # update this node. 
                print("Splitting on ", candidate[3], " Current level = ", node.level)
                node = self.update_node(node, candidate)
                print("Call left subtree.")
                self.build_decisiontree(dframe, node.left)
                print("Call right subtree.")
                self.build_decisiontree(dframe, node.right)
                return
        else:
            print("No features yielded any split candidates!") # is this possible?
            return;
        
        # we have now traversed all the features on this set, and found the feature that 
        # yields that best split. Use it to recurse down.  
        
