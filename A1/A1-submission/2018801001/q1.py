import pandas as pd
import math
import numpy as np
import statistics as st
import time
from collections import Counter
import scipy.spatial.distance as sp
import random
""" 
Question 1

k-Nearest Neighbors - Task 1
1. Implement a KNN based classier to predict digits from images of handwritten
digits in the dataset.
2. Featurize the images as vectors that can be used for classication.
3. Experiment with dierent values of K(number of neighbors).
4. Experiment with dierent distance measures - Euclidean distance, Manhattan dis-
tance,
5. Report accuracy score, F1-score, Confusion matrix and any other metrics you feel
useful.
6. Implement baselines such as random guessing/majority voting and compare perfor-
mance. Also, report the performance of scikit-learn's kNN classier. Report your findings. 

"""

"""
>>> data = {'Country': ['Belgium',  'India',  'Brazil'],
            'Capital': ['Brussels',  'New Delhi',  'Brasilia'],
            'Population': [11190846, 1303171035, 207847528]}
>>> df = pd.DataFrame(data,columns=['Country',  'Capital',  'Population'])

1. df.iloc[0] - Gets the first row
2. df.iloc[:,0] - Gets the first column

"""

class knnMetrics:
    def __init__(self, predicted, actual):
        self.predicted = predicted
        self.actual = actual
        self.accuracy = 0
        self.confmatrix = 0
        self.fiscore = 0

class KNNClassifier:

    def __init__(self):
        # include the initializer here. 
        self.trainingset = 0
        self.validationset = 0
        self.metricsDict = {} # holds the metrics for each k we use.
        self.kNN = 5 #Optimal k that we are using.  
        self.distmetric = 'cosine' #optimal metric
        self.fullset = 0 # holds the full set, without partitioning for training and validation. 
        self.labelcol = 0

        return None
    
    def traindf_q1(self, df, incprop=0.1, labelcol = 0): # Pass df rather than csv
        mydata = df
        self.fullset = mydata
        # create 2 sets, one for training, another for validation. 
        # used this idea: https://stackoverflow.com/questions/24147278/how-do-i-create-test-and-train-samples-from-one-dataframe-with-pandas
        INCLUDE_IN_SET = incprop
        VALIDATION_SET_SIZE = 0.2
        mask0 = np.random.rand(len(mydata)) <= INCLUDE_IN_SET
        mydata = mydata[mask0]
        mask = np.random.rand(len(mydata)) <= VALIDATION_SET_SIZE
        self.validationset = mydata[mask]
        self.trainingset = mydata[~mask]
        self.labelcol = labelcol
        return mydata


    def train_q1(self, trainingfile, incprop=0.1, labelcol = 0):
        # Following steps are required: 
        # 1. Open the file and read the content by line. 
        # 2. Take each row as training data or validation data randomly
        # 3. Keep training and validation set around for future use
        # 4. Run the validation round to see how good the prediction is. If the prediction is not good, tweak k or the distance measure

        self.labelcol = labelcol
        mydata = pd.read_csv(trainingfile, header=None)
        return self.traindf_q1(mydata, incprop, labelcol)

    
    def getDistanceMatrix(self, ds1, ds2, distmeasure = 'euclidean' ):
        distmatrix = sp.cdist(ds1, ds2, distmeasure)
        return distmatrix
    
    def findKNearestNeightborsV2(self, distmeasure = 'euclidean'):
        # Find the distance of testrow from each row in training set. 

        trainingset = self.trainingset
        validationset = self.validationset
        newset = trainingset.drop(trainingset.columns[0], axis=1) # drop the column with prediction
        newvalidationset = validationset.drop(validationset.columns[0], axis=1)
        distmatrix = self.getDistanceMatrix(newset, newvalidationset, distmeasure)
    
        # Identify the top K neighbors
        # Return a DF consisting of these neighbours
        # print("Distance Matrix shape: ", distmatrix.shape)
        return distmatrix # return the 2-d array containing the distances   

    def getPredictedLabel(self, distmatrix, trainingset, sampleCount, kNN, labelcol=0):
        result_list = list()
        #print("Row count: ", rowcount, " distmatrix shape: ", distmatrix.shape)
        #print("GPL: sampleCount = ", sampleCount, " K = ", kNN)
        
        for i in range(sampleCount):
            #print("GPL: i = ", i)
        
            distvec = distmatrix[:, i]
            arr = np.array(distvec)
            indx = arr.argsort()[:kNN-1] # indices of the smallest k values
            #print("index = ", indx)
            label = self.findTheLabel(trainingset, indx, labelcol)
            result_list.append(label)
        return result_list

    def predictdf_q1(self, testset):
        trainingset = self.trainingset
        kNN = self.kNN
        distmeasure = self.distmetric
        return self.predict_q1_internal(testset, trainingset, kNN, distmeasure)

    def predict_q1_internal(self, testset, trainingset, kNN, distmeasure):

        # newtrainingset = trainingset.drop(trainingset.columns[0], axis=1) # drop the column with prediction
        newtrainingset = trainingset.drop(trainingset.columns[self.labelcol], axis=1) # drop the column with prediction
        distmatrix = self.getDistanceMatrix(newtrainingset, testset, distmeasure)
    
        # Identify the top K neighbors
        # Return a DF consisting of these neighbours
        # print("Distance Matrix shape: ", distmatrix.shape)

        return self.getPredictedLabel(distmatrix, trainingset, testset.shape[0], kNN, self.labelcol)


    def predict_q1(self, testfile):
        # Find the distance of testrow from each row in training set. 
        k = self.kNN # learned value that we know works best
        distmeasure = self.distmetric # distmeasure that we know works best
        trainingset = self.fullset
        testset = pd.read_csv(testfile, header=None)
        
        retval = self.predict_q1_internal(testset, trainingset, k, distmeasure)
        return retval
        
    def findTheLabel(self, dataset, indx, labelcol = 0):
        # pick the label that is on majority of the rows.
        krows = dataset.iloc[indx] 
        label = st.mode(krows.iloc[:, labelcol])
        return label
    
    def accuracy(self, predicted, actual):
        # val = list(Counter(predicted).keys())
        # val = val.sort()
        val = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        # print("Labels for confusion matrix: ", val)
        data = np.zeros(shape = (len(val),len(val)))
        confmatrix = pd.DataFrame(data, index = val,
                                        columns = val)
        # print("Column of confmatrix: ", confmatrix.columns)
        correct = 0
        for i in range(len(predicted)):
            if (predicted[i] == actual[i]):
                correct = correct + 1
            confmatrix.loc[np.int64(predicted[i]), np.int64(actual[i])] += 1
        retval = list((confmatrix, (correct * 100)/len(predicted)))
        return retval
        #return (correct * 100)/len(predicted)

    
    def multiclassData(self, predicted, actual):
        predDict = Counter(predicted) # get the occurance of each item
        actDict = Counter(actual) # get the occurance of each item
        # print("Predicted: ", predDict)
        # print("Actual: ", actDict)

        correctDict = {}
        for i in range(len(predicted)):
            k = predicted[i]
            kact = actual[i]
            if (k == kact): # correct prediction
                if k not in correctDict.keys():
                    correctDict[k] = 1
                else:
                        correctDict[k] = correctDict[k] + 1 

        # print("Correct: ", correctDict)
        
        return list((predDict, actDict, correctDict))        

    def showMetrics(self, metriclist):
        confmatrix = metriclist[0]
        accval = metriclist[1]
        avgprecision = metriclist[2]
        avgrecall = metriclist[3]
        avgf1 = metriclist[4]
        
        print("Avg Accuracy: ", round(accval, 2), "%, Avg precision: ", round(avgprecision, 2), 
        "%, Avg recall: ", round(avgrecall, 2), 
        "% Avg f1 score: ", round(avgf1), "%")
        print("Confusion Matrix:\n", confmatrix)
    
    def getMetrics(self, predictedList):
        predicted = predictedList
        actual = list(self.validationset.iloc[:,0])
        metricData = self.multiclassData(predicted, actual) # get data aggregated by class
        predDict = metricData[0]
        actDict = metricData[1]
        correctDict = metricData[2]
    
        precDict = {}
        recDict = {}
        f1Dict = {}
        
        totprecision = 0
        totrecall = 0
        totf1 = 0

        for k in predDict.keys():
            if (k not in correctDict.keys() or 
                k not in actDict.keys()):
                print(k, " doesn't have sufficient data!")
                continue # skip calculating any metric for this k
            precDict[k] = (correctDict[k]*100)/predDict[k]
            recDict[k] = (correctDict[k]*100)/actDict[k]
            f1Dict[k] = (2 * precDict[k] * recDict[k])/(precDict[k] + recDict[k])
            
            totprecision = totprecision + precDict[k]
            totrecall = totrecall + recDict[k]
            totf1 = totf1 + f1Dict[k]
        
        retval = self.accuracy(predicted, actual)
        confmatrix = retval[0]
        accval = retval[1]
        classcount = len(correctDict.keys())
        if (classcount == 0):
            print("There is not enough data to show summary!")
            return ((0, 0, 0, 0, 0))
        else:
            avgprecision = totprecision/classcount
            avgrecall = totrecall/classcount
            avgf1 = totf1/classcount
            return ((confmatrix, accval, avgprecision, avgrecall, avgf1))
            
    def train(self, trainingfile):
        # Read the training data and train the kNN classifier
        self.fullset = pd.read_csv(trainingfile, header=None) # nothing to train in knn
        # df = train_q1(trainingfile)
    
    def predict(self, testfile):
        # Run the prediction on the test data using the trained model
        
        return self.predict_q1(testfile)

    # Assumes that training data has been loaded and split by calling train_q1. 
    # Uses the data to calculate different values of k and accuracy tuple for a given measure
    def experimentWithMeasure(self, maxK, distmeasure):
        retlist = list()
        distmatrix = self.findKNearestNeightborsV2(distmeasure)
        trainingset = self.trainingset
        validationset = self.validationset
        sampleCount = validationset.shape[0]
        print("Distmatrix shape: ", distmatrix.shape)
        for k in range(3, maxK):
            #print("EWM: k = ", k)
            result_list = self.getPredictedLabel(distmatrix, trainingset, sampleCount, k)
            metrics = self.getMetrics(result_list)
            retlist.append((k, distmeasure, metrics[0], metrics[1], metrics[2], metrics[3], metrics[4]))
        return retlist # contains k and various metrics for the given k


    def otherClassifier_baseline(self, ctype):
        # Whenever we have to estimate, we just pick a random response, 1 of the 10 digits. 
        val = [0,1,2,3,4,5,6,7,8,9]
        weights = []
        w1 = Counter(self.trainingset.iloc[:,0])
        for i in val:
            weights.append(w1[i]) # look up the frequency and add it in the weight list
        
        # print('Weights: ', weights)
        validationset = self.validationset
            
        result_list = list()
        for i in range(validationset.shape[0]):
            if (ctype == 'random'):
                label = random.choice(val)
            else: 
                labels = random.choices(val, weights=weights)
                # print("Label for weighted: ", labels[0])
                label = labels[0]
            
            result_list.append(label)
        return result_list


    # accuracy score, F1-score, Confusion matrix and any other metrics

def time_q1run(k = 5, subsetData = 0.01, distmeasure = 'euclidean'):

    # Step 1: Read the file
    knn = KNNClassifier()
    t0 = time.time()
    df = knn.train_q1("./Datasets/q1/train.csv", incprop=subsetData) 
    t1 = time.time()

    # Step 2: Calculate the distance    
    t2 = time.time()
    distmatrix = knn.findKNearestNeightborsV2(distmeasure)
    t3 = time.time()
    
    # Step 3: Find the predicted labels
    rowcount = knn.validationset.shape[0]
    result_list = knn.getPredictedLabel(distmatrix, knn.trainingset, rowcount, k)
    t4 = time.time()

    # Step 4: Calculate and print metrics
    metriclist = knn.getMetrics(result_list)
    knn.showMetrics(metriclist)
    t5 = time.time()
    
    print("Program time: ", round(t5-t0, 5), 
    "seconds, Run time (= program time - file read time - metric display time): ", round((t4-t2), 5), "seconds")

def test_trainingV2(k = 5, subsetData = 0.01):

    knn = KNNClassifier()
    t0 = time.time()
    df = knn.train_q1("./Datasets/q1/train.csv", incprop=subsetData) 
    t1 = time.time()
    
    print(len(df), ' records are read. Time taken (seconds): ', round(t1-t0, 5))

    rowcount = knn.validationset.shape[0]

    t2 = time.time()
    distmatrix = knn.findKNearestNeightborsV2()
    t3 = time.time()
    print("Training size: ", knn.trainingset.shape[0], " Validation size: ", rowcount, 
            " Time taken: ", round(t3-t2, 5))

    result_list = knn.getPredictedLabel(distmatrix, knn.trainingset, rowcount, k)
    t4 = time.time()
    metriclist = knn.getMetrics(result_list)
    knn.showMetrics(metriclist)
    t5 = time.time()
    
    print("Total time taken in the kNN selection loop", round(t5-t4, 5))
    print("Total time taken by the program: ", round(t5-t1, 5))
    
def test_trainingV3():
    knn = KNNClassifier()
    knn.train_q1("./Datasets/q1/train.csv", incprop=0.1) 
    print("Training Set: ", knn.trainingset.shape)
    print("Validation Set: ", knn.validationset.shape)
    
    ret1 = knn.experimentWithMeasure(15, 'cityblock')
    #print(ret1)
    for x in ret1:
        print("K: ", x[0], ", Distance Metric is ", x[1], ", Accuracy(%) = ", round(x[3], 2))
    


def test_module():
    print("Hello! q1 is loaded.")

def test_trainingV4():
    knn = KNNClassifier()
    knn.train_q1("./Datasets/q1/train.csv", incprop=0.1) 
    print("Training Set: ", knn.trainingset.shape)
    print("Validation Set: ", knn.validationset.shape)
    
    result_list = knn.otherClassifier_baseline('random')
    metriclist = knn.getMetrics(result_list)
    print("\n\nRandom Classifier results: ")
    knn.showMetrics(metriclist)

    result_list = knn.otherClassifier_baseline('weighted')
    metriclist = knn.getMetrics(result_list)
    print("\n\nWeighted Random Classifier results: ")
    knn.showMetrics(metriclist)

#time_q1run(5, 1)
#test_trainingV3()
