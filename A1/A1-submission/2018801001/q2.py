import pandas as pd
import math
import numpy as np
import statistics as st
import time
from collections import Counter
import scipy.spatial.distance as sp
import random
import q1
""" 
Question 2

k-Nearest Neighbors - Task 2
Implement a KNN based classier to classify given set of features in Mushroom
Database. Missing data must be handled appropriately.(Denoted by "?").
2. Choose an appropriate distance measure for categorical features.
3. Experiment with dierent values of K(number of neighbors).
4. Report accuracy score, F1-score, Confusion matrix and any other metrics you feel
useful.
5. Implement baselines such as random guessing/majority voting and compare perfor-
mance. Also, report the performance of scikit-learn's kNN classier. Report your
ndings.
"""

"""
>>> data = {'Country': ['Belgium',  'India',  'Brazil'],
            'Capital': ['Brussels',  'New Delhi',  'Brasilia'],
            'Population': [11190846, 1303171035, 207847528]}
>>> df = pd.DataFrame(data,columns=['Country',  'Capital',  'Population'])

1. df.iloc[0] - Gets the first row
2. df.iloc[:,0] - Gets the first column

"""

class KNNClassifier:

    def __init__(self):
        # include the initializer here. 
        self.trainingset = 0
        self.validationset = 0
        self.metricsDict = {} # holds the metrics for each k we use.
        self.kNN = 3 #Optimal k that we are using.  
        self.distmetric = 'euclidean'
        self.fullset = 0 # holds the full set, without partitioning for training and validation. 
        self.encoded = 0 # Keeps track of whether encoding is complete or not

        return None
    
    def split_data_q2(self, fullset):
        
        mydata = fullset # this has no missing data. 

        # create 2 sets, one for training, another for validation. 
        VALIDATION_SET_SIZE = 0.2
        
        # Split into training and valiation data. 
        mask = np.random.rand(len(mydata)) <= VALIDATION_SET_SIZE
        self.validationset = mydata[mask]
        self.trainingset = mydata[~mask]
        self.fullset = mydata



    def train_q2(self, trainingfile):
        # Following steps are required: 
        # 1. Open the file and read the content by line. 
        # 2. Take each row as training data or validation data randomly
        # 3. Keep training and validation set around for future use
        # 4. Run the validation round to see how good the prediction is. If the prediction is not good, tweak k or the distance measure

        mydata = pd.read_csv(trainingfile, header=None)
        self.fullset = self.preprocess_dataset(mydata) # only missing data, no encoding. 
        
        return mydata

    
    def getDistanceMatrix(self, ds1, ds2, distmeasure = 'euclidean' ):
        #print("Ds1 shape: ", ds1.shape)
        #print("Ds2 shape: ", ds2.shape)
        
        distmatrix = sp.cdist(ds1, ds2, distmeasure)
        return distmatrix
    
    def findKNearestNeightborsV2(self, distmeasure = 'euclidean'):
        # Find the distance of testrow from each row in training set. 

        trainingset = self.trainingset
        validationset = self.validationset
        newset = trainingset.drop(trainingset.columns[0], axis=1) # drop the column with prediction
        newvalidationset = validationset.drop(validationset.columns[0], axis=1)
        distmatrix = self.getDistanceMatrix(newset, newvalidationset, distmeasure)
    
        # Identify the top K neighbors
        # Return a DF consisting of these neighbours
        # print("Distance Matrix shape: ", distmatrix.shape)
        return distmatrix # return the 2-d array containing the distances   

    def getPredictedLabel(self, distmatrix, trainingset, sampleCount, kNN):
        result_list = list()
        #print("Row count: ", rowcount, " distmatrix shape: ", distmatrix.shape)
        #print("GPL: sampleCount = ", sampleCount, " K = ", kNN)
        
        for i in range(sampleCount):
            #print("GPL: i = ", i)
        
            distvec = distmatrix[:, i]
            arr = np.array(distvec)
            indx = arr.argsort()[:kNN-1] # indices of the smallest k values
            #print("index = ", indx)
            label = self.findTheLabel(trainingset, indx)
            result_list.append(label)
        return result_list

    def predict_q2_internal(self, testfile, kNN, distmeasure):

        testset = pd.read_csv(testfile, header=None)
        retval = self.cleverencoding(testset, self.fullset)
        testset = retval[0]
        fullset = retval[1]
        
        # now setup the training data as well. 
        self.split_data_q2(fullset) # This sets up the training and validation sets in the object
        trainingset = self.trainingset
        newtrainingset = trainingset.drop(trainingset.columns[0], axis=1) # drop the column with prediction
        distmatrix = self.getDistanceMatrix(newtrainingset, testset, distmeasure)
    
        # Identify the top K neighbors
        # Return a DF consisting of these neighbours
        # print("Distance Matrix shape: ", distmatrix.shape)

        return self.getPredictedLabel(distmatrix, trainingset, testset.shape[0], kNN)


    def predict_q2(self, testfile):
        # Find the distance of testrow from each row in training set. 
        k = self.kNN # learned value that we know works best
        distmeasure = self.distmetric # distmeasure that we know works best
        retval = self.predict_q2_internal(testfile, k, distmeasure)
        return retval
        
    def findTheLabel(self, dataset, indx, labelcol = 0):
        # pick the label that is on majority of the rows.
        krows = dataset.iloc[indx] 
        label = st.mode(krows.iloc[:, labelcol])
        return label
    
    def accuracy(self, predicted, actual):
        # val = list(Counter(predicted).keys())
        # val = val.sort()
        val = ['e', 'p']
        # print("Labels for confusion matrix: ", val)
        data = np.zeros(shape = (len(val),len(val)))
        confmatrix = pd.DataFrame(data, index = val,
                                        columns = val)
        #print("Column of confmatrix: ", confmatrix.columns)
        correct = 0
        for i in range(len(predicted)):
            # print("acc: i = ", i)
            if (predicted[i] == actual[i]):
                correct = correct + 1
            confmatrix.loc[(predicted[i]), (actual[i])] += 1
        retval = list((confmatrix, (correct * 100)/len(predicted)))
        return retval
        #return (correct * 100)/len(predicted)

    
    def multiclassData(self, predicted, actual):
        predDict = Counter(predicted) # get the occurance of each item
        actDict = Counter(actual) # get the occurance of each item
        # print("Predicted: ", predDict)
        # print("Actual: ", actDict)

        correctDict = {}
        for i in range(len(predicted)):
            k = predicted[i]
            kact = actual[i]
            if (k == kact): # correct prediction
                if k not in correctDict.keys():
                    correctDict[k] = 1
                else:
                        correctDict[k] = correctDict[k] + 1 

        # print("Correct: ", correctDict)
        
        return list((predDict, actDict, correctDict))        

    def showMetrics(self, metriclist):
        confmatrix = metriclist[0]
        accval = metriclist[1]
        avgprecision = metriclist[2]
        avgrecall = metriclist[3]
        avgf1 = metriclist[4]
        
        print("Avg Accuracy: ", round(accval, 2), "%, Avg precision: ", round(avgprecision, 2), 
        "%, Avg recall: ", round(avgrecall, 2), 
        "% Avg f1 score: ", round(avgf1), "%")
        print("Confusion Matrix:\n", confmatrix)
    
    def getMetrics(self, predictedList):
        predicted = predictedList
        actual = list(self.validationset.iloc[:,0])
        metricData = self.multiclassData(predicted, actual) # get data aggregated by class
        predDict = metricData[0]
        actDict = metricData[1]
        correctDict = metricData[2]
    
        precDict = {}
        recDict = {}
        f1Dict = {}
        
        totprecision = 0
        totrecall = 0
        totf1 = 0

        for k in predDict.keys():
            if (k not in correctDict.keys() or 
                k not in actDict.keys()):
                print(k, " doesn't have sufficient data!")
                continue # skip calculating any metric for this k
            precDict[k] = (correctDict[k]*100)/predDict[k]
            recDict[k] = (correctDict[k]*100)/actDict[k]
            f1Dict[k] = (2 * precDict[k] * recDict[k])/(precDict[k] + recDict[k])
            
            totprecision = totprecision + precDict[k]
            totrecall = totrecall + recDict[k]
            totf1 = totf1 + f1Dict[k]
        
        retval = self.accuracy(predicted, actual)
        confmatrix = retval[0]
        accval = retval[1]
        classcount = len(correctDict.keys())
        if (classcount == 0):
            print("There is not enough data to show summary!")
            return ((0, 0, 0, 0, 0))
        else:
            avgprecision = totprecision/classcount
            avgrecall = totrecall/classcount
            avgf1 = totf1/classcount
            return ((confmatrix, accval, avgprecision, avgrecall, avgf1))
            
    def train(self, trainingfile):
        # Read the training data and train the kNN classifier
        mydata = pd.read_csv(trainingfile, header=None) # nothing to train in knn
        self.fullset = self.preprocess_dataset(mydata) # just supply the missing data, no encoding yet. 
        # df = self.train_q2(trainingfile)
    
    def predict(self, testfile):
        # Run the prediction on the test data using the trained model
        
        return self.predict_q2(testfile)

    # Assumes that training data has been loaded and split by calling train_q2. 
    # Uses the data to calculate different values of k and accuracy tuple for a given measure
    def experimentWithMeasure(self, maxK, distmeasure):
        retlist = list()
        distmatrix = self.findKNearestNeightborsV2(distmeasure)
        trainingset = self.trainingset
        validationset = self.validationset
        sampleCount = validationset.shape[0]
        #print("Distmatrix shape: ", distmatrix.shape)
        for k in range(3, maxK):
            #print("EWM: k = ", k)
            result_list = self.getPredictedLabel(distmatrix, trainingset, sampleCount, k)
            metrics = self.getMetrics(result_list)
            retlist.append((k, distmeasure, metrics[0], metrics[1], metrics[2], metrics[3], metrics[4]))
        return retlist # contains k and various metrics for the given k


    def otherClassifier_baseline(self, ctype):
        # Whenever we have to estimate, we just pick a random response, 1 of the 10 digits. 
        #val = [0,1,2,3,4,5,6,7,8,9]
        val = ['e', 'p']
        weights = []
        w1 = Counter(self.trainingset.iloc[:,0])
        for i in val:
            weights.append(w1[i]) # look up the frequency and add it in the weight list
        
        # print('Weights: ', weights)
        validationset = self.validationset
            
        result_list = list()
        for i in range(validationset.shape[0]):
            if (ctype == 'random'):
                label = random.choice(val)
            else: 
                labels = random.choices(val, weights=weights)
                # print("Label for weighted: ", labels[0])
                label = labels[0]
            
            result_list.append(label)
        return result_list

    def cleverencoding(self, testset, trainingset):
        # Merge both sets of data, then encode. 
        # testdata has missing col as col 11, while training has it at 1, so we need to fix it. 
        #print("In cleverencoding:")
        #print("Testset shape: ", testset.shape)
        #print("trainingset shape: ", trainingset.shape)
        
        misscol = 11
        missingdf = testset.iloc[:, misscol] # the dataframe/series with the col with missing values
        testset = testset.drop(testset.columns[misscol], axis=1) # drop the col with missing values for encoding. 
        #print("Testset shape: ", testset.shape)
        misscol = 0 # We will move misscol to 1st position
        testset.insert(misscol, "random", missingdf) # this is the new location of this column
        #print("Testset shape: ", testset.shape)
        droppedcol = trainingset.iloc[:, 0] # The first col
        newtset = trainingset.iloc[:, 1:trainingset.shape[1]] # drop the first column
        #print("trainingset shape: ", newtset.shape)
        
        # Now let's add column names so that it is easy to merge them. 
        colnames = list(range(testset.shape[1]))
        testset.columns = colnames
        newtset.columns = colnames
        
        # Let's concatenate now
        mergedset = pd.concat([testset, newtset])
        #print("merged set shape: ", mergedset.shape)
        #print("From cleverencoding")
        
        tset = self.onehotencoding(mergedset, 0)
        # now let's retrieve the rows for testset. 
        newtestset = tset.iloc[0:testset.shape[0],:] # pick the first few rows, by dropping training set rows
        #print("Testset shape: ", newtestset.shape)
        newtrainingset = tset.iloc[testset.shape[0]:tset.shape[0], :]
        newtrainingset.insert(0, "random", droppedcol) # added the 0th column back. 
        #print("trainingset shape: ", newtrainingset.shape)
        
        return list((newtestset, newtrainingset))


        
    def onehotencoding(self, fullset, start = 1):
        # for each of the column, generate their encoding. 
        tset = fullset
        df = tset.iloc[:, 0:start] # keep the target column
        for i in range(start, tset.shape[1]):
            col = tset.iloc[:, i]
            df1 = pd.get_dummies(col)
            #print("col: ", i, "new count:", df1.shape[1], Counter(col))
            df = pd.concat([df, df1], axis=1)
        # df is the new dataset we should work with. 

        # print(df.iloc[0])
        return df

    def missingvalueMode(self, tset, misscol = 11):
            #print("Original tset: ", tset.shape)
            mask = tset.iloc[:, misscol].isin(['?'])
            tset2 = tset[~mask] # all rows without ?
            # print("Filtered tset: ", tset2.shape)

            mode = st.mode(tset2.iloc[:,misscol])
            #print("Mode is ", mode)
            tset.iloc[:, misscol].replace({'?': mode}, inplace=True) # replace ? with mode.
            return tset

    def encode_dataset(self, start=0):
        #print("From encode_dataset")
        if (self.encoded == 0):
            tset = self.onehotencoding(self.fullset, start=1) # skip first col when encoding since that is actual value
            #print("EDS: tset shape:", tset.shape)
            self.split_data_q2(tset) # set up training and validation split
        else:
            print("encode_dataset: Data is already encoded!")
        return tset
      
    def preprocess_dataset(self, fullset, start=1, type = 'knn', misscol=11):
        # do 1-hot encoding of data before starting. 
        
        # Also we need to take care of missing data. 
        # col 12 has missing data. We can replace it with mean, or median, or mode. 
        # we can also use kNN to do the same. 
        tset = fullset
        #print("Original DF shape: ", tset.shape)
        #print("Frequency of missing column values (before): ", Counter(tset.iloc[:, misscol]))
        # tset = tset.drop(tset.columns[0], axis=1) # dro the col with target value 
        if (type == 'mode'):
            tset = self.missingvalueMode(tset)
        else:
            tset = self.missingvaluekNN(tset, misscol)
        #print("New DF shape: ", tset.shape)
        #print("Frequency of missing column values (after): ", Counter(tset.iloc[:, 1])) # 1 because col with missing values is at that pos.
        
        return tset # encode so that distance matrix can be done. 
        # we are now ready to work with the data. 
        
    def missingvaluekNN(self, tset, misscol):
        from q1 import KNNClassifier as knc
        knn_classifier = knc()
        """
        1. Encode. 
        2. Estimate the rows with ? using training data of rows without ?
        3. Drop the column with missing values and add the new values. 

        """
        missingdf = tset.iloc[:, misscol] # the dataframe/series with the col with missing values
        tset = tset.drop(tset.columns[misscol], axis=1) # drop the col with missing values for encoding. 
        misscol = 1 # We will move misscol to 1st position
        tset.insert(misscol, "random", missingdf) # this is the new target column
        # Need to encode so that we can apply kNN
        #print("From kNN")
        tset_encoded = self.onehotencoding(tset, 2) # Skip first 2 when encoding
        mask = tset_encoded.iloc[:, misscol].isin(['?']) # create the mask of the rows that have ?
        # tset_test = tset_encoded[mask] # all rows with ?
        tset_train = tset_encoded[~mask] # all rows without ?
        knn_classifier.traindf_q1(tset_train.iloc[:, misscol:tset_train.shape[1]])
        # We pass the entire data set to be tested so that we get complete column back. 
        # predictions = knn_classifier.predictdf_q1(tset_test.iloc[:, misscol+1:tset_test.shape[1]])
        predictions = knn_classifier.predictdf_q1(tset_encoded.iloc[:, misscol+1:tset_encoded.shape[1]])
        # print("Predictions: ", predictions)
        
        # We can throw away the encoded version of tset. 
        tset = tset.drop(tset.columns[misscol], axis=1) # drop the col with missing values for encoding. 
        tset.insert(misscol, "random", predictions) # this should hold new values of the col with missing values.  
        
        return tset


        

def test_trainingV2(k = 5, subsetData = 0.01):

    knn = KNNClassifier()
    t0 = time.time()
    df = knn.train_q2("./Datasets/q1/train.csv") 
    t1 = time.time()
    
    print(len(df), ' records are read. Time taken (seconds): ', round(t1-t0, 5))

    rowcount = knn.validationset.shape[0]

    t2 = time.time()
    distmatrix = knn.findKNearestNeightborsV2()
    t3 = time.time()
    print("Training size: ", knn.trainingset.shape[0], " Validation size: ", rowcount, 
            " Time taken: ", round(t3-t2, 5))

    result_list = knn.getPredictedLabel(distmatrix, knn.trainingset, rowcount, k)
    t4 = time.time()
    metriclist = knn.getMetrics(result_list)
    knn.showMetrics(metriclist)
    t5 = time.time()
    
    print("Total time taken in the kNN selection loop", round(t5-t4, 5))
    print("Total time taken by the program: ", round(t5-t1, 5))
    
def test_trainingV3():
    knn = KNNClassifier()
    knn.train_q2("./Datasets/q2/train.csv") 
    knn.encode_dataset(1) # sets up encoding. 
    #predictions = knn.predict('./Datasets/q2/test.csv')
    print("Training Set: ", knn.trainingset.shape)
    print("Validation Set: ", knn.validationset.shape)
    
    ret1 = knn.experimentWithMeasure(15, 'euclidean')
    #print(ret1)
    for x in ret1:
        print("K: ", x[0], ", Distance Metric is ", x[1], ", Accuracy(%) = ", round(x[3], 2))
    


def test_module():
    print("Hello! q2 is loaded.")

def test_trainingV4():
    knn = KNNClassifier()
    knn.train_q2("./Datasets/q2/train.csv") 
    knn.encode_dataset(1) # sets up encoding. 
    #print("Training Set: ", knn.trainingset.shape)
    #print("Validation Set: ", knn.validationset.shape)
    result = knn.predict_q2('./Datasets/q2/test.csv')
    result_list = knn.otherClassifier_baseline('random')
    metriclist = knn.getMetrics(result_list)
    print("\n\nRandom Classifier results: ")
    knn.showMetrics(metriclist)

    result_list = knn.otherClassifier_baseline('weighted')
    metriclist = knn.getMetrics(result_list)
    print("\n\nWeighted Random Classifier results: ")
    knn.showMetrics(metriclist)


# test_trainingV4()
def test_missing():
    knn = KNNClassifier()
    knn.train_q2("./Datasets/q2/train.csv")
    knn.encode_dataset(1) # sets up encoding. 
    # knn.predict_q2()
    #knn.preprocess_dataset()
    #knn.missingvaluekNN()

def time_q2run(k = 5, distmeasure = 'euclidean'):

    # Step 1: Read the file
    knn = KNNClassifier()
    t0 = time.time()
    df = knn.train_q2("./Datasets/q2/train.csv") 
    knn.encode_dataset(1) # sets up encoding. 
    t1 = time.time()

    # Step 2: Calculate the distance    
    t2 = time.time()
    distmatrix = knn.findKNearestNeightborsV2(distmeasure)
    t3 = time.time()
    
    # Step 3: Find the predicted labels
    rowcount = knn.validationset.shape[0]
    result_list = knn.getPredictedLabel(distmatrix, knn.trainingset, rowcount, k)
    t4 = time.time()

    # Step 4: Calculate and print metrics
    metriclist = knn.getMetrics(result_list)
    knn.showMetrics(metriclist)
    t5 = time.time()

    print("Program time: ", round(t5-t0, 5), 
    "seconds, Run time (= program time - file read time - metric display time): ", round((t4-t2), 5), "seconds")

#test_trainingV3()
#time_q2run()

#test_missing()
