"""

       Attribute Information:
	
            1. Formatted Date
            2. Summary
            3. Precip Type // it has 3 values rain, snow and null, there is no missing value it's null. 
            4. Temperature (C)
            5. Apparent Temperature (C) [Output Class]
            6. Humidity
            7. Wind Speed (km/h)
            8. Wind Bearing (degrees)
            9. Visibility (km)
            10. Pressure (millibars)
            11. Daily Summary

"""
import numpy as np 
import pandas as pd 
from matplotlib import pyplot as plt
from sklearn.feature_extraction.text import TfidfVectorizer
import time
import q3 # reuse the implementation done for q3

class Weather:
    def __init__(self):
        self.originaldata = 0
        self.processeddata = 0
        self.outputvariable = 0
        self.B = 0
        self.costs = 0
        self.af = 0


    def process_datetime(self, ds):
        """
        Date time needs these: absolute_time, day_of_year, day_of_week, month_of_year, hour_of_day, minute_of_hour
        
        """
        ds = pd.DatetimeIndex(ds, tz=None)
        #ds = pd.DataFrame(ds)
        

        #absolute_time = (ds - ) // pd.Timedelta('1s')
        year = ds.year
        quarter = ds.quarter # might capture seasonality better
        month = ds.month
        week = ds.week
        day = ds.day
        day_of_week = ds.dayofweek
        hour = ds.hour
        
        newds = pd.DataFrame({#'abs_time': absolute_time, 
                                'year': year, 
                                'quarter': quarter, 
                                'month': month, 
                                'week': week, 
                                'day': day, 
                                'day_of_week': day_of_week, 
                                'hour': hour})

        #newds = pd.DataFrame([absolute_time, year, quarter, month, week, day, day_of_week, hour])

        #print("Date Time table (", newds.shape, ")\n", newds)
        return newds

    def process_1hot(self, ds):
        newds = pd.DataFrame(pd.get_dummies(ds))
        #print("1hot table (", newds.shape, ")\n", newds)
        
        return newds

    def process_vectorize(self, ds, vocab = None):
        vectorizer = TfidfVectorizer(vocabulary = vocab)
        X = vectorizer.fit_transform(ds)
        colnames = range(X.shape[1])
        colnames = [ds.name + '_' + str(s) for s in colnames]
        newds = pd.DataFrame(X.toarray(), columns=colnames) # Convert into dense metrix

        #print("vectorize table (", newds.shape, ")\n", newds)
        return newds, vectorizer.vocabulary_


    

    def process_data(self, data):
        """
 
        # Date should be converted into absolute_time, day_of_year, day_of_week, month_of_year, hour_of_day, minute_of_hour cols.
        
        Summary and and Daily summary should be vectorized
        Precip type should be 1-hot encoded
        Also, we should move the output variable (#5) out from the data easier processing. and create it as a separate Y value

        """
        
        df_dt = self.process_datetime(data.iloc[:,0])
        df_1hot = self.process_1hot(data.iloc[:, 2]) # precip type
        df_vectorize1, self.vocab1 = self.process_vectorize(data.iloc[:, 1])
        df_vectorize2, self.vocab2 = self.process_vectorize(data.iloc[:, 10])
        
        newdf = pd.concat([df_dt, df_1hot, df_vectorize1, df_vectorize2, data.iloc[:, 3], data.iloc[:, 5:10], data.iloc[:, 4]], axis = 1 )
        
        print("Column names: \n", newdf.columns)
        return newdf 



    def train(self, filename, alpha = 0.0000001, iteration = 100000):
        dataset = pd.read_csv(filename, infer_datetime_format = True, date_parser=lambda col: pd.to_datetime(col, utc=True), parse_dates=["Formatted Date"], converters = {"Precip Type": convert_preciptype}) # Read date column like a date
        print(dataset.dtypes)
        self.originaldata = dataset
        
        newdf = self.process_data(dataset) # convert the columns from textual to numeric
        self.processeddata = newdf

        af = q3.Airfoil()
        XY = np.array(self.processeddata)
        B = [0]*XY.shape[1]
        
        af.internal_train2(XY, B, alpha, iteration, validate=0) # Just run the regression, don't do training/validate

        self.B = af.B
        self.costs = af.costs
        self.af = af # keep the classifier around just in case. 

    
        #print("Original Data: \n", self.originaldata)
        #print("\n---------\nProcessed data: \n", self.processeddata)
        

        """
        VALIDATION_SET_SIZE = 0.2
        mask = np.random.rand(len(dataset)) <= VALIDATION_SET_SIZE
        self.vdata = dataset[mask]
        self.tdata = dataset[~mask]
        """
        #print("To be implemented")
    
    def predict(self, filename):
        print("To be implemented")
        return list()
        

def convert_preciptype(val):
    if (val == ""):
        return "null"
    else:
        return val

def runtest():
    model4 = Weather()
    #model4.train('Datasets/Question-4/mytest.csv') # Path to the train.csv will be provided 
    model4.train('Datasets/Question-4/weather.csv') # Path to the train.csv will be provided 
    prediction4 = model4.predict('./Datasets/q4/test.csv') # Path to the test.csv will be provided

#runtest()