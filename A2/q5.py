"""
Support Vector Machine
1. Given a dataset which contains a excerpts of text written by some author and the
corresponding author tag, implement an SVM classier to predict the author tag
of the test text excerpts.
2. For the feature extraction of the text segments, either use Vectorizers provided in
sklearn or use pre-trained word embedding models. ( Code snippet for usage of
word embedding models is given here).
3. Visualize the feature vectors and see if you could nd some pattern.
4. Tweak dierent parameters of the Linear SVM and report the results.
5. Experiment dierent kernels for classication and report the results.
6. Report accuracy score, F1-score, Confusion matrix and any other metrics you feel
useful.
7. (Bonus-20 points) You may do some pre-processing on textual data to improve
your classier. Explain why score has improved if it did.
8. Link to the dataset has been provided in the common link.

"""

"""
Solution
"""
import numpy as np
import time
from sklearn import svm
import pickle
import sklearn.metrics as metrics
import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer

class AuthorClassifier:
    def __init__(self):
        self.dataset = 0
        self.kernel = 'linear'
        self.vocabulary = None
        self.clf = 0
        self.tX = 0
        self.tV = 0
        self.tinputs = 0
        self.tlabels = 0
        self.vV = 0
        self.vX = 0
        self.vinputs = 0
        self.vlabels = 0
        self.vscore = 0
        self.predicted_labels = 0
        self.predictduration = 0
        self.vdata = 0
        self.tdata = 0

    


    def metric_score(self, actual, predicted):
        vscore = metrics.accuracy_score(actual, predicted)
        return vscore
        
    def svm_predict(self, vX):
        time2 = time.time()
        predicted_labels = self.clf.predict(vX)
        time3 = time.time()
        predictduration = time3 - time2
        return predicted_labels, predictduration

    def svm_fit(self, tX, tlabels, kernel = 'linear', degree = 3):
        # kernel has to be one of ‘linear’, ‘poly’, ‘rbf’, ‘sigmoid’
        time1 = time.time()
        clf = svm.SVC(kernel=kernel, random_state = 0)
        clf.fit(tX, tlabels)
        time2 = time.time()
        fitduration = time2 - time1
        return clf, fitduration

    def train(self, filename):
        dataset = pd.read_csv(filename)
        self.dataset = dataset
        VALIDATION_SET_SIZE = 0.2
        mask = np.random.rand(len(dataset)) <= VALIDATION_SET_SIZE
        self.vdata = dataset[mask]
        self.tdata = dataset[~mask]
        self.tV, self.tX, self.tinputs, self.tlabels = self.vectorize(self.tdata) # Training data
        self.vocabulary = self.tV.vocabulary_ # need to use same vocab for test data. 
        self.clf, self.fitduration = self.svm_fit(self.tX, self.tlabels)


    def predict(self, filename, labels = False):
        testdata = pd.read_csv(filename)
        self.testdata = testdata
        self.vV, self.vX, self.vinputs, self.vlabels = self.vectorize(testdata, self.vocabulary, labels=labels)
        self.predicted_labels, self.predictduration = self.svm_predict(self.vX)
        if (labels == True):
            self.vscore = self.metric_score(self.vlabels, self.predicted_labels)
        else:
            self.vscore = -1
        
        return self.predicted_labels

    def vectorize(self, dataset, vocab = None, labels = True):
        vectorizer = TfidfVectorizer(vocabulary = vocab)
        inputs = dataset.iloc[:,1]
        
        if (labels == True):
            labels = dataset.iloc[:,2]
        else:
            labels = 0
        X = vectorizer.fit_transform(inputs)
        return vectorizer, X, inputs, labels
    


    


                

