"""

Question 2
Gaussian Mixture Models Clustering

1. You are given 3 data les(dataset1.pkl,dataset2.pkl,dataset3.pkl) and 1 code le
gmm.py. The code consists of -
(a) Function to load dataset.
(b) Function to save dataset.
(c) Class GMM1D which consists multiple functions.

2. Load dataset .

3. Use inbuilt sklearn functions to cluster(GMM clustering) the points and plot them.
Also report no of iterations taken to converge.
4. In GMM1D, fill in the blanks with code and cluster the points. Plot for each
iteration.
5. (Bonus-20 points) Plot the log likelihood graph to show the behaviour.
"""
"""
Solution:

References: 
https://pythonmachinelearning.pro/clustering-with-gaussian-mixture-models/
https://jakevdp.github.io/blog/2013/12/01/kernel-density-estimation/
https://jakevdp.github.io/PythonDataScienceHandbook/05.12-gaussian-mixtures.html
https://www.analyticsvidhya.com/blog/2015/12/improve-machine-learning-results/
https://www.python-course.eu/expectation_maximization_and_gaussian_mixture_models.php

"""

import matplotlib.pyplot as plt
from matplotlib import style
import numpy as np
from scipy.stats import norm
import pickle
from sklearn.mixture import GaussianMixture 


np.random.seed(0)


def load(name):
    file = open(name,'rb')
    data = pickle.load(file)
    file.close()
    return data

def save(data,name):
    file = open(name, 'wb')
    pickle.dump(data,file)
    file.close()

class GMM1D:
    def __init__(self,X,iterations,initmean,initprob,initvariance):
    #initmean = [a,b,c] initprob=[1/3,1/3,1/3] initvariance=[d,e,f]  
        self.iterations = iterations
        self.X = X
        self.mu = initmean
        self.pi = initprob
        self.var = initvariance
    
    """E step"""

    def calculate_prob(self,r):
        for c,g,p in zip(range(3),[norm(loc=self.mu[0],scale=self.var[0]),
                                       norm(loc=self.mu[1],scale=self.var[1]),
                                       norm(loc=self.mu[2],scale=self.var[2])],self.pi):
            r[:,c] = list(p*g.pdf(self.X))

        """
        Normalize the probabilities such that each row of r sums to 1 and 
        weight it by mu_c == the fraction of points belonging to 
        cluster c
        """
        tot = np.sum(r, axis = 1) # Sum all the r values for each row. 
        
        for i in range(len(r)):
            # to make the sum = 1, we need to divide each prob by the sum of the prob of that row
            # to have this weighted by the # of cluster, we just divide by the sum of 
            # he probability of belonging to a cluster
            r[i] = r[i]/(tot[i] * np.sum(self.pi))
        	
        return r
    
    def plotnew(self, r):
        print("To be completed")



    def dumpData(self, iter, r):
        print("\n##############################")
        print("Iter #:   ", iter)
        print("Mu:       ", self.mu)
        print("Variance: ", self.var)
        print("pi:       ", self.pi)
        #print("r matrix")
        #print(r)
        print("##############################")
            
    def plot(self,r):
        fig = plt.figure(figsize=(10,10))
        ax0 = fig.add_subplot(111)
        for i in range(len(r)):
            ax0.scatter(self.X[i],0,c=np.array([r[i][0],r[i][1],r[i][2]]).reshape(1,-1),s=100)
        """Plot the gaussians"""
        for g,c in zip([norm(loc=self.mu[0],scale=self.var[0]).pdf(np.linspace(-20,20,num=60)),
                        norm(loc=self.mu[1],scale=self.var[1]).pdf(np.linspace(-20,20,num=60)),
                        norm(loc=self.mu[2],scale=self.var[2]).pdf(np.linspace(-20,20,num=60))],['r','g','b']):
            ax0.plot(np.linspace(-20,20,num=60),g,c=c)
        
    def run(self):
        
        for iter in range(self.iterations):

            n_comps = 3 # Just define the constant rather than using 3 everywhere! 
            n_samples = len(self.X)
            """Create the array r with dimensionality nxK"""
            r = np.zeros((n_samples,3))  

            """
            Probability for each datapoint x_i to belong to gaussian g 
            """
            r = self.calculate_prob(r)
            
            self.dumpData(iter, r)
        

            """Plot the data"""
            self.plot(r)
            
            """M-Step"""

            """calculate m_c"""
            mc_array = r.sum(axis = 0) # Add up so that we get 3 values, one for each c. We will use array object later as well. 
            m_c = mc_array.flatten().tolist()
            
            """calculate pi_c"""
            self.pi = (mc_array/sum(m_c)).flatten().tolist()

            
            """calculate mu_c"""

            # Do matrix multiplication. We have r as 60x3, x as 60x1, we want the output to be 3x1, one for each cluster. 
            # So we do r_transpose multiplied by x and then convert into a list. 

            m1 = np.dot(r.T, self.X.reshape(n_samples, 1))
            m2 = m1.T/mc_array
            self.mu = m2.flatten().tolist() # have to also divide each by its m_c, so array division)

            """calculate var_c"""
            
            ## To do this matrix multiplication for covariance, we use a mix of numpy array multiplication and dot product capabilities. 
            # First we multiply r with X_MATRIX (repeated transpose of X_ARRAY to produce 60 X 3 matrix) to get a 60 X 3 matric. Say we get rx. 
            # We then do dot product of transpose of rx and X_ARRAY to produce a 3 x 1 matrix which is the variance we seek. 
            
            x_arr = np.array(self.X).reshape(n_samples, 1)
            # Repeat x array by col, so that it is easy to multiply. Also subtract the mu_c. 
            x_mat = np.repeat(x_arr, n_comps, axis=1) - np.array(self.mu).reshape(1, n_comps) 
            rx = r * x_mat
            var_c = np.dot(rx.T, x_arr).T/mc_array 
            self.var = var_c.flatten().tolist()
            
            
            plt.show()


def plotInputData(data, data1, data2, data3):
    plt.scatter(data1,np.full((len(data1), ), 0), c="r", marker="o")
    plt.scatter(data2,np.full((len(data2), ), 1), c="b", marker="x")
    plt.scatter(data3,np.full((len(data3), ), 2), c="g", marker="+")
    #plt.scatter(data, 0)

def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))

def plotGaussian(data, glist, low=-20, high=20):
    x_values = np.linspace(low, high, len(data))
    for mu, sig in glist:
        plt.plot(x_values, gaussian(x_values, mu, sig))
    plt.scatter(data,np.zeros(len(data)))
    plt.show()

    
def runModel(data, comp = 3):
    gmm = GaussianMixture(comp)
    gmm.fit(data)
    glist = list()
    for i in range (len(gmm.means_)):
        glist.append([gmm.means_[i][0],gmm.covariances_[i][0][0]])
    return gmm, glist



def showMetrics(gmm, glist):
    print("Converged: ", gmm.converged_, "No. of iterations: ", gmm.n_iter_)
    print("Gaussians:")
    i = 0
    for mu, sig in glist:
        print ("i: ", i, " Mu: ", mu, "Sigma: ", sig)
        i = i + 1




"""
To run the code - 
g = GMM1D(data,10,[mean1,mean2,mean3],[1/3,1/3,1/3],[var1,var2,var3])
g.run()
"""

def readAndMergeData(plot = 1):
    data1 = load('Datasets/Question-2/dataset1.pkl')
    data2 = load('Datasets/Question-2/dataset2.pkl')
    data3 = load('Datasets/Question-2/dataset3.pkl')
    fulldata = np.stack((data1, data2, data3)).flatten().reshape(-1,1)
    if (plot == 1):
        plotInputData(fulldata, data1, data2, data3)

    return fulldata

    
def runtest():
    initmean = [1, 2, 100]
    initprob = [1/3, 1/3, 1/3]
    initvariance = [10, 100, 1000]
    Xdata = readAndMergeData()
    g = GMM1D(Xdata, 10, initmean, initprob, initvariance)
    g.run()

#runtest()


