import numpy as np 
import pandas as pd 
from matplotlib import pyplot as plt
from sklearn.feature_extraction.text import TfidfVectorizer
import time
from nltk.stem import PorterStemmer
import os



class Cluster:
    def __init__(self):
        self.data = np.empty([1,1]) # just to initialize


    def get_clusterdict(self, dataset, cluster_index, k_clusters):
        clusterlist = list()
        for i in range(k_clusters):
            clusterid = i
            clusterdata = dataset[cluster_index == i] # Get the dataframe data. 
            clusterlist.append((clusterid, clusterdata))

        cdict = dict()
        for clusterid, clusterdata in clusterlist:
            for filename in clusterdata['filename']:
                cdict[filename] = clusterid
        
        #print(cdict)        
        
        return cdict

    
    def cluster(self, filedir):
        # Take the directory, read the files, vectorize them, and send out a dictionary of file to label mapping

        dlist, colnames = self.loadData(filedir, labels = False)
        dataset = pd.DataFrame(dlist, columns = colnames)

        tV, tX, tinputs, tlabels = vectorize(dataset, labels = False) # Test data
        
        self.data = tX.toarray() # convert into dense matrix to make it easier to work with numpy arrays

        k_clusters = 5
        centroids, cluster_index, euc_distance, iter, clusteringduration = self.makeCluster(k_clusters)

        cdict = self.get_clusterdict(dataset, cluster_index, k_clusters)
        return cdict

    def loadData(self, path, labels = True):
        txt_files = os.listdir(path)
        
        dlist = list()

        for f in txt_files:
            with open(path + f, "r") as fd:
                content = stem_all(fd.read())
            if (labels == True):
                fname = f.split(".")
                label = fname[0].split("_")[1]
                tuple = (f, content, label)
            else:
               tuple = (f, content)
            
            dlist.append(tuple)

        colnames = ['filename', 'article']
        if (labels == True):
            colnames.append('topic')
        return dlist, colnames    


    def generateRandomData(self):
        c1 = np.array([1,1])
        c2 = np.array([5,8])
        c3 = np.array([3,0])
        c4 = np.array([9,3])
        
        # Generate random data and center it to the three centroids
        d1 = np.random.randn(100, 2) + c1
        d2 = np.random.randn(200, 2) + c2
        d3 = np.random.randn(300, 2) + c3
        d4 = np.random.randn(400, 2) + c4
       
        data = np.concatenate((d1, d2, d3, d4), axis = 0)

        return data


    def plotData(self, data, centroids, title = "Sample data and cluster centroids"):
        plt.scatter(data[:,0], data[:,1], marker = 'o', c = 'b', s=10)
        plt.scatter(centroids[:,0], centroids[:,1], marker='X', c='r', s=100)
        plt.show()
    
    def getRandomCentroids(self, data, k_clusters, n_features):
        # Generate random centroids, here we use sigma and mean to ensure it represent the whole data
        mu = np.mean(data, axis = 0)
        sigma = np.std(data, axis = 0)
        centroids = sigma * np.random.randn(k_clusters,n_features) + mu
        #centroids = data[0:k_clusters]
        return centroids

    def makeCluster(self, k_clusters, plot = 0):
        # Number of training data
        data = self.data
        n_samples, n_features = data.shape
        
        time1 = time.time()
        
        prevcentroids = np.zeros([k_clusters, n_features]) # We need to keep 2 versions of centroid, one for current iteration, another for next iteration. 
        
        centroids = self.getRandomCentroids(data, k_clusters, n_features)
        
        if (plot == 1):
            self.plotData(data, centroids, title = "Randomly selected centroids")
        
        
        cluster_index = np.zeros(n_samples) # This will be used to create the location mask - which cluster the sample belongs to
        euc_distance = np.zeros((n_samples, k_clusters))

        iter = 0 # Initialize the iteration counter
        found = False

        while (found == False) :
            delta = np.linalg.norm(centroids - prevcentroids)
            print("Iteration: ", iter, "Delta: ", delta)

            prevcentroids = np.copy(centroids) # Make a copy. Just assignment will create a ref
            
            # Step 1: Calculate the distances of each point from each centroid. 
            for i in range(k_clusters):
                distmatrix = data - centroids[i]

                euc_distance[:,i] = np.linalg.norm(distmatrix, axis=1)
            
            # Step 2: Identify which one is the closest centroid for each point
            cluster_index = np.argmin(euc_distance, axis = 1) # find the index which gives the min value. 
            
            
            # Step 3: Find next approximation of the location for each centroid based on the points that belong to it
            for i in range(k_clusters):
                centroids[i] = np.mean(data[cluster_index == i], axis=0)

            # Step 4: If there is at least one centroid that moved, then we continue to iterate. 
                    
            #delta = np.linalg.norm(nextcentroids - centroids)
            if (np.array_equal(centroids, prevcentroids) == True):
                print("Found! Iter = ", iter)
                found = True
            
            iter = iter + 1

        if (plot == 1):
                self.plotData(data, centroids, title = "Final Plot")

        time2 = time.time()
        clusteringduration = time2 - time1

        return centroids, cluster_index, euc_distance, iter, clusteringduration

def vectorize(dataset, vocab = None, labels = True):
    vectorizer = TfidfVectorizer(vocabulary = vocab)
    inputs = dataset['article']
    if (labels == True):
        labels = dataset['topic']
    else:
        labels = 0
    X = vectorizer.fit_transform(inputs)
    return vectorizer, X, inputs, labels

def runtest():
    cluster = Cluster()
    cluster.data = cluster.generateRandomData()
    cluster.makeCluster(4)

#runtest()

def get_percent(freq):
    d = dict(freq)
    #print("Dict: ", d)
    true_label = list(d.keys())[0]
    total = sum(d.values())
    for key, value in d.items():
        #print(key, round((value*100/total), 2), "%")
        if d[true_label] < value:
            true_label = key
    
    return true_label, round((d[true_label]*100/total), 2)



def verify_clustering(clusterlist):
    # clusterlist has cluster id and the corresponding data. Data has truth label. 
    for clusterid, clusterdata in clusterlist:
        print("\nClustered group   ", clusterid)
        freq = clusterdata['topic'].value_counts()
        clid, clpercent = get_percent(freq)
        print("Cluster ID: ", clid, " Accuracy % (% of samples belonging to dominant true cluster): ", clpercent)


def stem_all(content):
    st = PorterStemmer()
    #print("Content\n", content)
    wordlist = list()
    for word in content.split():
        wordlist.append(st.stem(word))

    stemmed_content =  " ".join(wordlist)

    #print("\nStemmed Content:\n", stemmed_content)
    return stemmed_content


def runtest2():
    import os

    cluster = Cluster()
    path = "Datasets/Question-6/dataset/"
    dlist, colnames = cluster.loadData(path)
    dataset = pd.DataFrame(dlist, columns = colnames)

    tV, tX, tinputs, tlabels = vectorize(dataset) # Training data
    #vocab = tV.vocabulary_ # need to use same vocab for test data. 
    #vV, vX, vinputs, vlabels = vectorize(vdata, vocab) # validation data

    cluster.data = tX.toarray() # convert into dense matrix to make it easier to work with numpy arrays

    k_clusters = 5
    centroids, cluster_index, euc_distance, iter, clusteringduration = cluster.makeCluster(k_clusters)

    clusterlist = list()
    for i in range(k_clusters):
            clusterid = i
            clusterdata = dataset[cluster_index == i] # Get the dataframe data. 
            clusterlist.append((clusterid, clusterdata))
    verify_clustering(clusterlist)


    

#runtest2()



