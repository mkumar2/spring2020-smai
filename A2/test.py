from sklearn.metrics import accuracy_score
from sklearn.metrics import mean_squared_error


from q3 import Airfoil as ar
print("Starting Q3..\n")
model3 = ar()
model3.train('./Datasets/q3/train.csv') # Path to the train.csv will be provided
prediction3 = model3.predict('./Datasets/q3/test.csv') # Path to the test.csv will be provided
print("Q3 completed\n")
# prediction3 should be Python 1-D List
'''WE WILL CHECK THE MEAN SQUARED ERROR OF PREDICTIONS WITH THE GROUND TRUTH VALUES'''

from q4 import Weather as wr
print("Starting Q4..\n")
model4 = wr()
model4.train('./Datasets/q4/train.csv') # Path to the train.csv will be provided 
prediction4 = model4.predict('./Datasets/q4/test.csv') # Path to the test.csv will be provided
print("Q4 completed\n")
# prediction4 should be Python 1-D List
'''WE WILL CHECK THE MEAN SQUARED ERROR OF PREDICTIONS WITH THE GROUND TRUTH VALUES'''


from q5 import AuthorClassifier as ac
print("Starting Q5..\n")
auth_classifier = ac()
auth_classifier.train('./Datasets/q5/train.csv') # Path to the train.csv will be provided
predictions = auth_classifier.predict('./Datasets/q5/test.csv') # Path to the test.csv will be provided
print("Q5 completed\n")

'''WE WILL CHECK THE PREDICTIONS WITH THE GROUND TRUTH LABELS'''


from q6 import Cluster as cl
print("Starting Q6..\n")
cluster_algo = cl()
# You will be given path to a directory which has a list of documents. You need to return a list of cluster labels for those documents
predictions = cluster_algo.cluster('./Datasets/q6/') 
print("Q6 completed\n")

'''SCORE BASED ON THE ACCURACY OF CLUSTER LABELS'''