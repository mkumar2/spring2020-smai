import numpy as np 
import pandas as pd 
from matplotlib import pyplot as plt
import time

"""
Column description
    1. Frequency, in Hertzs.
	2. Angle of attack, in degrees.
	3. Chord length, in meters.
	4. Free-stream velocity, in meters per second.
	5. Suction side displacement thickness, in meters.
	6. Scaled sound pressure level, in decibels.

"""
class Airfoil:
    def __init__(self):
        self.data = 0 
        self.columns = ['frequency', 'attack-angle', 'chord-length', 'free-stream-velocity', 'displacement-thickness', 'sound-pressure-level']
        self.vdata = 0
        self.tdata = 0
        self.costs = 0 # Cost value at each iteration
        self.X = 0 # Keep the X used for regression
        self.B = 0 # Estimated value of B post linear regression run
        self.Y = 0 # Y used for training purposes, test run will not have any Y

    def clf_train(self, data, validate = 0):
        if (validate == 1):
            VALIDATION_SET_SIZE = 0.2
            mask = np.random.rand(len(data)) <= VALIDATION_SET_SIZE
            vdata = data[mask]
            tdata = data[~mask]
        else:
            tdata = data
            vdata = 0
        
        self.tdata = tdata
        self.vdata = vdata

        return tdata, vdata
        
    def clf_setupXYB(self, data, B = [0,0,0,0,0,0]):
        n_features = data.shape[1]     
        X = data[:,:n_features-1]
        Y = data[:,n_features-1].reshape(-1,1)
        B = np.array(B).reshape(-1,1)
        #B = np.zeros(n_features).reshape(-1, 1)

        return X, Y, B

    def internal_train2(self, data, B = [0,0,0,0,0,0], alpha = 0.0000001, iteration = 100000, validate = 1):
        self.data = data
        tdata, vdata = self.clf_train(self.data, validate=validate)
        X, Y, B = self.clf_setupXYB(tdata, B)
        self.X = X
        self.Y = Y
        self.B, self.costs  = self.clf_linregression(X, Y, B, alpha=alpha, iterations = iteration) # Run the linear regression to discover value of B
        
        tmse = self.metric_MSE(self.clf_prepareX(X), Y, self.B) # need to pass the X with X0 inserted
        print("MSE of training set (internal): ", tmse)

        # Use this to run validation if needed
        if (validate == 1):
            vX, vY, vB = self.clf_setupXYB(vdata)
            vmse = self.metric_MSE(self.clf_prepareX(vX), vY, self.B)
            print("MSE of validation set: ", vmse)
        else:
            vmse = 0
        
        return tmse, vmse

    def internal_train(self, filename, B = [0,0,0,0,0,0], alpha = 0.0000001, iteration = 100000, validate = 1):
        # Read the file
        data = np.loadtxt(filename, delimiter=",", skiprows=0)
        return self.internal_train2(data, B, alpha, iteration, validate = validate)        

    def train(self, filename):
        # Read the file
        self.data = np.loadtxt(filename, delimiter=",", skiprows=0)
        tdata, vdata = self.clf_train(self.data)
        X, Y, B = self.clf_setupXYB(tdata)
        self.X = X
        self.Y = Y

        self.B, self.costs = self.clf_linregression(X, Y, B) # Run the linear regression to discover value of B
        tmse = self.metric_MSE(self.clf_prepareX(X), Y, self.B) # need to pass the X with X0 inserted
        self.Y = Y
        print("MSE of training set: ", tmse)

    


    def print_shape(self, data, text = "Matrix"):
        print(text, "Shape", data.shape)

    def predict(self, filename):
        data = np.loadtxt(filename, delimiter=",", skiprows=0)
        
        # Since data doesn't contain Y, data can be used as X. 
        X = data
        Y = np.zeros((data.shape[0], 1)) # Create a null Y so that we can reuse loss function to do estimation since estimation is X.B 
        B = self.B
        return self.clf_loss(self.clf_prepareX(X), Y, B)

    def cost_function(self, X, Y, B):
        Xt = X.dot(B) - Y
        J = np.sum(Xt * Xt)
        return J

    def clf_loss(self, X, Y, B):
        loss = X.dot(B) - Y # X.B is Y based on hypothesis that B is the right coefficient. 
        return loss

    def metric_MSE(self, X, Y, B):
        loss = self.clf_loss(X, Y, B)
        mse = np.square(loss).mean(axis = 0)
        return mse


    def gradient_descent(self, X, Y, B, alpha, iterations):

        costs = list() # Store the cost values for plotting later 
        
        for iteration in range(iterations):
            loss = self.clf_loss(X, Y, B) # Loss function
            gradient = X.T.dot(loss) / len(Y) # transpose(X).loss/m is is gradient. 
            B = B - alpha * gradient # Update B's estimate
            cost = self.cost_function(X, Y, B) # update cost estimate (MSE)
            costs.append(cost) # Keep it around for future graphing

            #print("Iteration: ", iteration, "Cost: \n", cost)
            
        return B, costs

 
    def clf_prepareX(self, X):
        n_samples, n_features = X.shape
        #Initial estimate of X
        X0 = np.ones((n_samples, 1))
        X = np.concatenate((X0, X), axis = 1) # Insert a constant term for b0
        return X


    def clf_linregression(self, X, Y, B, iterations = 100000, alpha = 0.0000001):
         # Alpha and iteration counts are random guesses for learning rate and iterations. 
         # If learning rate is not appropriate cost will start going up and diverge
         # Not sure what is the best way to estimate these

        X = self.clf_prepareX(X)
        
        newB, costs = self.gradient_descent(X, Y, B, alpha, iterations) # Perform gradient descent 

        return newB, costs 

def runtest():
    af = Airfoil()
    af.train("Datasets/Question-3/airfoil.csv")
    vY = af.predict("Datasets/Question-3/test.csv")    
    # Use this to run validation
    vX = af.data[:,0:af.data.shape[1]-1]
    vB = af.B
    vmse = af.metric_MSE(af.clf_prepareX(vX), vY, vB)
    print("MSE of test set: ", vmse)
    
#runtest()

def runinternaltest():
    af = Airfoil()
    af.internal_train("Datasets/Question-3/airfoil.csv")
    #af.internal_train("Datasets/Question-3/mytest.csv")    


#runinternaltest()
   
