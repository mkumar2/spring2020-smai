"""

Question 1
1. (100 points) Image Classication
    1. Given CiFAR-10 dataset, implement a linear SVM classier to predict the classes
    of the test images.
    2. Featurize the images as vectors that can be used for classication.
    3. Report your observations for different values of C. Explain the signicance of C.
    4. Compare and contrast the classifier with the KNN classifier built in the previous
    assignment.
    5. Report accuracy score, F1-score, Confusion matrix and any other metrics you feel
    useful.
    6. Report the support vector images in each case.
    7. (Bonus-20 points) You may do some processing on the train set to improve your
    scores on linear SVM. Report your changes clearly.
    8. You can use inbuilt functions for SVM.

"""

"""
Solution:

1. Read the file and prepare the data, use that data to do the fit. 
2. Write a function that takes a kernel as input and calls SVM to estimate using test data. 

"""
import numpy as np
import time
from sklearn import svm
import pickle
import sklearn.metrics as metrics
from sklearn.svm import LinearSVC
from sklearn import svm


class SVMClassifier:
    def __init__(self):
        self.dataset = 0
        self.kernel = 'linear'
        self.filelist = ["data_batch_1", "data_batch_2", "data_batch_3", "data_batch_4", "data_batch_5"]
        self.testfile = "test_batch"
        self.metafile = "batches.meta"
        self.filedict = list()
        self.metadict = 0
        self.testdict = 0
        self.filepath = "Datasets/Question-1/cifar-10-batches-py/"
        self.tol = 0.01 # Doesn't work with tolerance level lower than this, takes too much time to come back
    

    # Read a file
    def unpickle(self, file, path):
        with open(path + file, 'rb') as fo:
            dict = pickle.load(fo, encoding='bytes')
        return dict

    # Read all the files
    def readAllFiles(self, path):
        # Read all the files
        for f in self.filelist:
            print("Reading", f)
            dict = self.unpickle(f, path)
            self.filedict.append(dict)
            #print("Read data with shape = ",  dict["data"].shape)
            print(dict.keys())
        
        # Read the test file
        self.testdict = self.unpickle(self.testfile, path)
        
        # Read the meta file
        self.metadict = self.unpickle(self.metafile, path)
        print(self.metadict)

    def mergeDataset(self, dl):
        # ugly implementation, need to fix. 
        newdata = np.concatenate((dl[0][b'data'], 
                            dl[1][b'data'],
                            dl[2][b'data'],
                            dl[3][b'data'],
                            dl[4][b'data']), axis = 0)

        newlabels = np.concatenate((dl[0][b'labels'], 
                            dl[1][b'labels'],
                            dl[2][b'labels'],
                            dl[3][b'labels'],
                            dl[4][b'labels']), axis=0)
        return newdata, newlabels

    def maskDataset(self, data, labels, mask):
        # Pick a subset of data to try fit
        mask = np.random.choice([False, True], len(data), p=[mask, 1-mask])
        a = np.array(labels)
        a = a[mask]
        subsetlabels = a.tolist()
        subsetdata = data[mask]
        return subsetdata, subsetlabels

    def sliceData(self, data):
        return data[:,0:1024]

    def reduceData(self, data):
        # Y = 0.2125 R + 0.7154 G + 0.0721 B

        # This is image data, we can average the color data into a composite number, and use that. 
        print("Reducing data..")
        time1 = time.time()
        
        row, col = data.shape
        chsize = int(col/3)
        newdata = np.empty((row, chsize), dtype=int)
        for i in range(row):
            for j in range(chsize):
                newdata[i,j] = round(0.2125*data[i, j] + 0.7154*data[i, j+chsize] + 0.0721 * data[i, j+chsize*2]) 

        time2 = time.time()
        print("Time in reducing data: ", time2 - time1)
        
        return newdata

    def reduceData2(self, data):
        # Y = 0.2125 R + 0.7154 G + 0.0721 B

        # This is image data, we can average the color data into a composite number, and use that. 
        print("Reducing data..")
        time1 = time.time()
        
        row, col = data.shape
        chsize = int(col/3)
        newdata = np.empty((row, chsize), dtype=int)
        vround = np.vectorize(round)

        for j in range(chsize):
            newdata[:,j] = vround(0.2125*data[:,j] + 0.7154*data[:,j+chsize] + 0.0721 * data[:,j+chsize*2]) 

        time2 = time.time()
        print("Time in reducing data: ", time2 - time1)
        
        return newdata

###############################################


def optimizeData(svm1):
    print("(Before) Shape of training data: ", svm1.processedTrainingData.shape, 
            " Shape of test data: ", svm1.processedTestData.shape)
    #svm1.processedTrainingData =  svm1.reduceData2(svm1.processedTrainingData)
    #svm1.processedTestData = svm1.reduceData2(svm1.processedTestData)
    # add these columns into the existing data
    np.concatenate((svm1.processedTrainingData, svm1.reduceData2(svm1.processedTrainingData)), axis = 1)
    np.concatenate((svm1.processedTestData, svm1.reduceData2(svm1.processedTestData)), axis = 1)
    
    print("(After) Shape of training data: ", svm1.processedTrainingData.shape, 
            " Shape of test data:", svm1.processedTestData.shape)
    
"""

Prepare data for further processing. This will read the files, mask them appropriately and produce the 
subset data that will be used for prediction. 
Returns the classifier object, the subsetdata and subsetlabels

"""

def prepareData(mask = 0.8):
    svm1 = SVMClassifier()
    svm1.readAllFiles(svm1.filepath)
    tempdata, templabels = svm1.mergeDataset(svm1.filedict)
    newdata, newlabels = svm1.maskDataset(tempdata, templabels, mask)

    # Update the processeddata fields
    svm1.processedTrainingData = newdata
    svm1.processedTrainingLabels = newlabels
    svm1.processedTestData = svm1.testdict[b'data']
    svm1.processedTestLabels = svm1.testdict[b'labels']
    

    return svm1, newdata, newlabels
    
"""

Print the relevant metrics: accuracy, F1, confusion metrix. 

"""
def showMetrics(clf, testlabels, predicted_labels):
    print("Accuracy Score: ", metrics.accuracy_score(testlabels, predicted_labels))
    print("Print F1-score: ", metrics.f1_score(testlabels, predicted_labels, average='weighted'))
    x = clf.support_vectors_
    print("No. of support vectors: ", x.shape[0])

    print("Confusion Metrix: \n", metrics.confusion_matrix(testlabels, predicted_labels))

"""

Runs the model given a particular value of C and processed dataset for fitting. 

"""
def runModel(svm1, cvalue, algo = 'svc'):
    data = svm1.processedTrainingData
    labels = svm1.processedTrainingLabels 
    if (algo == 'svc'):
        clf = svm.SVC(kernel='linear', random_state = 0, C=cvalue, tol=svm1.tol)
    else:
        clf = LinearSVC(random_state=0, dual=False, C=cvalue, tol = svm1.tol)
    time1 = time.time()
    clf.fit(data, labels)
    time2 = time.time()
    fittingtime = time2 - time1
    
    
    testdata = svm1.processedTestData
    testlabels = svm1.processedTestLabels

    predicted_labels = clf.predict(testdata)
    accuracy_score = metrics.accuracy_score(testlabels, predicted_labels)
    print("C = : ", cvalue, 
            "Accuracy = ", accuracy_score, 
            "Time = ", fittingtime)
    
    return fittingtime, clf, predicted_labels, testlabels, accuracy_score



def trainAndTest():
    # Run the training after merging the data
    svm1 = SVMClassifier()
    svm1.readAllFiles(svm1.filepath)
    newlist = svm1.mergeDataset(svm1.filedict)
    print("newlist size: ", len(newlist))
    newdata = newlist[0]
    newlabels = newlist[1]
    print("Merged data shape = ", newdata.shape, "Length of merged labels: ", len(newlabels))
    sk_svm = svm.SVC(decision_function_shape='ovo', kernel='linear', C=1)
    sk_svm.fit(newdata, newlabels)
    testdata = svm1.testdict[b'data']
    predicted_labels = sk_svm.predict(testdata)
    testlabels = svm1.testdict[b'labels']
    print(metrics.accuracy_score(testlabels, predicted_labels))

#trainAndTest()



def runtest():
    svm1 = SVMClassifier()
    svm1.readAllFiles(svm1.filepath)
    print(svm1.metadict)
    return svm1

#runtest()

#### Code for testing ####
def runModelTest():
    svm1, newdata, newlabels = prepareData()
    for i in range(10):
        cvalue = 1 - i/10
        fittingtime, clf, predicted_labels, testlabels, accuracy_score = runModel(svm1, cvalue)

#runModelTest()

def runModelOnce():
    svm1, newdata, newlabels = prepareData(mask=0.5)
    cvalue = 1
    optimizeData(svm1)
    #svm1.processedTrainingData = np.copy(svm1.sliceData(svm1.processedTrainingData))
    #svm1.processedTestData = np.copy(svm1.sliceData(svm1.processedTestData))
    fittingtime, clf, predicted_labels, testlabels, accuracy_score = runModel(svm1, cvalue)

runModelOnce()
